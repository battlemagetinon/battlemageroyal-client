(() => {
	let _readyResolve;
	const _readyPromise = new Promise(resolve => _readyResolve = resolve);

	const C = {};

	IMAGE_PROCESSING = {
		loadOpenCV: async () => {
			const sources = [
				"js/OpenCV/4.4.0/opencv.js",
				"js/OpenCV/delaunator.min.js",
				"js/OpenCV/flubber.min.js",
			];

			let ready = 0;
			for (let i = 0; i < sources.length; i++) {
				createScript(sources[i]).onload = () => ready++;
			}
			await waitUntil(() => ready >= sources.length);

			ready = false;
			createScript("wasm/ImageProcessing.js").onload = () => ready = true;
			await waitUntil(() => ready);

			Object.assign(C, await ModuleCG());
			Object.freeze(C);
			_readyResolve();
		},
		createImageData: (width, height) => {
			const canvas = document.createElement("canvas");
			const context = canvas.getContext('2d');
			return context.createImageData(width, height);
		},
		getWornItemImage: (item, tier = 0, sprite = null) => {
			sprite = sprite || item.base.worn_image_url || item.base.image_url;
			return item.variant_color && Math.max(GAME_MANAGER.instance.tier, tier) >= 2 ? sprite.replace("items", "items/dyed/" + nameToRef(item.variant_color)) : sprite;
		},
		getItemImage: (item, tier = 0) => item.variant_color && Math.max(GAME_MANAGER.instance.tier, tier) >= 2 ? item.base.image_url.replace("items", "items/dyed/" + nameToRef(item.variant_color)) : item.base.image_url,
		getMorphImage: (imageUrl, color) => color && GAME_MANAGER.instance.tier >= 2 ? imageUrl.replace("items", "items/dyed/" + nameToRef(color)) : imageUrl,
	};

	Object.defineProperties(IMAGE_PROCESSING, {
		'ready': {
			get: () => _readyPromise,
		},
	});

	const MIN_PIXELS_PER_DIVISION = 20;

	function getImageData(source, boundingBox, scale = 1, flipped = false) {
		const canvas = document.createElement("canvas");
		const context = canvas.getContext('2d');
		canvas.width = source.width * scale;
		canvas.height = source.height * scale;
		if (flipped) {
			context.translate(canvas.width, 0);
			context.scale(-1, 1);
		}
		context.drawImage(source, 0, 0, canvas.width, canvas.height);
		return context.getImageData(boundingBox[0] * scale, boundingBox[1] * scale, boundingBox[2] * scale, boundingBox[3] * scale);
	}

	function getOffset(_sourceA, _sourceB) {
		const centerOfMassA = [_sourceA.centerOfMass[0] - _sourceA.boundingBox[0], _sourceA.centerOfMass[1] - _sourceA.boundingBox[1]];
		const centerOfMassB = [_sourceB.centerOfMass[0] - _sourceB.boundingBox[0], _sourceB.centerOfMass[1] - _sourceB.boundingBox[1]];
		return [0, 0, Math.max(0, centerOfMassA[0] * _sourceA.scale / _sourceB.scale - centerOfMassB[0]), Math.max(centerOfMassA[1] * _sourceA.scale / _sourceB.scale - centerOfMassB[1])];
	}

	function findOffsetRect(range, width, height, points) {
		if (!range) {
			return [0, 0];
		}

		const topLeft = [Number.MAX_VALUE, Number.MAX_VALUE];
		const bottomRight = [Number.MIN_VALUE, Number.MIN_VALUE];

		for (let i = 0; i < points.length; i++) {
			topLeft[0] = Math.min(topLeft[0], points[i][0]);
			topLeft[1] = Math.min(topLeft[1], points[i][1]);
			bottomRight[0] = Math.max(bottomRight[0], points[i][0]);
			bottomRight[1] = Math.max(bottomRight[1], points[i][1]);
		}

		bottomRight[0] = Math.min(width - bottomRight[0], 0);

		topLeft[0] = Math.min(topLeft[0], 0);
		topLeft[1] = Math.min(topLeft[1] + height - (bottomRight[1] - topLeft[1]), 0);

		return [[topLeft[0], topLeft[1]], [bottomRight[0], bottomRight[1]]];
	}

	function pointsToVector(points) {
		const vector = new C.Point2fVector();
		for (let i = 0; i < points.length; i++) {
			vector.push_back(points[i]);
		}
		return vector;
	}

	function rectsToVector(rects) {
		const vector = new C.Point2fVector();
		for (let i = 0; i < rects.length; i++) {
			vector.push_back(rects[i]);
		}
		return vector;
	}

	function imageToVector(image) {
		const vector = new C.Color32Vector();
		const size = image.width * image.height * 4;
		for (let i = 0; i < size; i += 4) {
			vector.push_back([image.data[i + 0], image.data[i + 1], image.data[i + 2], image.data[i + 3]]);
		}
		return vector;
	}

	function morph(points) {
		for (let i = 0; i < points.length; i++) {
			this.points.set(i, points[i]);
		}
		this.affineDeformation.MorphRects(this.pointsOrigin, this.points, this.rectsOrigin, this.rects);
		return this.rects;
	}

	IMAGE_PROCESSING.Interpolator = function (sourceA, sourceB, offset) {
		return flubber.interpolate(
			PointVector.multiply(PointVector.subtract(sourceA.points.slice().map(p => [p[0], p[1]]), sourceA.boundingBox[0] - offset[0], sourceA.boundingBox[1] - offset[1]), sourceA.scale),
			PointVector.multiply(PointVector.subtract(sourceB.points.slice().map(p => [p[0], p[1]]), sourceB.boundingBox[0] - offset[2], sourceB.boundingBox[1] - offset[3]), sourceB.scale),
			{ string: false, maxSegmentLength: 0 }
		);
	}

	IMAGE_PROCESSING.Animator = function (_sourceA, _sourceB, _range) {
		let a_source;
		let a_points;
		let a_rects_out;
		let a_affine_deformation

		let b_source;
		let b_points;
		let b_rects_out;
		let b_affine_deformation;

		let rects_origin;
		let points_in;
		let bilinear_interpolation;

		const _offset = getOffset(_sourceA, _sourceB);

		let interpolator = new IMAGE_PROCESSING.Interpolator(_sourceA, _sourceB, _offset);

		const [_topLeft, _bottomRight] = findOffsetRect(_range, _sourceA.boundingBox[2], _sourceA.boundingBox[3], interpolator(_range[0]));

		_offset[0] += _sourceA.relativeOffset[0] - _topLeft[0];
		_offset[1] += _sourceA.relativeOffset[1] - _topLeft[1];
		_offset[2] += _sourceB.relativeOffset[0] - _topLeft[0];
		_offset[3] += _sourceB.relativeOffset[1] - _topLeft[1];

		_sourceA.boundingBox[2] -= _topLeft[0] + _bottomRight[0];
		_sourceA.boundingBox[3] -= _topLeft[1];

		interpolator = new IMAGE_PROCESSING.Interpolator(_sourceA, _sourceB, _offset);

		_offset[0] *= _sourceA.scale;
		_offset[1] *= _sourceA.scale;
		_offset[2] *= _sourceB.scale;
		_offset[3] *= _sourceB.scale;

		_sourceA = Object.assign({}, _sourceA, { points: interpolator(0), image: getImageData(_sourceA.source, _sourceA.boundingBox, _sourceA.scale, _sourceA.flipped) });
		_sourceB = Object.assign({}, _sourceB, { points: interpolator(1), image: getImageData(_sourceB.source, _sourceB.boundingBox, _sourceB.scale, _sourceB.flipped) });

		const _canvasWidth = Math.max(_sourceA.image.width, _sourceB.image.width);
		const _canvasHeight = Math.max(_sourceA.image.height, _sourceB.image.height);

		const _canvas = document.createElement("canvas");
		const _context = _canvas.getContext("2d");

		_canvas.width = _canvasWidth;
		_canvas.height = _canvasHeight;

		const _gridSize = [Math.floor(_canvasWidth / MIN_PIXELS_PER_DIVISION), Math.floor(_canvasHeight / MIN_PIXELS_PER_DIVISION)];
		const _rectSize = [_canvasWidth / _gridSize[0], _canvasHeight / _gridSize[1]];
		const _rects = [];

		for (let j = 0; j <= _gridSize[1]; j++) {
			let y = Math.round(j * _rectSize[1]);
			for (let i = 0; i <= _gridSize[0]; i++) {
				let x = Math.round(i * _rectSize[0]);
				_rects.push([x, y]);
			}
		}

		this.Delete = () => {
			a_source && a_source.delete();
			a_points && a_points.delete();
			a_rects_out && a_rects_out.delete();
			a_affine_deformation && a_affine_deformation.delete();

			b_source && b_source.delete();
			b_points && b_points.delete();
			b_rects_out && b_rects_out.delete();
			b_affine_deformation && b_affine_deformation.delete();

			rects_origin && rects_origin.delete();
			points_in && points_in.delete();
			bilinear_interpolation && bilinear_interpolation.delete();
		};

		try {
			a_points = pointsToVector(_sourceA.points);
			b_points = pointsToVector(_sourceB.points);

			points_in = rectsToVector(_sourceA.points);
			rects_origin = rectsToVector(_rects);

			a_rects_out = rectsToVector(_rects);
			b_rects_out = rectsToVector(_rects);

			b_affine_deformation = new C.AffineDeformation(b_points.size());
			a_affine_deformation = new C.AffineDeformation(a_points.size());

			Object.assign(_sourceA, { morph: morph, pointsOrigin: a_points, points: points_in, rectsOrigin: rects_origin, rects: a_rects_out, affineDeformation: a_affine_deformation });
			Object.assign(_sourceB, { morph: morph, pointsOrigin: b_points, points: points_in, rectsOrigin: rects_origin, rects: b_rects_out, affineDeformation: b_affine_deformation });

			bilinear_interpolation = new C.BilinearInterpolation(_canvasWidth, _canvasHeight, _gridSize[0], _gridSize[1], _rectSize[0], _rectSize[1]);

			bilinear_interpolation.SetOffset(_offset[0], _offset[1], _offset[2], _offset[3]);
			bilinear_interpolation.SetBoundingBoxes(
				_sourceA.boundingBox[0], _sourceA.boundingBox[1], Math.floor(_sourceA.boundingBox[2] * _sourceA.scale), Math.floor(_sourceA.boundingBox[3] * _sourceA.scale),
				_sourceB.boundingBox[0], _sourceB.boundingBox[1], Math.floor(_sourceB.boundingBox[2] * _sourceB.scale), Math.floor(_sourceB.boundingBox[3] * _sourceB.scale)
			);

			a_source = imageToVector(_sourceA.image);
			b_source = imageToVector(_sourceB.image);
		}
		catch (e) {
			this.Delete();
			throw e;
		}

		let _drawTriangles = false;
		let _applyMask = false;
		let _antialiasing = false;
		let _drawLines = false;

		Object.defineProperties(this, {
			'drawTriangles': {
				get: () => _drawTriangles,
				set: (drawTriangles) => _drawTriangles = drawTriangles == true,
			},
			'drawLines': {
				get: () => _drawLines,
				set: (drawLines) => _drawLines = drawLines == true,
			},
			'applyMask': {
				get: () => _applyMask,
				set: (applyMask) => _applyMask = applyMask == true,
			},
			'antialiasing': {
				get: () => _antialiasing,
				set: (antialiasing) => _antialiasing = antialiasing == true,
			},
		});

		this.DrawFrame = (progress, canvas, width, height) => {
			const ctx = canvas.getContext("2d");

			renderFrame(interpolator(progress), progress);

			const ratio = height / (_sourceA.source.height);

			canvas.height = _sourceA.boundingBox[3] * ratio;

			ctx.clearRect(0, 0, canvas.width, canvas.height);

			if (_drawLines) {
				ctx.rect(0, 0, canvas.width, canvas.height);

				ctx.rect(0, canvas.height - _sourceA.boundingBox[3] * ratio - _topLeft[1] * ratio, canvas.width, canvas.height);

				ctx.rect((canvas.width - width) / 2, 0, width, canvas.height);

				ctx.rect(
					(canvas.width - (_sourceA.source.width - (_sourceA.source.width - _sourceA.boundingBox[2]) + _topLeft[0] + _bottomRight[0]) * ratio) / 2,
					0,
					(_sourceA.boundingBox[2] + _topLeft[0] + _bottomRight[0]) * ratio,
					canvas.height,
				);

				ctx.rect(
					(canvas.width - (_sourceA.source.width - (_sourceA.source.width - _sourceA.boundingBox[2]) + _topLeft[0] + _bottomRight[0]) * ratio) / 2 + _topLeft[0] * ratio,
					0,
					_sourceA.boundingBox[2] * ratio,
					canvas.height,
				);

				ctx.stroke();
			}

			ctx.translate(0, -2);
			ctx.drawImage(_canvas, (canvas.width - (_sourceA.source.width - (_sourceA.source.width - _sourceA.boundingBox[2]) + _topLeft[0] + _bottomRight[0]) * ratio) / 2 + _topLeft[0] * ratio, 0, _sourceA.boundingBox[2] * ratio, canvas.height);
		}

		const renderFrame = async (points, progress) => {
			const imageData = _antialiasing
				? new ImageData(new Uint8ClampedArray(C.HEAP8.buffer, bilinear_interpolation.DrawRectsAntialiasing(a_source, b_source, _sourceA.morph(points), _sourceB.morph(points), rects_origin, clamp01(progress)), _sourceA.image.width * _sourceA.image.height * 4), _sourceA.image.width, _sourceA.image.height)
				: new ImageData(new Uint8ClampedArray(C.HEAP8.buffer, bilinear_interpolation.DrawRects(a_source, b_source, _sourceA.morph(points), _sourceB.morph(points), rects_origin, clamp01(progress)), _sourceA.image.width * _sourceA.image.height * 4), _sourceA.image.width, _sourceA.image.height);

			_context.putImageData(imageData, 0, 0);
			_context.beginPath();
			{
				_context.moveTo(points[points.length - 1][0], points[points.length - 1][1]);
				for (let i = 0; i < points.length; i++) {
					_context.lineTo(points[i][0], points[i][1]);
				}
			}
			_context.closePath();

			if (_applyMask && progress > 0 && progress < 1) {
				_context.globalCompositeOperation = "destination-in";
				_context.fill();
			}

			if (_drawTriangles) {
				let triangles = Delaunator.from(points).triangles;

				for (let i = 0; i < triangles.length; i += 3) {
					let p1 = [points[triangles[i + 0]][0], points[triangles[i + 0]][1]];
					let p2 = [points[triangles[i + 1]][0], points[triangles[i + 1]][1]];
					let p3 = [points[triangles[i + 2]][0], points[triangles[i + 2]][1]];
					_context.moveTo(p3[0], p3[1]);
					_context.lineTo(p1[0], p1[1]);
					_context.lineTo(p2[0], p2[1]);
					_context.lineTo(p3[0], p3[1]);
				}

				_context.globalCompositeOperation = "source-over";
				_context.stroke();
			}
		}
	};

	IMAGE_PROCESSING.findPoints = (image, out = null, drawContour = false, drawTriangles = false, drawConvexHull = false, drawCenterOfMass = false) => {
		let src, dst, low, high, blob, poly, poly_mat, hull, hull_mat, contours, hierarchy;

		try {
			src = cv.imread(image);
		}
		catch {
			return null;
		}

		try {
			const obj = { source: image, scale: 1, flipped: false, relativeOffset: [0, 0] };
			contours = new cv.MatVector();
			hierarchy = new cv.Mat();
			{
				blob = new cv.Mat();
				low = new cv.Mat(src.rows, src.cols, src.type(), [0, 0, 0, 0]);
				high = new cv.Mat(src.rows, src.cols, src.type(), [150, 150, 150, 150]);
				cv.inRange(src, low, high, blob);
				cv.bitwise_not(blob, blob);
				cv.findContours(blob, contours, hierarchy, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE);
			}

			let cnt;
			let cntIndex;
			{
				let maxArea = 0;
				for (let i = 0; i < contours.size(); ++i) {
					let area = cv.contourArea(contours.get(i), false);
					if (area > maxArea) {
						maxArea = area;
						cntIndex = i;
					}
				}
				cnt = contours.get(cntIndex);
			}

			poly = new cv.MatVector();
			{
				let epsilon = cv.arcLength(cnt, true) / cnt.rows;
				poly_mat = new cv.Mat();
				cv.approxPolyDP(cnt, poly_mat, epsilon, true);
				poly.push_back(poly_mat);
				cnt = poly_mat;
			}

			hull = new cv.MatVector();
			{
				hull_mat = new cv.Mat();
				cv.convexHull(cnt, hull_mat, false, true);
				hull.push_back(hull_mat);
				let boundingBox = cv.boundingRect(cnt);
				obj.boundingBox = [boundingBox.x, boundingBox.y, boundingBox.width, boundingBox.height];
			}

			let points = [];
			{
				for (let row = 0; row < cnt.rows; row++) {
					points.push([cnt.data32S[row * 2], cnt.data32S[row * 2 + 1]]);
				}
				for (let row = 0; row < cnt.rows; row++) {
					points.push([cnt.data32S[row * 2], cnt.data32S[row * 2 + 1]]);
				}
			}

			const delaunay = Delaunator.from(points);
			const centerOfMass = [0, 0];
			const triangles = [];
			let totalWeight = 0;

			for (let i = 0; i < delaunay.triangles.length; i += 3) {
				let triangle = [points[delaunay.triangles[i + 0]], points[delaunay.triangles[i + 1]], points[delaunay.triangles[i + 2]]];
				let point = Triangle.centroid(triangle[0], triangle[1], triangle[2]);

				if (cv.pointPolygonTest(cnt, new cv.Point(point[0], point[1]), false) < 0) {
					continue;
				}

				let weight = Triangle.area(triangle[0], triangle[1], triangle[2]);
				centerOfMass[0] += point[0] * weight;
				centerOfMass[1] += point[1] * weight;
				totalWeight += weight;
				triangles.push(triangle[0], triangle[1], triangle[2],);
			}

			centerOfMass[0] = Math.floor(centerOfMass[0] / totalWeight);
			centerOfMass[1] = Math.floor(centerOfMass[1] / totalWeight);

			if (out) {
				dst = cv.Mat.zeros(src.rows, src.cols, cv.CV_8UC3);

				if (drawTriangles) {
					let color = new cv.Scalar(Math.round(Math.random() * 255 * 0.5), Math.round(Math.random() * 255 * 0.5), Math.round(Math.random() * 255 * 0.5));

					for (let i = 0; i < delaunay.triangles.length; i += 3) {
						let p1 = new cv.Point(points[delaunay.triangles[i + 0]][0], points[delaunay.triangles[i + 0]][1]);
						let p2 = new cv.Point(points[delaunay.triangles[i + 1]][0], points[delaunay.triangles[i + 1]][1]);
						let p3 = new cv.Point(points[delaunay.triangles[i + 2]][0], points[delaunay.triangles[i + 2]][1]);
						cv.line(dst, p1, p2, color, 1);
						cv.line(dst, p2, p3, color, 1);
						cv.line(dst, p3, p1, color, 1);
					}

					color = new cv.Scalar(Math.round(Math.random() * 255), Math.round(Math.random() * 255), Math.round(Math.random() * 255));
					for (let i = 0; i < cnt.rows; i++) {
						cv.circle(dst, new cv.Point(points[i][0], points[i][1]), 1, color, 1, cv.LINE_8, 0);
					}
				}

				if (drawContour) {
					let color = new cv.Scalar(Math.round(Math.random() * 255), Math.round(Math.random() * 255), Math.round(Math.random() * 255));
					cv.drawContours(dst, poly, 0, color, 1, cv.LINE_8, hierarchy, 0);
				}

				if (drawConvexHull) {
					let color = new cv.Scalar(Math.round(Math.random() * 255), Math.round(Math.random() * 255), Math.round(Math.random() * 255));
					cv.drawContours(dst, hull, 0, color, 1, cv.LINE_8, hierarchy, 0);
				}

				if (drawCenterOfMass) {
					let color = new cv.Scalar(Math.round(Math.random() * 255), Math.round(Math.random() * 255), Math.round(Math.random() * 255));
					cv.circle(dst, new cv.Point(centerOfMass[0], centerOfMass[1]), 4, color, 2, cv.LINE_8, 0);

					for (let i = 0; i < triangles.length; i += 3) {
						let point = Triangle.centroid(triangles[i + 0], triangles[i + 1], triangles[i + 2]);
						cv.circle(dst, new cv.Point(point[0], point[1]), 1, color, 1, cv.LINE_8, 0);
					}
				}

				cv.imshow(out, dst);
			}
			return Object.assign(obj, { points: points, centerOfMass: centerOfMass });
		}
		finally {
			src && src.delete();
			dst && dst.delete();
			low && low.delete();
			high && high.delete();
			blob && blob.delete();
			poly && poly.delete();
			poly_mat && poly_mat.delete();
			hull && hull.delete();
			hull_mat && hull_mat.delete();
			contours && contours.delete();
			hierarchy && hierarchy.delete();
		}
	}

	const Triangle = {
		centroid: (a, b, c) => [(a[0] + b[0] + c[0]) / 3, (a[1] + b[1] + c[1]) / 3],
		area: (a, b, c) => {
			let m = [Math.min(a[0], b[0], c[0]), Math.min(a[1], b[1], c[1]), Math.max(a[0], b[0], c[0]), Math.max(a[1], b[1], c[1])];
			return (m[2] - m[0]) * (m[3] - m[1]) / 2;
		},
	};

	const PointVector = {
		subtract: (v, x, y) => {
			for (let i = 0; i < v.length; i++) {
				v[i][0] -= x; v[i][1] -= y;
			}
			return v;
		},
		multiply: (v, w) => {
			for (let i = 0; i < v.length; i++) {
				v[i][0] *= w; v[i][1] *= w;
			}
			return v;
		}
	};

	function createScript(source) {
		const script = document.createElement("script");
		script.type = "text/javascript";
		script.src = source;
		script.async = true;
		document.getElementsByTagName("head")[0].appendChild(script);
		return script;
	}
})();
