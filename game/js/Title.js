(() => {
    TITLE = {};

    const _messages = [];

    let _title = document.title;
    let _time;
    let _displayTime;

    let _id = 0;

    Object.defineProperties(TITLE, {
        'time': {
            get: () => _time,
            set: time => _time = time
        },
    });

    TITLE.Message = (message) => !document.hasFocus() && _messages.indexOf(message) < 0 && _messages.push(message);

    TITLE.Update = () => {
        if (!document.hasFocus()) {
            _displayTime = !_displayTime;
            if (_displayTime || _messages.length == 0) {
                CALENDAR.instance.Update();
                document.title = (_time && _time.length > 0 ? `[${_time}] ` : "") + _title;
            }
            else {
                document.title = `[${_messages[_id++ % _messages.length]}] ${_title}`;
            }
        }
        else {
            if (document.title != _title) {
                document.title = _title;
            }
            if (_messages.length > 0) {
                _messages.length = 0;
            }
        }
    };
})();
