(() => {
	COUNTDOWN = {
		RADIUS: 40,
		LINE_WIDTH: 5,
		COLOR: "#ff0000",
		list: [],
		Clear: () => {
			for (let i = 0; i < COUNTDOWN.list.length; i++) {
				COUNTDOWN.list[i].RemoveSelf();
			}
		},
		ClearByTarget: target => {
			for (let i = 0; i < COUNTDOWN.list.length; i++) {
				if (COUNTDOWN.list[i].target == target) {
					COUNTDOWN.list[i].RemoveSelf();
				}
			}
		}
	}
})();

COUNTDOWN.Countdown = function (_duration, _target, _type) {
	const _elm = document.createElement("div");
	const _timer = document.createElement("span");
	const _label = document.createElement("span");
	const _cancel = document.createElement("div");
	const _seconds = _duration / 1000;

	const _start = Date.now();

	let _cancellable;
	let _labelText;
	let _circle;
	let _scale;
	let _s;

	let _progress = 0;

	switch (_type) {
		case "leave":
			_labelText = "Leaving";
			_cancellable = _target == "player";
			if (_cancellable) {
				_elm.onclick = () => GAME_MANAGER.instance.Send({ action: "Leave", cancel: true });
			}
			break;
		case "dazed":
			_labelText = "Dazed";
			_cancellable = false;
			break;
	}

	if (_target == "player") {
		_elm.classList.add("left");
	}
	else {
		_elm.classList.add("right");
	}

	_label.innerHTML = _labelText;

	Object.defineProperties(this, {
		'cancellable': {
			get: () => _cancellable
		},
		'label': {
			get: () => _labelText
		},
		'type': {
			get: () => _type
		},
		'target': {
			get: () => _target
		},
	});

	this.Update = (now) => {
		_progress = (now - _start) / _duration;
		if (_progress < 1) {
			_circle.style.strokeDashoffset = `${3.14 * 2 * (COUNTDOWN.RADIUS - COUNTDOWN.LINE_WIDTH) * _scale * _progress}px`;
			_s = Math.floor(_seconds - _progress * _seconds);
			if (_s.toString() != _timer.innerHTML) {
				if (_s <= 5) {
					_circle.style.stroke = COUNTDOWN.COLOR;
					_elm.style.color = COUNTDOWN.COLOR;
				}
				_timer.innerHTML = _s;
				_timer.style.fontSize = `${1.5 + 0.5 * _progress}em`;
			}
		}
		else {
			this.RemoveSelf();
		}
	};

	this.Resize = () => {
		_scale = container.offsetHeight / 720;
		_elm.innerHTML = `<svg><circle r="${(COUNTDOWN.RADIUS - COUNTDOWN.LINE_WIDTH) * _scale}" cx="${COUNTDOWN.RADIUS * _scale}" cy="${COUNTDOWN.RADIUS * _scale}"></circle></svg>`;
		_elm.style.width = `${COUNTDOWN.RADIUS * 2 * _scale}px`;
		_elm.style.height = `${COUNTDOWN.RADIUS * 2 * _scale}px`;

		_elm.appendChild(_timer);
		_elm.appendChild(_cancel);
		_cancel.appendChild(_label);

		if (_cancellable) {
			_elm.onmouseenter = () => _label.innerHTML = "Cancel";
			_elm.onmouseleave = () => _label.innerHTML = _labelText;
		}
		else {
			_elm.style.pointerEvents = "none";
		}

		_timer.style.lineHeight = `${Math.floor(COUNTDOWN.RADIUS * 2 * _scale)}px`;

		_circle = _elm.getElementsByTagName("circle")[0];
		_circle.style.strokeWidth = `${COUNTDOWN.LINE_WIDTH * _scale}px`;
		_circle.style.strokeDasharray = `${3.14 * 2 * (COUNTDOWN.RADIUS - COUNTDOWN.LINE_WIDTH) * _scale}px`;

		if (_seconds - _progress * _seconds <= 5) {
			_circle.style.stroke = COUNTDOWN.COLOR;
			_timer.style.color = COUNTDOWN.COLOR;
		}

		this.Update(0);
	};

	this.RemoveSelf = () => {
		_elm.parentNode && _elm.parentNode.removeChild(_elm);
		var index = COUNTDOWN.list.indexOf(this);
		if (index >= 0) {
			COUNTDOWN.list.splice(index, 1);
		}
	};

	this.Resize();
	document.getElementById("countdowns").appendChild(_elm);
	COUNTDOWN.list.push(this);
}
