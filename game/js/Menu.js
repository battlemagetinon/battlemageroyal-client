MENU = {
	INVENTORY_TAB_WIDTH: 10,
	INVENTORY_TAB_HEIGHT: 5,
	HEIRLOOM_CONTAINER_HEIGHT: 3,
	COOLDOWN_DURATION: 3000,
	CloseAll: () => closeMenu(),
	CloseLeftMenu: () => closeMenu("left"),
	CloseRightMenu: () => closeMenu("right"),
	MenuIsOpened: () => document.getElementById("menus").childElementCount > 0,
};

function displayMenu(menu, position, onClose) {
	const menus = document.getElementById("menus");

	if (position != "full") {
		removeMenu(position);
	}
	else {
		removeMenu();
	}

	menu.classList.add(position);
	container.classList.add(`menu_${position}`);

	const closeButtons = menu.getElementsByClassName("close");
	for (let i = 0; i < closeButtons.length; i++) {
		if (closeButtons[i].parentNode == menu) {
			menu.removeChild(closeButtons[i]);
		}
	}

	if (onClose !== false) {
		const closeBtn = document.createElement("div");
		closeBtn.className = "close";
		closeBtn.onclick = onClose ? onClose : () => closeMenu(position);
		menu.appendChild(closeBtn);
	}

	if (position == "left" && menus.firstChild) {
		menus.insertBefore(menu, menus.firstChild)
	}
	else {
		menus.appendChild(menu);
	}

	if (CURSOR.instance.IsMouseOver(menu)) {
		TOOLTIP.instance.Hide();
	}
};

function removeMenu(position) {
	const positions = position ? [position, "full"] : ["left", "right", "full"];
	const menus = document.getElementById("menus");

	for (let i = 0; i < positions.length; i++) {
		const menu = menus.getElementsByClassName(positions[i])[0];

		if (menu) {
			if (isDescendant(menu, TOOLTIP.instance.trigger)) {
				TOOLTIP.instance.Hide();
			}
			menus.removeChild(menu);
			container.classList.remove(`menu_${positions[i]}`);
		}
	}
}

async function closeMenu(position) {
	if (MENU.Crafting.menu && MENU.Crafting.full) {
		removeMenu();
	}
	else {
		removeMenu(position);
	}
	if (GAME_MANAGER.instance.hasLoot || MENU.Crafting.pending) {
		await waitForFrame();
		if (!menus.getElementsByClassName("left")[0]) {
			if (GAME_MANAGER.instance.hasLoot) {
				MENU.Loot.Open();
			}
			else {
				MENU.Crafting.Open();
			}
		}
		if (MENU.Crafting.full && !menus.getElementsByClassName("right")[0]) {
			MENU.Inventory.Open();
		}
	}
};

function createInputField(id, placeholder = null) {
	const input = document.createElement("input");
	input.type = "text";
	input.id = id;
	input.name = id;
	if (placeholder) {
		input.placeholder = placeholder;
	}
	return input;
}

function createForm() {
	const form = document.createElement("form");
	form.autocomplete = "off"
	form.method = "post";
	form.action = "#";
	form.onsubmit = () => false;
	return form;
}

function drawHeader(title, parent) {
	const header = document.createElement("h3");
	header.innerHTML = title;
	parent.appendChild(header);
	return header;
}

function drawToggle(parent, id, title, tooltip) {
	const li = document.createElement("li");
	li.className = "enable";

	const input = document.createElement("input");
	input.type = "checkbox";
	input.id = id;

	const label = document.createElement("label");
	label.htmlFor = input.id;
	label.onmouseenter = (e) => TOOLTIP.instance.Show(e, tooltip);
	label.ontouchstart = label.onmouseenter;

	const span = document.createElement("span");
	span.innerHTML = title;

	label.appendChild(span);
	li.appendChild(input);
	li.appendChild(label);
	parent.appendChild(li);
	return input;
}
