(() => {
	GAME_MANAGER = {
		DEV_ENV: window.location.host != "battlemageroyal.com" && window.location.host != "www.battlemageroyal.com",
	};

	GAME_MANAGER.WEB_SOCKET = `wss://battlemageroyal-app-${GAME_MANAGER.DEV_ENV ? "test" : "live"}.herokuapp.com`;
	GAME_MANAGER.HTTPS = `https://battlemageroyal-app-${GAME_MANAGER.DEV_ENV ? "test" : "live"}.herokuapp.com`;

	let _instance;

	Object.defineProperty(GAME_MANAGER, "instance", {
		get: () => {
			if (!_instance) {
				_instance = new GAME_MANAGER.GameManager();
			}
			return _instance;
		}
	});
})();

GAME_MANAGER.GameManager = function () {
	const _loadSymbol = document.getElementById("load_symbol");

	if (!GAME_MANAGER.DEV_ENV) {
		container.oncontextmenu = (e) => e && !e.preventDefault();
	}

	let _session;
	let _ws;

	const _colorsByUsername = {};
	const _friends = [{}, {}, {}, {}];

	let _ready = false;
	let _remembered = false;

	let _recentCharacters = [];
	let _ignoredUsers = {};

	let _scenario = false;
	let _turnEvents = {};
	let _enabledFacultyLocations = [];
	let _discoveredLocations = [];

	let _spells = {};
	let _skills = {};
	let _filter = {};

	let _tier = 0;
	let _settings = {};

	let _actions = {};
	let _usedInteractions = {};
	let _roleplayScenario = null;

	let _items = null;
	let _baseItems = null;
	let _enchantments = null;

	let _equipment = { items: [], worn: [] };
	let _inventory = { held: null, crafting: null, loot: [], tab: [], heirlooms: [] };

	Object.defineProperties(this, {
		'username': {
			get: () => getSession().username,
		},
		'spells': {
			get: () => _spells
		},
		'skills': {
			get: () => _skills
		},
		'actions': {
			get: () => _actions
		},
		'remembered': {
			get: () => _remembered
		},
		'filter': {
			get: () => _filter
		},
		'settings': {
			get: () => _settings
		},
		'tier': {
			get: () => _tier
		},
		'inventory': {
			get: () => _inventory
		},
		'heldItem': {
			get: () => _inventory.held
		},
		'hasLoot': {
			get: () => _inventory.loot.length > 0
		},
		'equipment': {
			get: () => _equipment
		},
		'friends': {
			get: () => {
				const friends = [];
				for (let i = 0; i < _friends.length; i++) {
					friends[i] = Object.assign({}, _friends[i]);
				}
				return friends;
			},
		},
		'ignoredUsers': {
			get: () => _ignoredUsers || {}
		},
		'roleplayScenario': {
			get: () => _roleplayScenario || { title: '', description: '' }
		},
		'recentCharacters': {
			get: () => _recentCharacters || []
		},
		'turnEvents': {
			get: () => _turnEvents
		},
		'enabledFacultyLocations': {
			get: () => _enabledFacultyLocations
		},
		'discoveredLocations': {
			get: () => _discoveredLocations
		},
		'ready': {
			get: () => _ready
		},
	});

	const getSession = () => {
		if (!_session) {
			let data = sessionStorage.getItem("session");
			if (!data) {
				data = localStorage.getItem("session");
				_remembered = true;
			}
			try {
				_session = data ? JSON.parse(data) : {};
			}
			catch (e) {
			}
			if (!_session) {
				_session = {};
			}
		}
		return _session;
	};

	this.Connect = (idToken) => {
		displaySymbol(true);

		const session = getSession();

		if (!session.username) {
			window.location.href = '/';
		}

		_ws = new WebSocket(GAME_MANAGER.WEB_SOCKET);

		_ws.onmessage = e => {
			let obj = JSON.parse(e.data);

			if (obj.success) {
				displaySymbol(false);

				_ws.onmessage = onMessage;

				if (obj.tier !== undefined) {
					_tier = obj.tier;
					MENU.Inventory.UpdateTier(_tier);
					MENU.Settings.UpdateTier(_tier);
					MENU.Inventory.Redraw();
					MENU.Loot.Refresh();
				}

				onMessage(e);

				this.Send = (obj) => {
					if (GAME_MANAGER.DEV_ENV) {
						console.log(JSON.stringify(obj));
					}
					_ws.send(JSON.stringify(obj))
				};

				_ready = true;

				if (MENU.Messages.menu) {
					MENU.Messages.ShowInbox();
				}

				MENU.Skills.Refresh();
				MENU.Myself.Refresh();

				window.onmessage = (e) => {
					const segments = e.data.split(":");
					switch (segments[0]) {
						case "inspect":
							MENU.Inspect.Open(GUI.Position.Left, null, segments[1]);
							break;
						case "message":
							MENU.Messages.Open(segments[1]);
							break;
						case "friend":
							GAME_MANAGER.instance.AddFriend(segments[1], false, segments[2] ? segments[2] : null);
							MENU.Social.Open();
							break;
						case "ignore":
							GAME_MANAGER.instance.IgnoreUser(segments[1]);
							MENU.Social.OpenIgnored();
							break;
					}
				};
			}
		}

		_ws.onopen = e => _ws.send(JSON.stringify(Object.assign(session, { id_token: idToken })));

		_ws.onclose = async (e) => {
			displaySymbol(false);
			console.log(e);

			if (await GUI.instance.Alert(`${e.code}: ${e.reason ? e.reason : "Unexpected error."}`, "Exit", "Reload")) {
				window.location.href = '/';
			}
			else {
				window.location.reload(true);
			}
		};
	};

	const onMessage = (e) => {
		const obj = JSON.parse(e.data);

		if (obj.message !== undefined) {
			console.log(obj.message);

			GUI.instance.DisplayMessage(obj.message);

			if (obj.data !== undefined && obj.data.action) {
				switch (obj.data.action.toLowerCase()) {
					case "item":
						if (obj.data.equip === true) {
							CURSOR.instance.updating = false;
						}
				}
			}
		}
		else if (GAME_MANAGER.DEV_ENV) {
			console.log(obj);
		}

		if (obj.inventory !== undefined) {
			let itemIds;

			if (obj.inventory.items !== undefined) {
				_items = obj.inventory.items;
				itemIds = Object.keys(_items).map(key => parseInt(key));
			}

			if (obj.inventory.baseItems !== undefined) {
				_baseItems = obj.inventory.baseItems;
			}

			if (obj.inventory.equipment !== undefined) {
				_equipment = obj.inventory.equipment;
			}

			if (obj.inventory.enchantments !== undefined) {
				_enchantments = obj.inventory.enchantments;
			}

			_inventory.tab = obj.inventory.tab !== undefined ? obj.inventory.tab : itemIds ? _inventory.tab.map(id => itemIds.includes(id) ? id : null) : _inventory.tab;
			_inventory.heirlooms = obj.inventory.heirlooms !== undefined ? obj.inventory.heirlooms : itemIds ? _inventory.heirlooms.map(id => itemIds.includes(id) ? id : null) : _inventory.heirlooms;

			if (obj.inventory.loot !== undefined) {
				if (obj.inventory.loot && obj.inventory.loot.length > 0 && obj.inventory.loot[0] != null) {
					_inventory.loot = obj.inventory.loot;
					MENU.Inventory.Open();
					MENU.Loot.Open();
				}
				else {
					_inventory.loot = [];
					MENU.Loot.Close();
				}
			}
			else if (itemIds) {
				_inventory.loot = _inventory.loot.map(id => itemIds.includes(id) ? id : null);
			}

			if (obj.inventory.crafting !== undefined) {
				_inventory.crafting = obj.inventory.crafting;
				GUI.instance.SetCraftingBench(_inventory.crafting ? _inventory.crafting : false);
				MENU.Crafting.Refresh();
			}
			else if (_inventory.crafting && itemIds && !itemIds.includes(_inventory.crafting)) {
				_inventory.crafting = null;
			}

			if (obj.inventory.held !== undefined) {
				_inventory.held = obj.inventory.held;

				if (_inventory.held && _items[_inventory.held] && _baseItems[_items[_inventory.held].base]) {
					CURSOR.instance.SetItem(Object.assign({}, _items[_inventory.held], { base: _baseItems[_items[_inventory.held].base] }));
					MENU.Myself.ClearSlotByItem(_inventory.held);
					MENU.Inventory.Open();
				}
				else {
					CURSOR.instance.SetItem(null);
				}
			}
			else {
				if (_inventory.held && itemIds && !itemIds.includes(_inventory.held)) {
					_inventory.held = null;
				}
				CURSOR.instance.ValidateItem();
			}

			MENU.Inventory.Refresh();
			MENU.Crafting.Refresh();
			MENU.Spells.Refresh();
		}

		if (obj.scenario !== undefined) {
			_scenario = obj.scenario;

			if (_scenario) {
				LOCAL_CHAT.Clear();
				GUI.instance.HideStatus();
				GUI.instance.CloseActionHubs();
				MENU.Location.Close();
				COUNTDOWN.Clear();
			}
			else {
				GUI.instance.HideDialog();
				GUI.instance.UpdateLocation(LOCATION.instance);
			}
		}

		if (obj.social !== undefined) {
			if (obj.social.roleplayScenario !== undefined) {
				_roleplayScenario = obj.social.roleplayScenario;
				if (MENU.Social.menu && MENU.Social.menu.classList.contains("scenario_creation")) {
					MENU.Social.ShowRoleplayScenarios();
				}
			}
			if (obj.social.scenarios !== undefined) {
				MENU.Social.AddRoleplayScenarios(obj.social.scenarios, obj.social.id);
			}
			if (obj.social.broadcasting !== undefined) {
				GUI.instance.SetRoleplayBroadcasting(obj.social.broadcasting);
			}
			if (obj.social.details) {
				MENU.Social.ShowRoleplayScenarioDetails(obj.social.username, obj.social.title, obj.social.details);
			}
		}

		if (obj.chat !== undefined) {
			if (!_scenario) {
				if (obj.chat.local) {
					if (obj.chat.local.message !== undefined) {
						if (obj.chat.local.sender == getSession().username) {
							LOCAL_CHAT.player.Append(obj.chat.local.channel, obj.chat.local.message, LOCATION.instance.player.nature);
						}
						else if (LOCATION.instance.opponent && obj.chat.local.sender === LOCATION.instance.opponent.username) {
							LOCAL_CHAT.opponent.Append(obj.chat.local.channel, obj.chat.local.message, LOCATION.instance.opponent.nature);
							NOTIFICATION.instance.ChatMessage(obj.chat.local.sender, obj.chat.local.channel, obj.chat.local.message);
						}
					}
					else if (obj.chat.local.isTyping !== undefined) {
						if (!_scenario && LOCATION.instance.opponent && obj.chat.local.sender === LOCATION.instance.opponent.username) {
							LOCAL_CHAT.SetTypingIndicator(obj.chat.local.isTyping, GUI.Position.Right);
						}
					}
				}
				else if (obj.chat.combat) {
					if (obj.chat.combat.firstPerson) {
						LOCAL_CHAT.player.Append(LOCAL_CHAT.Channel.Combat, obj.chat.combat.message, LOCATION.instance.player.nature);
						if (obj.chat.combat.hit) {
							GUI.instance.AnimateCharacter(LOCATION.instance.player, GUI.Animation.Hit);
							NOTIFICATION.instance.CombatMessage(obj.chat.combat.message);
						}
					}
					else if (LOCATION.instance.opponent) {
						LOCAL_CHAT.opponent.Append(LOCAL_CHAT.Channel.Combat, obj.chat.combat.message, LOCATION.instance.opponent.nature);
						if (obj.chat.combat.hit) {
							GUI.instance.AnimateCharacter(LOCATION.instance.opponent, GUI.Animation.Hit);
						}
					}
				}
			}

			if (obj.chat.voice) {
				VOICE_CHAT.instance.Display(obj.chat.voice.message, obj.chat.voice.position);
			}
			else if (obj.chat.message) {
				const menu = MENU.Messages.menu;

				if (obj.chat.message.sender && (!document.hasFocus() || !menu || !menu.parentNode || !menu.classList.contains("new") || obj.chat.message.sender != MENU.Messages.receiver.value)) {
					NOTIFICATION.instance.PrivateMessage(obj.chat.message);
				}

				if (menu && menu.parentNode) {
					if (obj.chat.message.sender) {
						if (menu.classList.contains("inbox")) {
							MENU.Messages.ShowInbox();
						}
						else if (menu.classList.contains("new") && obj.chat.message.sender == MENU.Messages.receiver.value) {
							MENU.Messages.LoadChat(MENU.Messages.receiver.value);
						}
					}
					else if (obj.chat.message.error) {
						menu.style.pointerEvents = "";
					}
					else {
						if (MENU.Messages.username) {
							MENU.Messages.LoadChat(MENU.Messages.receiver.value);
						}
						else {
							MENU.Messages.receiver.value = "";
							MENU.Messages.ShowInbox();
						}

						menu.getElementsByClassName("editable")[0].innerHTML = "";
					}
				}
			}

			if (obj.chat.unreadMessages !== undefined) {
				GUI.instance.SetUnreadMessages(obj.chat.unreadMessages);
			}
		}

		if (obj.notification !== undefined) {
			switch (NOTIFICATION.Type[obj.notification.type]) {
				case NOTIFICATION.Type.PrivateMessage:
					NOTIFICATION.instance.PrivateMessage(obj.notification);
					break;
				case NOTIFICATION.Type.Online:
					NOTIFICATION.instance.FriendOnline(obj.notification);
					break;
				case NOTIFICATION.Type.FriendRequest:
					NOTIFICATION.instance.FriendRequest(obj.notification);
					break;
				case NOTIFICATION.Type.Meetup:
					NOTIFICATION.instance.Meetup(obj.notification);
					break;
			}
		}

		if (obj.calendar !== undefined) {
			if (obj.calendar.month !== undefined) {
				CALENDAR.instance.SetTime(obj.calendar.month, obj.calendar.date, obj.calendar.weekday, obj.calendar.daytime, obj.calendar.remaining_time, obj.calendar.holiday);
			}
			else if (obj.calendar.daytime !== undefined) {
				CALENDAR.instance.SetDaytime(obj.calendar.daytime);
			}
		}

		if (obj.discovery !== undefined && !_scenario) {
			GUI.instance.Acquired(obj.discovery.label, obj.discovery.category);
		}

		if (obj.spells !== undefined) {
			for (let spellRef in obj.spells) {
				_spells[spellRef] = obj.spells[spellRef];
			}
			MENU.Spells.Refresh();
		}

		if (obj.skills !== undefined) {
			for (let skillRef in obj.skills) {
				_skills[skillRef] = obj.skills[skillRef];
			}
			MENU.Skills.Refresh();
		}

		if (obj.menu !== undefined) {
			if (obj.menu.crafting === null) {
				MENU.Crafting.Close();
			}
			else if (obj.menu.crafting) {
				MENU.Crafting.Open(obj.menu.crafting.item !== undefined ? obj.menu.crafting : Object.assign(obj.menu.crafting, { item: _inventory.crafting ? _inventory.crafting : null }));
			}

			if (obj.menu.inspect) {
				MENU.Inspect.Update(obj.menu.inspect);
			}

			if (obj.menu.myself) {
				MENU.Myself.Update(obj.menu.myself);
			}

			switch (obj.menu.refresh) {
				case "yourself":
					MENU.Myself.Refresh();
					break;
				case "inspect":
					MENU.Inspect.Refresh(obj.menu.username);
					break;
				case "social":
					MENU.Social.Refresh();
					break;
			}
		}

		if (obj.locations) {
			if (obj.locations.discovered !== undefined) {
				_discoveredLocations = obj.locations.discovered;
			}
			if (obj.locations.faculty !== undefined) {
				_enabledFacultyLocations = obj.locations.faculty;
			}
			if (obj.locations.events !== undefined) {
				_turnEvents = obj.locations.events;
			}
			if (obj.locations.usedInteractions !== undefined) {
				_usedInteractions = obj.locations.usedInteractions || {};
			}
			MENU.Location.Refresh();
		}

		if (obj.location !== undefined) {
			if (obj.location.countdowns !== undefined) {
				obj.countdowns = obj.location.countdowns;
				delete obj.location.countdowns;
			}
			if (obj.location.status !== undefined) {
				obj.status = obj.location.status;
				delete obj.location.status;
			}
			if (obj.location.tokens !== undefined) {
				obj.tokens = obj.location.tokens;
				delete obj.location.tokens;
			}
			LOCATION.instance.Sync(obj.location);
		}

		if (obj.tokens != undefined) {
			const prevActions = _actions.actions;
			const prevSpells = _actions.spells;

			if (obj.tokens.maxActions !== undefined) {
				_actions.maxActions = obj.tokens.maxActions;
			}

			if (obj.tokens.maxSpells !== undefined) {
				_actions.maxSpells = obj.tokens.maxSpells;
			}

			if (obj.tokens.actions !== undefined) {
				const difference = obj.tokens.actions - _actions.actions;
				_actions.actions = obj.tokens.actions;
				GUI.instance.UpdateToken("actions", obj.tokens.actions, _actions.maxActions, difference);
			}

			if (obj.tokens.spells !== undefined) {
				const difference = obj.tokens.spells - _actions.spells;
				_actions.spells = obj.tokens.spells;
				GUI.instance.UpdateToken("spells", obj.tokens.spells, _actions.maxSpells, difference);
			}


			if (prevSpells > 0 && _actions.spells == 0 ||
				prevActions > 0 && _actions.actions == 0 ||
				prevSpells == 0 && _actions.spells > 0 ||
				prevActions == 0 && _actions.actions > 0
			) {
				MENU.Spells.Refresh();
			}

			if (obj.tokens.cards !== undefined) {
				GUI.instance.UpdateToken("cards", obj.tokens.cards, 0, 0);
			}
		}

		if (obj.status !== undefined) {
			GUI.instance.SetPlayerStatus(obj.status);
			MENU.Myself.menu ? GUI.instance.ShowPlayerStatus() : null;
		}

		if (obj.friends || obj.ignored || obj.recentCharacters) {
			if (obj.friends !== undefined) {
				for (i = 0; i < obj.friends.length; i++) {
					if (obj.friends[i]) {
						for (let username in obj.friends[i]) {
							if (obj.friends[i][username]) {
								if (_friends[i][username]) {
									Object.assign(_friends[i][username], obj.friends[i][username]);
								}
								else {
									_friends[i][username] = obj.friends[i][username];
								}
								if (_friends[i][username].username_color === undefined) {
									if (_colorsByUsername[username] !== undefined) {
										_friends[i][username].username_color = _colorsByUsername[username];
									}
								}
								else if (_friends[i][username].username_color !== undefined) {
									_colorsByUsername[username] = _friends[i][username].username_color;
									for (let l = 0; l < _recentCharacters.length; l++) {
										if (_recentCharacters[l].username.toLowerCase() == username.toLowerCase()) {
											_recentCharacters[l].username_color = _friends[i][username].username_color;
										}
									}
									for (let prop in _ignoredUsers) {
										if (prop.toLowerCase() == username.toLowerCase()) {
											_ignoredUsers[prop].username_color = _friends[i][username].username_color;
											break;
										}
									}
								}
								if (i == 2 && _friends[i][username].last_online === undefined) {
									_friends[i][username].last_online = Date.now();
								}
							}
							else if (username in _friends[i]) {
								delete _friends[i][username];
							}
						}
					}
				}
			}

			if (obj.ignored !== undefined) {
				for (let prop in obj.ignored) {
					if (obj.ignored[prop]) {
						_ignoredUsers[prop] = obj.ignored[prop];
					}
					else if (prop in _ignoredUsers) {
						delete _ignoredUsers[prop];
					}
				}
			}

			if (obj.recentCharacters !== undefined) {
				_recentCharacters = obj.recentCharacters;
			}

			GUI.instance.SetFriendRequests(Object.keys(_friends[1]).length);
			GUI.instance.SetOnlineFriends(Object.keys(_friends[3]).length);
			MENU.Social.Refresh();
		}

		if (obj.settings !== undefined) {
			if (obj.settings.filter) {
				Object.assign(_filter, obj.settings.filter);
				MENU.Settings.UpdateFilter(_filter);
			}
			if (obj.settings.roleplaying !== undefined) {
				_settings.roleplaying = obj.settings.roleplaying;
				MENU.Settings.UpdateRoleplaying(_settings.roleplaying);
				GUI.instance.SetPlayerStatus({ roleplaying: _settings.roleplaying });
			}
			if (obj.settings.autoWear !== undefined) {
				_settings.autoWear = obj.settings.autoWear;
				MENU.Settings.UpdateAutoWear(_settings.autoWear);
			}
			if (obj.settings.usernameColor !== undefined) {
				_settings.usernameColor = obj.settings.usernameColor;
				MENU.Settings.UpdateUsernameColor(_settings.usernameColor);
			}
			if (obj.settings.allowedMessages !== undefined) {
				_settings.allowedMessages = obj.settings.allowedMessages;
				MENU.Settings.UpdateAllowedMessages(_settings.allowedMessages);
			}
		}

		if (obj.countdowns !== undefined) {
			for (let target in obj.countdowns) {
				for (let type in obj.countdowns[target]) {
					if (obj.countdowns[target][type]) {
						new COUNTDOWN.Countdown(obj.countdowns[target][type].remainingTime, target, type);
					}
					else {
						for (let i = 0; i < COUNTDOWN.list.length; i++) {
							if (COUNTDOWN.list[i].type == type && COUNTDOWN.list[i].target == target) {
								COUNTDOWN.list[i].RemoveSelf();
							}
						}
					}
				}
			}
		}

		if (obj.location !== undefined || obj.scenario !== undefined) {
			LOCATION.instance.location.style.pointerEvents = _scenario || LOCATION.instance.opponent ? "none" : "";
		}

		if (obj.gui !== undefined) {
			onGUI(obj.gui, obj.callbackId);
		}
	};

	const onGUI = async (gui, callbackId) => {
		await LOCK.Lock(this);
		try {
			next(await GUI.instance[gui.action].apply(null, gui.params), callbackId);
		}
		finally {
			LOCK.Unlock(this);
		}
	};

	const next = (response, callbackId) => _ws.send(JSON.stringify({ action: "scenario", response: response, callbackId: callbackId }));

	this.IsIgnored = (username) => _ignoredUsers[username.toLowerCase()] !== undefined;

	this.OpponentIgnored = () => LOCATION.instance.opponent && LOCATION.instance.opponent.username.toLowerCase() in _ignoredUsers;

	this.IgnoreOpponent = (ignored) => LOCATION.instance.opponent ? this.IgnoreUser(LOCATION.instance.opponent.username, ignored) : null;

	this.UnignoreUser = (username) => this.IgnoreUser(username, false);

	this.IgnoreUser = (username, ignored = true) => this.Send({ action: "Ignore", user: username, ignored: ignored });

	this.RemoveFriend = (username) => this.AddFriend(username, true);

	this.AddFriend = (username, removed = false, friendToken = null) => this.Send({ action: "Friend", user: username, removed: removed, token: friendToken });

	this.AcceptFriend = (username) => this.Send({ action: "Friend", user: username, accepted: true });

	this.Invite = (username, request = false) => this.Send({ action: "Invite", user: username, request: request });

	this.InvitationResponse = (username, request, accepted) => this.Send({ action: "Invite", user: username, request: request, accepted: accepted, response: true });

	this.GetKnownUsernames = () => {
		let usernames = [];
		for (let i = 0; i < _friends.length; i++) {
			usernames = usernames.concat(Object.getOwnPropertyNames(_friends[i]));
		}
		for (let i = 0; i < _recentCharacters.length; i++) {
			usernames.push(_recentCharacters[i].username);
		}
		return unique(usernames);
	};

	this.DiscardItem = async (itemId) => {
		if (await GUI.instance.Alert("Are you sure you want to throw away this item?", "Throw Away")) {
			this.Send({ action: "Item", discard: true, itemId: itemId });
			return true;
		}
		return false;
	};

	this.HasMeterials = (materials) => {
		if (materials) {
			for (let prop in materials) {
				let count = materials[prop] > 1 ? materials[prop] : 1;
				for (let itemId in _items) {
					if (_baseItems[_items[itemId].base].item_name == prop) {
						count -= Math.max(1, _items[itemId].stack);
						if (count <= 0) {
							break;
						}
					}
				}
				if (count > 0) {
					return false;
				}
			}
		}
		return true;
	};

	this.GetItemBySlot = (slot) => {
		const item = _equipment.items[typeof slot === "number" ? slot : MENU.Inventory.equipmentSlots.indexOf(slot)];
		console.log(item);
		return item;
	};

	this.IsWorn = (itemId) => itemId && _equipment.worn[_equipment.items.indexOf(itemId)] == true;

	this.IsEquipped = (itemId) => itemId && _equipment.items.includes(itemId);

	this.HasSpell = (spell) => {
		if (typeof spell === "number") {
			for (let spellRef in _spells) {
				if (_spells[spellRef].id === spell) {
					return true;
				}
			}
			return false;
		}
		return _spells[nameToRef(spell)];
	};

	this.EquipItem = (itemId, slot) => this.Send({ action: "Item", equip: true, itemId, slot });

	this.UnequipItem = (itemId) => this.Send({ action: "Item", equip: false, itemId });

	this.GetItem = (itemId) => _items[itemId] !== undefined && _baseItems[_items[itemId].base] !== undefined ? Object.assign({}, _items[itemId], { base: _baseItems[_items[itemId].base], stack: Math.max(1, _items[itemId].stack) }) : null;

	this.UnstackItem = (itemId, stack) => this.Send({ action: "item", itemId, stack });

	this.StackItems = (from, to, remainder) => {
		_items[to].stack = Math.max(1, _items[to].stack) + Math.max(1, _items[from].stack) - remainder;
		_items[from].stack = remainder;
		if (_items[from].stack <= 0) {
			delete _items[from];
		}
		this.Send({ action: "Inventory", from, to });
		MENU.Inventory.Refresh();
	};

	this.UpdateInventory = (inventoryImage) => {
		let obj = {};

		for (let prop in inventoryImage) {
			if (Array.isArray(_inventory[prop])) {
				const maxSize = Math.max(_inventory[prop].length, inventoryImage[prop].length);
				let changed = false;

				for (let i = 0; i < maxSize && !changed; i++) {
					changed = _inventory[prop].length < i && inventoryImage[prop][i] || _inventory[prop][i] != inventoryImage[prop][i];
				}

				if (changed) {
					_inventory[prop] = inventoryImage[prop].slice();
					while (_inventory[prop].length > 0 && !_inventory[prop][_inventory[prop].length - 1]) {
						_inventory[prop].pop();
					}
					obj[prop] = _inventory[prop];
				}
			}
			else if (_inventory[prop] != inventoryImage[prop]) {
				_inventory[prop] = inventoryImage[prop];
				obj[prop] = _inventory[prop];
			}
		}

		if (Object.keys(obj).length > 0) {
			MENU.Inventory.Refresh();
			if (obj.loot !== undefined) {
				MENU.Loot.Refresh();
			}
			if (obj.crafting !== undefined) {
				const item = obj.crafting ? this.GetItem(obj.crafting) : null;
				MENU.Crafting.SetItem(item);
				GUI.instance.SetCraftingBench(item != null);
			}
			this.Send({ action: "Inventory", inventory: obj });
		}
	}

	this.HoldItem = (itemId) => {
		const image = this.GetInventoryImage();
		this.ClearInventoryImage(image, itemId);
		image.held = itemId;
		this.UpdateInventory(image);
	};

	this.GetInventoryImage = () => {
		const image = Object.assign({}, _inventory);
		for (let prop in image) {
			if (Array.isArray(image[prop])) {
				image[prop] = image[prop].slice();
			}
		}
		return image;
	};

	this.ClearInventoryImage = (image, itemId) => {
		for (let prop in image) {
			if (Array.isArray(image[prop])) {
				let index = image[prop].indexOf(itemId);
				if (index >= 0) {
					do {
						image[prop][index] = null;
					}
					while ((index = image[prop].indexOf(itemId, index)) >= 0);
				}
			}
			else if (image[prop] == itemId) {
				image[prop] = null;
			}
		}
	};

	this.GetLoot = () => this.hasLoot && this.GetItem(_inventory.loot[0]);

	this.GetCraftingBench = () => _inventory.crafting && this.GetItem(_inventory.crafting);

	this.WearItem = (itemId, wear = true) => this.IsWorn(itemId) != wear && this.Send({ action: "Item", wear: wear, itemId: itemId });

	this.GetEnchantments = (itemId) => _enchantments && _enchantments[itemId] !== undefined ? _enchantments[itemId] : null;

	this.CastSpell = (spellId, isSelf, value, variant = null) => !this.IsDazed() ? this.Send({ action: "Spell", spellId: spellId, isSelf: isSelf, value: value, variant: variant }) : null;

	this.SpecialAction = (type, label) => label ? this.Send({ action: "special", [type]: label }) : null;

	this.GetUsedInteractions = npc => _usedInteractions[npc] !== undefined ? _usedInteractions[npc].slice() : [];

	this.InScenario = () => _scenario;

	this.IsDazed = (target) => {
		if (target === undefined) {
			target = "player";
		}
		for (let i = 0; i < COUNTDOWN.list.length; i++) {
			if (COUNTDOWN.list[i].type == "dazed" && COUNTDOWN.list[i].target == target) {
				return true;
			}
		}
		return false;
	};

	this.Evaluate = (str) => {
		if (str.replace(/\s/g, '').substring(0, 4) != "()=>") {
			return str;
		}
		let func = new Function('target', `"use strict"; return ${str.substring(str.indexOf(">") + 1)}`);
		return func(MENU.Spells.target);
	};

	this.Send = () => { };

	this.Api = (url, params = null, symbol = true) => this.Request(`/api/${url}`, params, symbol);

	this.Request = (url, params = null, symbol = true) => new Promise((resolve, reject) => {
		displaySymbol(symbol);

		const urlRequest = new XMLHttpRequest();
		urlRequest.open("POST", url, true);
		urlRequest.overrideMimeType("application/json");
		urlRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		urlRequest.onerror = (e) => {
			displaySymbol(false);
			reject(e.message);
		}

		urlRequest.onload = () => {
			let obj;

			displaySymbol(false);

			try {
				obj = JSON.parse(urlRequest.responseText);
				if (GAME_MANAGER.DEV_ENV) {
					console.log(obj);
				}
				if (obj.message) {
					console.log(obj.message);
				}
			}
			catch (e) {
				if (GAME_MANAGER.DEV_ENV) {
					console.error(e);
					console.log(urlRequest.responseText);
				}
				reject(e);
			}

			if (obj) {
				resolve(obj);
			}
		};

		params = params ? Object.assign(params, getSession()) : getSession();

		const arr = [];

		for (let prop in params) {
			arr.push(`${prop}=${params[prop]}`);
		}

		urlRequest.send(arr.join('&'));
	});

	const displaySymbol = display => {
		if (_loadSymbol) {
			if (display) {
				_loadSymbol.classList.add("visible");
			}
			else {
				_loadSymbol.classList.remove("visible");
			}
		}
	};
};
