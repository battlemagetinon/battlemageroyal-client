function Random(_seed = 1) {
    if (typeof _seed === "string") {
        let hash = 0;
        for (let i = 0; i < str.length; i++) {
            hash = ((hash << 5) - hash) + str.charCodeAt(i);
            hash |= 0;
        }
        _seed = hash;
    }

    this.NextNumber = () => Math.abs(Math.sin(_seed++) * 10000 % 1);
	this.PrevNumber = () => Math.abs(Math.sin(_seed - 1) * 10000 % 1);
}
