(() => {
    NOTIFICATION = {
        WordsPerMinute: 200,
        NotificationInterval: 1000,
        Timeout: 3000,
        Type: {
            FriendRequest: 0,
            Online: 1,
            PrivateMessage: 2,
            Meetup: 3,
        },
        browserSupport: "Notification" in window,
    };

    let _instance;
    Object.defineProperty(NOTIFICATION, "instance", {
        get: () => {
            if (!_instance) {
                _instance = new NOTIFICATION.Notification();
            }
            return _instance;
        }
    });

    const _options = {
        badge: "https://battlemageroyal.com/assets/favicon/favicon.ico",
        icon: "https://battlemageroyal.com/game/assets/gui/default_icon.png",
    };

    NOTIFICATION.Notification = function () {
        let _timeout;

        let _notifications = document.getElementById("notifications");
        let _pendingNotifications = [];
        let _id = 0;

        this.FriendRequest = async (params) => {
            TITLE.Message("Friend request");

            if (SETTINGS.Get("browser_notification_on_friend_request", true) &&
                this.BrowserNotification("Friend Request", { body: `New friend request from ${params.character} (${params.username})` }, () => MENU.Social.Open()) !== false
            ) {
                return;
            }

            let elm = document.createElement("div");
            elm.className = "friend_request";

            let div = document.createElement("div");
            div.textContent = "Friend request";
            elm.appendChild(div);

            let content = document.createElement("div");
            content.className = "box";
            elm.appendChild(content);

            div = document.createElement("div");
            div.textContent = params.username;
            content.appendChild(div);

            if (params.username_color) {
                div.style.color = params.username_color;
            }

            div = document.createElement("div");
            div.textContent = params.character;
            div.className = params.nature;
            content.appendChild(div);

            div = document.createElement("div");

            let span = document.createElement("span");
            span.className = "accept";
            span.onclick = async () => {
                GAME_MANAGER.instance.AcceptFriend(params.username)
                await waitForFrame();
                elm.parentNode ? elm.parentNode.removeChild(elm) : null;
            };
            div.appendChild(span);

            span = document.createElement("span");
            span.className = "reject";
            span.onclick = async () => {
                GAME_MANAGER.instance.RemoveFriend(params.username);
                await waitForFrame();
                elm.parentNode ? elm.parentNode.removeChild(elm) : null;
            };
            div.appendChild(span);
            elm.appendChild(div);

            return displayNotification(elm);
        };

        this.Meetup = async (params) => {
            if (SETTINGS.Get("browser_notification_on_meetup", true)) {
                if (params.request) {
                    this.BrowserNotification("Meetup Request", { body: `${params.character} (${params.username}) has made a request to join your location}` });
                }
                else {
                    this.BrowserNotification("Meetup Invitation", { body: `${params.character} (${params.username}) has invited you to join their location` });
                }
            }

            let elm = document.createElement("div");
            elm.className = "meetup";

            let div = document.createElement("div");
            div.textContent = params.request ? "Meetup Request" : "Meetup Invitation";
            elm.appendChild(div);

            let content = document.createElement("div");
            content.className = "box";
            elm.appendChild(content);

            div = document.createElement("div");
            div.textContent = params.username;
            content.appendChild(div);

            if (params.username_color) {
                div.style.color = params.username_color;
            }

            div = document.createElement("div");
            div.textContent = params.character;
            div.className = params.nature;
            content.appendChild(div);

            div = document.createElement("div");

            let span = document.createElement("span");
            span.className = "accept";
            span.onclick = async () => {
                GAME_MANAGER.instance.InvitationResponse(params.username, params.request, true);
                await waitForFrame();
                elm.parentNode ? elm.parentNode.removeChild(elm) : null;
            };
            div.appendChild(span);

            span = document.createElement("span");
            span.className = "decline";
            span.onclick = async () => {
                GAME_MANAGER.instance.InvitationResponse(params.username, params.request, false);
                await waitForFrame();
                elm.parentNode ? elm.parentNode.removeChild(elm) : null;
            };
            div.appendChild(span);
            elm.appendChild(div);

            return displayNotification(elm);
        };

        this.PrivateMessage = async (params) => {
            TITLE.Message("New private message");

            if (SETTINGS.Get("browser_notification_on_private_message", true) &&
                this.BrowserNotification(`New private message from ${params.sender}`, { body: params.text }, () => MENU.Messages.Open(params.sender)) !== false
            ) {
                return;
            }

            let elm = document.createElement("div");
            elm.className = "message";

            let div = document.createElement("div");
            div.textContent = "New message";
            elm.appendChild(div);

            div = document.createElement("div");
            elm.appendChild(div);

            let span = document.createElement("span");
            span.textContent = "From: ";
            div.appendChild(span);

            span = document.createElement("span");
            span.textContent = params.sender;
            div.appendChild(span);

            div = document.createElement("div");
            div.className = "box";
            div.textContent = params.text;
            elm.appendChild(div);

            elm.onclick = async (e) => {
                if (!e.defaultPrevented) {
                    MENU.Messages.Open(params.sender);
                    await waitForFrame();
                    elm.parentNode ? elm.parentNode.removeChild(elm) : null;
                }
            };

            return displayNotification(elm);
        };

        this.FriendOnline = async (params) => {
            TITLE.Message("Friend online");

            if (SETTINGS.Get("browser_notification_on_friend_online", true) &&
                this.BrowserNotification("Friend Online", { body: `${params.character} (${params.username}) has come online` }, () => MENU.Social.Open()) !== false
            ) {
                return;
            }

            let elm = document.createElement("div");
            elm.className = "online";

            let div = document.createElement("div");
            div.textContent = "Online";
            elm.appendChild(div);

            let content = document.createElement("div");
            content.className = "box";
            elm.appendChild(content);

            div = document.createElement("div");
            div.textContent = params.username;
            content.appendChild(div);

            if (params.username_color) {
                div.style.color = params.username_color;
            }

            div = document.createElement("div");
            div.textContent = params.character;
            div.className = params.nature;
            content.appendChild(div);

            div = document.createElement("div");
            elm.appendChild(div);

            elm.onclick = async (e) => {
                if (!e.defaultPrevented) {
                    MENU.Social.Open();
                    await waitForFrame();
                    elm.parentNode ? elm.parentNode.removeChild(elm) : null;
                }
            };

            return displayNotification(elm);
        }

        this.ChatMessage = (sender, channel, message) => {
            TITLE.Message("New chat message");
            if (SETTINGS.Get("browser_notification_on_chat_message", true)) {
                if (Array.isArray(message)) {
                    let str = "";
                    for (let i = 0; i < message.length; i++) {
                        switch (message[i].channel) {
                            case LOCAL_CHAT.Channel.OOC:
                                str += `(${message[i].text})`;
                                break;
                            case LOCAL_CHAT.Channel.Emote:
                                str += `*${message[i].text}*`;
                                break;
                            default:
                                str += message[i].text;
                                break;
                        }
                    }
                    message = str;
                }
                else {
                    switch (channel) {
                        case LOCAL_CHAT.Channel.OOC:
                            message = `(${message})`;
                            break;
                        case LOCAL_CHAT.Channel.Emote:
                            message = `*${message}*`;
                            break;
                    }
                }
                return this.BrowserNotification(`New chat message from ${sender}`, { body: message });
            }
        };

        this.CombatMessage = message => {
            TITLE.Message("Entered combat!");
            if (SETTINGS.Get("browser_notification_on_combat_log", true)) {
                return this.BrowserNotification(`Combat log`, { body: message });
            }
        };

        this.Encounter = (characterNames, username) => {
            TITLE.Message("Entered encounter!");
            if (SETTINGS.Get("browser_notification_on_encounter", true)) {
                return this.BrowserNotification(`Entered encounter`, { body: `${characterNames} (${username}) has entered the same location as you` });
            }
        };

        this.EncounterEnded = (characterNames, username) => {
            if (SETTINGS.Get("browser_notification_on_encounter_ended", true)) {
                return this.BrowserNotification(`Encounter ended`, { body: `${characterNames} (${username}) has left your location` });
            }
        };

        const displayNotification = async (elm, displayTime = 0) => {
            _pendingNotifications.push(elm);

            clearTimeout(_timeout);

            if (_notifications.classList.contains("inactive")) {
                await waitUntil(() => !_notifications.classList.contains("inactive"));
            }

            await waitUntil(() => _pendingNotifications.length == 0 || _pendingNotifications[0] == elm);

            if (_pendingNotifications.length > 0) {
                elm.classList.add("inactive");
                elm.classList.add(`notification_${_id++ % 3}`);

                let closeButton = document.createElement("div");
                closeButton.className = "close";
                closeButton.onclick = async (e) => {
                    e.preventDefault();
                    await waitForFrame();
                    elm.parentNode ? elm.parentNode.removeChild(elm) : null;
                };
                elm.appendChild(closeButton);

                _notifications.appendChild(elm);

                await waitForFrames();
                elm.classList.remove("inactive");
                await transitionEnded(elm);
                await wait(60000 / NOTIFICATION.WordsPerMinute * countWords(elm.textContent) + NOTIFICATION.NotificationInterval);
                _pendingNotifications.shift();
            }

            if (_pendingNotifications.length == 0) {
                _timeout = setTimeout(() => NOTIFICATION.instance.RemoveAll(), NOTIFICATION.Timeout);
            }
        };

        this.BrowserNotification = (title, options, onclick) => {
            if (!document.hasFocus() && NOTIFICATION.browserSupport && Notification.permission === "granted" && SETTINGS.Get("browser_notifications", false)) {
                let notification = new Notification(title, options ? Object.assign({}, _options, options) : _options);
                notification.onclick = () => {
                    window.focus();
                    onclick && onclick();
                    notification.close();
                };
                return notification;
            }
            return false;
        };

        this.RemoveAll = async () => {
            _notifications.classList.add("inactive");
            await transitionEnded(_notifications);
            clearHTML(_notifications);
            _notifications.classList.remove("inactive");
        };

        const countWords = (str) => str.trim().split(/\s+/).length;
    }
})();
