(() => {
	CONTROLS = {
		HasTouch: () => 'ontouchstart' in document.documentElement,
		GetHotkeysByLabel: () => Object.assign({}, _defaultHotkeysByLabel, SETTINGS.Get("hotkeys")),
		InputToHokey: (e) => {
			const keys = [];
			if (e.ctrlKey) {
				keys.push("Ctrl");
			}
			if (e.altKey) {
				keys.push("Alt");
			}
			if (e.shiftKey) {
				keys.push("Shift");
			}
			keys.push(e.key.toUpperCase());
			return keys.join("+");
		},
	};

	const _defaultHotkeysByLabel = {
		'Myself': "C",
		'Inventory': "I",
		'Spells': "S",
		'Skills': "K",
		'Social': "O",
		'Messages': "M",
		'Settings': "T",
		'Location': "L",
		'Inspect Opponent': "Alt+1"
	}

	const _actionsByLabel = {
		'Myself': () => MENU.Myself.Toggle(),
		'Inventory': () => MENU.Inventory.Toggle(),
		'Spells': () => MENU.Spells.Toggle(),
		'Skills': () => MENU.Skills.Toggle(),
		'Social': () => MENU.Social.Toggle(),
		'Messages': () => MENU.Messages.Toggle(),
		'Settings': () => MENU.Settings.Toggle(),
		'Location': () => MENU.Location.Toggle(),
		'Inspect Opponent': () => LOCATION.instance.opponent && MENU.Inspect.Toggle(GUI.Position.Right, LOCATION.instance.opponent.token),
	}

	const _systemActionsByInput = {
		'ESCAPE': (e) => {
			TOOLTIP.instance.Hide();
			if (!GUI.instance.CloseAlert() && !UNSTACK.instance.Hide()) {
				if (CURSOR.instance.usingItem) {
					CURSOR.instance.SetDefault();
				}
				else if (MENU.Loot.menu) {
					MENU.Loot.Close();
				}
				else if (MENU.MenuIsOpened()) {
					MENU.CloseAll();
				}
				else {
					GUI.instance.ExitAlert();
				}
				e.stopImmediatePropagation();
			}
		},
		'ENTER': async (e) => {
			if (!GAME_MANAGER.instance.InScenario()) {
				e.stopImmediatePropagation();
				if (LOCATION.instance.player) {
					await LOCAL_CHAT.player.Show();
					LOCAL_CHAT.player.input.focus();
				}
			}
		},
	};

	const _isDownByKey = {};

	document.addEventListener('keydown', (e) => {
		if (_isDownByKey[e.key]) {
			return;
		}

		_isDownByKey[e.key] = true;

		if (document.activeElement === document.body) {
			let input = CONTROLS.InputToHokey(e);

			if (_systemActionsByInput[input]) {
				_systemActionsByInput[input](e);
			}
			else {
				let hotkeysByLabel = CONTROLS.GetHotkeysByLabel();
				for (let label in hotkeysByLabel) {
					if (hotkeysByLabel[label] == input) {
						_actionsByLabel[label]();
						e.stopImmediatePropagation();
						break;
					}
				}
			}
		}
		else if (e.key === "Escape" && document.activeElement) {
			document.activeElement.blur();
		}
	});

	document.addEventListener('keyup', (e) => {
		switch (e.key.toUpperCase()) {
			case "SHIFT":
				if (CURSOR.instance.usingItem) {
					CURSOR.instance.SetDefault();
				}
				break;
		}
		_isDownByKey[e.key] = false;
	});
})();

