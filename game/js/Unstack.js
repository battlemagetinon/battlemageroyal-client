(() => {
    UNSTACK = {};

    let _instance;
    Object.defineProperty(UNSTACK, "instance", {
        get: () => {
            if (!_instance) {
                _instance = new UNSTACK.Unstack();
            }
            return _instance;
        }
    });
})();

UNSTACK.Unstack = function () {
    const _unstack = document.getElementById("unstack");
    const _datalist = document.getElementById("stack_sizes");
    const _input = _unstack.getElementsByTagName("input")[0];
    const _button = unstack.getElementsByClassName("button")[0];

    const _remainder = _unstack.getElementsByTagName("span")[0];
    const _newStackSize = _unstack.getElementsByTagName("span")[1];

    let _x;
    let _y;
    let _item;
    let _node;

    _input.addEventListener('input', () => this.Update());
    _input.addEventListener('change', () => this.Update());

    _button.onclick = (e) => this.Take(e);

    this.Show = async (e, item) => {
        _x = e.pageX !== undefined ? e.pageX : e.targetTouches[0].pageX;
        _y = e.pageY !== undefined ? e.pageY : e.targetTouches[0].pageY;
        _item = item;
        _node = e.target;

        _unstack.style.left = "";
        _unstack.style.top = "";

        const rect = container.getBoundingClientRect();

        if (_x - rect.x + _unstack.offsetWidth > container.offsetWidth) {
            _unstack.style.left = `${(_x - rect.x - _unstack.offsetWidth) / container.offsetWidth * 100}%`;
        }
        else {
            _unstack.style.left = `${(_x - rect.x) / container.offsetWidth * 100}%`;
        }

        if (_y - rect.y + _unstack.offsetHeight > container.offsetHeight) {
            _unstack.style.top = `${(_y - rect.y - _unstack.offsetHeight) / container.offsetHeight * 100}%`;
        }
        else {
            _unstack.style.top = `${(_y - rect.y) / container.offsetHeight * 100}%`;
        }

        await waitForFrame();

        _input.max = item.stack;
        _input.value = Math.floor(item.stack / 2);

        this.Update();

        clearHTML(_datalist);

        for (let i = 0; i <= item.stack; i++) {
            const option = document.createElement("option");
            option.value = i;
            _datalist.appendChild(option);
        }

        _unstack.classList.remove("inactive");
        container.addEventListener("click", this.Hide);
        TOOLTIP.instance.Hide();
    };

    this.Update = () => {
        if (_item) {
            _newStackSize.innerHTML = _input.value;
            _remainder.innerHTML = _item.stack - _input.value;
        }
    };

    this.IsActive = () => !_unstack.classList.contains("inactive");

    this.Hide = (e) => {
        if (e === undefined || e.target != _unstack && !isDescendant(_unstack, e.target)) {
            container.removeEventListener("click", this.Hide);
            if (!_unstack.classList.contains("inactive")) {
                _unstack.classList.add("inactive");
                return true;
            }
        }
        return false;
    }

    this.Take = (e) => {
        this.Hide();
        if (_input.value == 0 || !_item || _input.value == _item.stack) {
            if (_input.value == _item.stack) {
                if (_node && _node.parentNode) {
                    _node.parentNode.removeChild(_node);
                }
                GAME_MANAGER.instance.HoldItem(_item.id);
                CURSOR.instance.SetItem(_item);
            }
        }
        else {
            GAME_MANAGER.instance.UnstackItem(_item.id, _input.value);
        }
    };
}