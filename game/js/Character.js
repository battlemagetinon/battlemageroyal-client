
CHARACTER = {
	GetId: character => character.username ? `u:${character.username}` : (character.names || "???").replace(" ", "."),
	GetGreeting: character => character.greeting && (Array.isArray(character.greeting) ? character.greeting[Math.floor(Math.random() * character.greeting.length)] : character.greeting),
	GetFarewell: character => character.farewell && (Array.isArray(character.farewell) ? character.farewell[Math.floor(Math.random() * character.farewell.length)] : character.farewell),
	GetSprite: character => `assets/characters/${character.sprite ? character.sprite + (character.expression !== undefined ? `_${character.expression}` : '') : `${character.gender}_${character.nature}`}.png`,
};

CHARACTER.Character = function (_obj) {
	_obj = Object.assign({ names: "???" }, _obj);

	const _id = CHARACTER.GetId(_obj);

	Object.defineProperties(this, {
		'names': {
			get: () => _obj.names
		},
		'firstName': {
			get: () => _obj.name || _obj.names.split(" ")[0],
		},
		'lastName': {
			get: () => _obj.surname || _obj.names.split(" ")[1],
		},
		'username': {
			get: () => _obj.username,
		},
		'nature': {
			get: () => _obj.nature
		},
		'gender': {
			get: () => _obj.gender,
			set: (gender) => _obj.gender = gender
		},
		'options': {
			get: () => _obj.options,
		},
		'token': {
			get: () => _obj.token
		},
		"id": {
			get: () => _id
		},
		"player": {
			get: () => _obj.player
		},
		"e": {
			get: () => {
				switch (_obj.gender) {
					case "male":
						return "he";
					case "female":
						return "she";
					default:
						return "they";
				}
			}
		},
		"em": {
			get: () => {
				switch (_obj.gender) {
					case "male":
						return "him";
					case "female":
						return "her";
					default:
						return "them";
				}
			}
		},
		"eir": {
			get: () => {
				switch (_obj.gender) {
					case "male":
						return "his";
					case "female":
						return "her";
					default:
						return "their";
				}
			}
		},
		"eirs": {
			get: () => {
				switch (_obj.gender) {
					case "male":
						return "his";
					case "female":
						return "hers";
					default:
						return "theirs";
				}
			}
		},
		"eirself": {
			get: () => {
				switch (_obj.gender) {
					case "male":
						return "himself";
					case "female":
						return "herself";
					default:
						return "themself";
				}
			}
		},
	});

	this.HasSprite = () => _obj.sprite != null;

	this.GetGreeting = () => CHARACTER.GetGreeting(_obj);

	this.GetFarewell = () => CHARACTER.GetFarewell(_obj);

	this.GetSprite = () => CHARACTER.GetSprite(_obj);

	this.toString = () => this.names;

	GUI.instance.PreloadImage(CHARACTER.GetSprite(_obj));
};
