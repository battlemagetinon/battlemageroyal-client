(() => {
	ACTION_HUB = {
		RADIUS: 90,
		OFFSET: 80,
		TENSION: 1,
		State: {
			Default: 0,
			Open: 1,
			Closing: 2,
			Closed: 3,
			Reopening: 4
		},
		flipXPosition: function (i) {
			switch (i) {
				case 0:
					return 6;
				case 1:
					return 5;
				case 2:
					return 4;
				case 6:
					return 0;
				case 5:
					return 1;
				case 4:
					return 2;
				default:
					return i;
			}
		}
	};
})();

ACTION_HUB.ActionHub = function (_x, _y, _nature, _flip, _actions) {
	const _parent = document.getElementById("actions");
	const _elm = document.createElement("div");
	const _center = document.createElement("div");
	const _ul = document.createElement("ul");

	let _li;
	let _i;
	let _width;
	let _radius;
	let _position;
	let _scale;
	let _offset;
	let _list;
	let _closing;

	let _actionHub = this;
	let _progress = 0;
	let _start = 0;

	let _state = ACTION_HUB.State.Default;
	let _nestedActions = [];

	_elm.className = `${_nature} inactive`;
	_center.className = "center";

	_center.onclick = () => {
		_center.classList.add("selected");
		_nestedActions.pop();
		_actionHub.Reopen();
	};

	let updateActions = function () {
		let actions;
		let li;

		if (_nestedActions.length > 0) {
			actions = _nestedActions[_nestedActions.length - 1];
			_center.classList.add("back");
		}
		else {
			actions = _actions;
			_center.classList.remove("back");
		}

		clearHTML(_ul);
		_center.classList.remove("selected");

		for (let i = 0; i < actions.length; i++) {
			li = document.createElement("li");

			if (!actions[i]) {
				li.className = "inactive";
			}
			else {
				li.innerHTML = actions[i].label;

				switch (actions[i].cost) {
					case ACTION.Cost.Action:
						li.className = "star";
						break;
					case ACTION.Cost.Spell:
						li.className = "spell";
						break;
					case ACTION.Cost.SpecialAction:
						li.className = "special star";
						break;
					case ACTION.Cost.SpecialSpell:
						li.className = "special spell";
						break;
				}

				((action, li) => {
					if (!action.enabled) {
						li.classList.add("disabled");
					}
					else if (Array.isArray(action.event)) {
						li.onclick = () => {
							if (_state == ACTION_HUB.State.Open) {
								li.classList.add("selected");
								_nestedActions.push(action.event);
								_actionHub.Reopen();
							}
						};
					}
					else {
						li.onclick = () => {
							if (_state == ACTION_HUB.State.Open) {
								li.classList.add("selected");
								_actionHub.Close();
								action.event ? action.event(action) : null;
							}
						};
					}
				})(actions[i], li);
			}

			_ul.appendChild(li);
		}

		_list = _ul.getElementsByTagName("li");
	}

	_elm.appendChild(_center);
	_elm.appendChild(_ul);

	{
		const rect = _parent.getBoundingClientRect();
		_x = (_x - rect.x) * 900 / container.offsetHeight;
		_y = (_y - rect.y) * 900 / container.offsetHeight;

		for (let i = _actions.length - 1; i >= 0; i--) {
			if (!_actions[i]) {
				_actions.splice(i, 1);
			}
		}
	};

	this.Close = async function () {
		if (_state !== ACTION_HUB.State.Open) {
			return;
		}

		_state = ACTION_HUB.State.Closing;
		_start = Date.now();
		_progress = 0;

		_elm.classList.add("inactive");
		await transitionEnded(_elm);

		if (_elm.parentNode) {
			_elm.parentNode.removeChild(_elm);
		}

		_state = ACTION_HUB.State.Closed;
		return true;
	}

	this.Reopen = async function () {
		_state = ACTION_HUB.State.Reopening;
		_start = Date.now();
		_progress = 0;

		_elm.classList.add("inactive");
		_elm.classList.add("reopen");
		await transitionEnded(_elm);

		await _actionHub.Open();
		_elm.classList.remove("reopen");
	}

	this.Open = async function () {
		_parent.appendChild(_elm);

		updateActions();

		_state = ACTION_HUB.State.Default;
		_start = Date.now();
		_progress = 0;

		await waitForFrames();
		_elm.classList.remove("inactive");
		await transitionEnded(_elm);
		_state = ACTION_HUB.State.Open;
	}

	this.Update = function (now) {
		if (!_elm.parentNode) {
			return;
		}

		_closing = _state == ACTION_HUB.State.Closing || _state == ACTION_HUB.State.Reopening;
		_scale = container.offsetHeight / 900;
		_elm.style.left = `${_x * _scale}px`;
		_elm.style.top = `${_y * _scale}px`;

		_offset = ACTION_HUB.OFFSET * _scale;
		_radius = ACTION_HUB.RADIUS * _scale;

		_progress = Math.min(1, (now - _start) / (_closing ? 250 : 300));

		_scale = _closing ? (1 - _progress) - 1 : _progress - 1;
		_scale = _scale * _scale * ((ACTION_HUB.TENSION + 1) * _scale + ACTION_HUB.TENSION) + 1;

		for (_i = 0; _i < _list.length; _i++) {
			_li = _list[_i];

			_width = _li.offsetWidth;
			_position = _list.length == 1 ? 1 : _i;

			if (_flip) {
				_position = ACTION_HUB.flipXPosition(_position);
			}

			switch (_position) {
				case 0:
				case 2:
				case 4:
				case 6:
					if (_position < 4) {
						_li.style.left = `${Math.sqrt(_radius * _radius - _offset * _offset) * _scale}px`;
					}
					else {
						_li.style.left = `${- Math.sqrt(_radius * _radius - _offset * _offset) * _scale - _width}px`;
					}
					_li.style.top = `${_offset * (_position % 6 == 0 ? -1 : 1) * _scale}px`;
					break;
				case 1:
					_li.style.left = `${_radius * _scale}px`;
					break;
				case 5:
					_li.style.left = `${- _radius * _scale - _width}px`;
					break;
				case 3:
					_li.style.top = `${_offset * 2 * _scale}px`;
					break;
				case 7:
					_li.style.top = `${- _offset * 2 * _scale}px`;
					break;
				default:
					_li.style.display = "none";
					break;
			}
		}
	}

	this.IsClosing = () => _state == ACTION_HUB.State.Closing;

	this.IsClosed = () => _state == ACTION_HUB.State.Closed;

	this.Open();
}