(() => {
	ACTION = {
		Cost: {
			None: 0,
			Action: 1,
			Spell: 2,
			SpecialAction: 3,
			SpecialSpell: 4,
		}
	};
})();

ACTION.Action = function (_label, _cost, _event, _enabled = true) {
	if (Array.isArray(_event)) {
		for (let i = _event.length - 1; i >= 0; i--) {
			if (!_event[i]) {
				_event.splice(i, 1);
			}
		}
	}

	Object.defineProperties(this, {
		'label': {
			get: () => _label
		},
		'cost': {
			get: () => _cost
		},
		'enabled': {
			get: () => _enabled
		},
		'event': {
			get: () => _event
		},
	});
};
