(() => {
	let _position = document.getElementById("container").getBoundingClientRect();

	_position = _position ? { x: _position.x + _position.width / 2, y: _position.y + _position.height / 2 } : { x: 0, y: 0 };

	CURSOR = {
		DRAG_THRESHOLD: 10,
		USE_CURSOR_SCALE: 0.6667,
		VALID_SLOT_COLOR: "rgba(0, 126, 0, 0.1)",
		INVALID_SLOT_COLOR: "rgba(126, 0, 0, 0.1)",
	};

	let _instance;
	Object.defineProperty(CURSOR, "instance", {
		get: () => {
			if (!_instance) {
				_instance = new CURSOR.Cursor();
			}
			return _instance;
		}
	});

	function update(e) {
		_position = { x: e.pageX !== undefined ? e.pageX : e.targetTouches[0].pageX, y: e.pageY !== undefined ? e.pageY : e.targetTouches[0].pageY };
		_instance && _instance.UpdatePosition();
	}

	document.addEventListener("mousemove", update);
	document.addEventListener("touchmove", update);
	document.onmousedown = update;
	document.ontouchstart = update;

	CURSOR.Cursor = function () {
		let _startPosition = { x: 0, y: 0 };
		let _offset = { x: 0, y: 0 };

		let _mouseDown = false;
		let _locked = false;
		let _updating = false;

		let _item;
		let _slots;
		let _target;
		let _action;

		let _prevFirstSpace;
		let _prevSpacesCount;
		let _validPosition;
		let _container;
		let _coords;
		let _swapWith;

		let _inventoryImage;

		let _background = document.getElementById("background_wrapper");
		let _characters = document.getElementById("characters");

		let _cursor = document.createElement("div");
		_cursor.id = "cursor";
		_cursor.className = "font_size_2";

		Object.defineProperties(this, {
			'item': {
				get: () => _item
			},
			'heldItem': {
				get: () => _item && !_action
			},
			'updating': {
				get: () => _updating,
				set: updating => _updating = updating
			},
			'locked': {
				get: () => _item || _updating || _locked
			},
			'usingItem': {
				get: () => _item && _action
			},
		});

		this.GetElementUnderMouse = () => document.elementFromPoint(_position.x, _position.y);

		this.IsMouseOver = elm => {
			const elmUnderMouse = this.GetElementUnderMouse();
			return elmUnderMouse && (elmUnderMouse === elm || isDescendant(elm, elmUnderMouse));
		}

		this.WaitForDragStart = async () => {
			do {
				await waitForFrame();
				if (!_mouseDown || this.locked) {
					return false;
				}
			}
			while (Math.abs(_startPosition.x - _position.x) <= CURSOR.DRAG_THRESHOLD && Math.abs(_startPosition.y - _position.y) <= CURSOR.DRAG_THRESHOLD);
			return true;
		};

		this.SetItem = (item, action, target) => {
			_updating = false;
			_item = item;
			_action = action;
			_target = target;
			_inventoryImage = GAME_MANAGER.instance.GetInventoryImage();
			TOOLTIP.instance.Hide();
			if (item && !MENU.Inventory.menu) {
				MENU.Inventory.Open();
			}
			updateCursor();
		}

		this.ValidateItem = async () => {
			if (_item) {
				_item = GAME_MANAGER.instance.GetItem(_item.id);
				updateCursor();
			}
		};

		this.SetDefault = () => {
			if (_item) {
				_item = null;
				if (MENU.Inventory.menu) {
					let spaces = MENU.Inventory.menu.getElementsByTagName("td");
					for (let i = 0; i < spaces.length; i++) {
						spaces[i].style.background = "";
					}
				}
				this.UpdateEquipmentHighlights();
			}
			_cursor.style.opacity = 0;
			return false;
		};

		const updateCursor = (e) => {
			if (!_item) {
				return this.SetDefault(e);
			}

			const rect = { width: _item.base.width, height: _item.base.height };
			const containerScale = container.getBoundingClientRect().width / 1280;

			for (let prop in rect) {
				switch (rect[prop]) {
					case 1:
						rect[prop] = 54.3125;
						break;
					case 2:
						rect[prop] = 110.625;
						break;
					case 3:
						rect[prop] = 166.953125;
						break;
				}
				rect[prop] *= containerScale;
			}

			const scale = _target ? CURSOR.USE_CURSOR_SCALE : 1;
			_offset.x = rect.width * scale * 0.5;
			_offset.y = rect.height * scale * 0.5;
			_cursor.style.width = `${rect.width * scale}px`;
			_cursor.style.height = `${rect.height * scale}px`;
			_cursor.style.backgroundImage = `url(${IMAGE_PROCESSING.getItemImage(_item)})`;
			_cursor.style.opacity = 1;

			clearHTML(_cursor);

			if (!_target && _item.base.stack > 1 && _item.stack > 1) {
				let div = document.createElement("div");
				div.innerHTML = _item.stack;
				_cursor.appendChild(div);
			}

			_prevFirstSpace = null;

			if (!_cursor.parentNode) {
				container.appendChild(_cursor);
			}

			this.UpdateEquipmentHighlights();
			this.UpdatePosition();
		};

		this.UpdateEquipmentHighlights = () => {
			const prevSlots = _slots;
			_slots = !_item || _action || _item.base.type != "equipment" ? null : Array.isArray(_item.base.slot) ? _item.base.slot : [_item.base.slot];
			if (MENU.Myself.menu) {
				if (prevSlots) {
					for (i = 0; i < prevSlots.length; i++) {
						MENU.Myself.menu.classList.remove(prevSlots[i]);
					}
				}
				if (_slots) {
					for (i = 0; i < _slots.length; i++) {
						MENU.Myself.menu.classList.add(_slots[i]);
					}
				}
			}
		};

		const tryPlaceItem = (e) => {
			if (!_item) {
				this.SetDefault(e);
				return false;
			}
			if (GUI.instance.AlertIsOpen()) {
				return false;
			}
			if (_action) {
				if (isDescendant(_characters, e.target)) {
					if (GUI.instance.MouseOverCharacter(e) && validateTarget("character")) {
						useOnCharacter(e);
						return true;
					}
				}
				else if (validateTarget("item")) {
					let targetItem;
					if (isDescendant(MENU.Inventory.menu, e.target) && e.target.parentNode.tagName.toLowerCase() == "td") {
						let container = isDescendant(MENU.Inventory.menu.getElementsByClassName("inventory")[0], e.target) ? _inventoryImage.tab : _inventoryImage.heirlooms;
						targetItem = container[getSiblingIndex(e.target.parentNode.parentNode) * e.target.parentNode.parentNode.children.length + getSiblingIndex(e.target.parentNode)];
					}
					else if (isDescendant(MENU.Myself.menu, e.target)) {
						let slot;
						if (e.target.classList.contains("head")) {
							slot = 0;
						}
						else if (e.target.classList.contains("shirt")) {
							slot = 1;
						}
						else if (e.target.classList.contains("pants")) {
							slot = 2;
						}
						else if (e.target.classList.contains("shoes")) {
							slot = 3;
						}
						else if (e.target.classList.contains("undershirt")) {
							slot = 4;
						}
						else if (e.target.classList.contains("underpants")) {
							slot = 5;
						}
						else if (e.target.classList.contains("accessory")) {
							slot = 6 + getSiblingIndex(e.target);
						}
						if (slot >= 0) {
							targetItem = GAME_MANAGER.instance.GetItemBySlot(slot);
						}
					}
					else if (isDescendant(MENU.Loot.menu, e.target)) {
						targetItem = GAME_MANAGER.instance.hasLoot && GAME_MANAGER.instance.GetLoot().id;
					}
					else if (isDescendant(MENU.Crafting.menu, e.target)) {
						if (e.target.classList.contains("item_container") || e.target.parentNode.classList.contains("item_container")) {
							targetItem = GAME_MANAGER.instance.GetCraftingBench();
							targetItem = targetItem && targetItem.id;
						}
					}
					if (targetItem && targetItem != _item.id) {
						useOnItem(e, targetItem);
						return true;
					}
				}
				this.SetDefault(e);
				return true;
			}
			if (_validPosition && isDescendant(MENU.Inventory.menu, e.target)) {
				if (_swapWith) {
					let swapItem = GAME_MANAGER.instance.GetItem(_swapWith);
					if (swapItem) {
						if (_item.base.id == swapItem.base.id && _item.base.stack > 1 && _item.stack < _item.base.stack && swapItem.stack < swapItem.base.stack) {
							stackItems(e, swapItem);
							return true;
						}
						swapItems(e, swapItem);
						return true;
					}
				}
				placeInContainer(e);
				return true;
			}
			if (isDescendant(MENU.Myself.menu, e.target)) {
				if (_item.base.type == "equipment" && _slots) {
					for (let i = 0; i < _slots.length; i++) {
						if (e.target.classList.contains(_slots[i])) {
							return equipItem(_slots[i], e.target);
						}
					}
				}
				return false;
			}
			if (isDescendant(MENU.Loot.menu, e.target)) {
				return placeInLootContainer(e);
			}
			if (isDescendant(MENU.Crafting.menu, e.target) && (e.target.classList.contains("item_container") || e.target.parentNode.classList.contains("item_container"))) {
				return placeInCraftingBench(e);
			}
			if (e.target.parentNode == _background || isDescendant(_characters, e.target)) {
				if (GUI.instance.MouseOverCharacter(e)) {
					switch (_item.base.type) {
						case "equipment":
							return equipItem();
						case "consumable":
							return consumeItem(e);
					}
				}
				else {
					discardItem(e);
				}
			}
			return false;
		};

		const stackItems = (e, swapItem) => {
			const stack = swapItem.stack + _item.stack;
			swapItem.stack = Math.min(swapItem.base.stack, stack);
			_item.stack = Math.max(0, stack - swapItem.stack);

			GAME_MANAGER.instance.StackItems(_item.id, swapItem.id, _item.stack);

			if (!GAME_MANAGER.instance.hasLoot) {
				MENU.Loot.Close();
			}

			if (_item.stack > 0) {
				let list = [{ elm: _cursor, stack: _item.stack }, { elm: e.target, stack: swapItem.stack }];
				for (let i = 0; i < list.length; i++) {
					clearHTML(list[i].elm);
					if (list[i].stack > 1) {
						div = document.createElement("div");
						div.innerHTML = list[i].stack;
						list[i].elm.appendChild(div);
					}
				}
			}
			else {
				_swapWith = null;
				this.SetDefault(e);
			}
		};

		const useOnCharacter = (e) => {
			GAME_MANAGER.instance.Send({ action: "Item", itemId: _item.id, itemAction: _action, target: "character" });
			if (!e.shiftKey) {
				this.SetDefault(e);
			}
		};

		const useOnItem = (e, targetItem) => {
			GAME_MANAGER.instance.Send({ action: "Item", itemId: _item.id, itemAction: _action, target: "item", targetId: targetItem });
			if (!e.shiftKey) {
				this.SetDefault(e);
			}
		};

		const discardItem = async (e) => {
			if (await GAME_MANAGER.instance.DiscardItem(_item.id)) {
				_inventoryImage.held = null;
				this.SetDefault(e);
			}
		};

		const consumeItem = (e) => {
			GAME_MANAGER.instance.Send({ action: "Item", itemId: _item.id, consume: true });
			if (_item.base.stack === undefined || _item.stack - 1 == 0) {
				_inventoryImage.held = null;
				this.SetDefault(e);
			}
			return true;
		};

		const placeInContainer = (e) => {
			for (let i = 0; i < _coords.length; i++) {
				_container[_coords[i]] = _item.id;
			}
			_inventoryImage.held = null;
			GAME_MANAGER.instance.UpdateInventory(_inventoryImage);
			if (!GAME_MANAGER.instance.hasLoot) {
				MENU.Loot.Close();
			}
			this.SetDefault(e);
		};

		const swapItems = (e, swapItem) => {
			clearSpaces(_swapWith);
			for (let i = 0; i < _coords.length; i++) {
				_container[_coords[i]] = _item.id;
			}
			_item = swapItem;
			_inventoryImage.held = _item.id;
			GAME_MANAGER.instance.UpdateInventory(_inventoryImage);
			updateCursor(e);
		};

		const placeInLootContainer = (e) => {
			_swapWith = _inventoryImage.loot[0] ? _inventoryImage.loot[0] : null;
			if (_swapWith) {
				let swapItem = GAME_MANAGER.instance.GetItem(_swapWith);
				if (_item.base.id == swapItem.base.id && _item.base.stack > 1 && _item.stack < _item.base.stack && swapItem.stack < swapItem.base.stack) {
					return stackItems(e, swapItem);
				}
				clearSpaces(_swapWith);
				_inventoryImage.loot = [_item.id];
				_item = GAME_MANAGER.instance.GetItem(_swapWith);
				_inventoryImage.held = _item.id;
				updateCursor(e);
			}
			else {
				_inventoryImage.loot = [_item.id];
				_inventoryImage.held = null;
				this.SetDefault(e);
			}
			GAME_MANAGER.instance.UpdateInventory(_inventoryImage);
			return true;
		};

		const placeInCraftingBench = (e) => {
			console.log("MENU.Crafting.containerEnabled", MENU.Crafting.containerEnabled);
			if (!MENU.Crafting.containerEnabled) {
				return false;
			}
			_swapWith = _inventoryImage.crafting;
			const swapItem = _swapWith && GAME_MANAGER.instance.GetItem(_swapWith);
			if (swapItem) {
				if (_item.base.id == swapItem.base.id && _item.base.stack > 1 && _item.stack < _item.base.stack && swapItem.stack < swapItem.base.stack) {
					stackItems(e, swapItem);
					return MENU.Crafting.SetItem(swapItem);
				}
				clearSpaces(_swapWith);
				_inventoryImage.crafting = _item.id;
				_item = GAME_MANAGER.instance.GetItem(_swapWith);
				_inventoryImage.held = _item.id;
				updateCursor(e);
			}
			else {
				_inventoryImage.crafting = _item.id;
				_inventoryImage.held = null;
				this.SetDefault(e);
			}
			GAME_MANAGER.instance.UpdateInventory(_inventoryImage);
			return true;
		}

		const equipItem = (slot, obj) => {
			_updating = true;
			if (obj && slot === "accessory") {
				slot = obj.parentNode.classList.contains("accessories") ? 6 + getSiblingIndex(obj) : 0;
			}
			else {
				console.log(slot);
				slot = MENU.Inventory.equipmentSlots.indexOf(slot);
			}
			GAME_MANAGER.instance.EquipItem(_item.id, slot);
			return true;
		};

		const clearSpaces = (itemId) => GAME_MANAGER.instance.ClearInventoryImage(_inventoryImage, itemId);

		document.onmouseup = async (e) => {
			if (e.button != 0) {
				return;
			}
			_mouseDown = false;
			if (!_locked && !_updating && !_action) {
				tryPlaceItem(e);
			}
			await waitForFrame();
			_locked = false;
		};

		document.onmousedown = (e) => {
			if (e.button == 2 && _action) {
				return this.SetDefault(e);
			}
			if (_updating || e.button != 0) {
				return;
			}
			update(e);
			_mouseDown = true;
			_startPosition = { x: e.pageX !== undefined ? e.pageX : e.targetTouches[0].pageX, y: e.pageY !== undefined ? e.pageY : e.targetTouches[0].pageY };
			_position = _startPosition;
			if (tryPlaceItem(e)) {
				_locked = true;
			}
		};

		document.ontouchstart = document.onmousedown;

		this.UpdatePosition = () => {
			if (!_cursor.parentNode) {
				return;
			}

			_cursor.style.left = `${_position.x - container.getBoundingClientRect().x - _offset.x}px`;
			_cursor.style.top = `${_position.y - _offset.y}px`;

			if (_item && !_action && MENU.Inventory.menu) {
				let rows = 0;

				let cursorRect = _cursor.getBoundingClientRect();
				cursorRect = { x: cursorRect.x, y: cursorRect.y, right: cursorRect.right + 4, bottom: cursorRect.bottom + 4 };

				const tables = MENU.Inventory.menu.getElementsByTagName("table");
				const spaces = [];
				const containers = [];

				for (let i = 0; i < 2; i++) {
					const table = tables[i * 2];
					const rowList = table.getElementsByTagName("tr");

					for (let l = 0; l < rowList.length; l++) {
						const spaceList = rowList[l].getElementsByTagName("td");
						const rowRect = rowList[l].getBoundingClientRect();
						const y = rowRect.y + rowRect.height * 0.5;

						if (y >= cursorRect.y && y < cursorRect.bottom && rows < _item.base.height) {
							let columns = 0;
							for (let j = 0; j < spaceList.length; j++) {
								const spaceRect = spaceList[j].getBoundingClientRect();
								const x = spaceRect.x + spaceRect.width * 0.5;
								if (x >= cursorRect.x && x < cursorRect.right && columns < _item.base.width) {
									spaces.push(spaceList[j]);
									if (containers.indexOf(i) < 0) {
										containers.push(i);
									}
									columns++;
								}
								spaceList[j].style.background = "";
							}
							rows++;
						}
						else {
							for (let j = 0; j < spaceList.length; j++) {
								spaceList[j].style.background = "";
							}
						}
					}
				}

				if (spaces.length > 0) {
					if (spaces[0] != _prevFirstSpace || spaces.length != _prevSpacesCount) {
						_validPosition = validatePosition(containers, spaces, getSiblingIndex(spaces[0].parentNode) * spaces[0].parentNode.children.length + getSiblingIndex(spaces[0]));
						_prevFirstSpace = spaces[0];
						_prevSpacesCount = spaces.length;
					}

					if (!_target) {
						let color = _validPosition ? CURSOR.VALID_SLOT_COLOR : CURSOR.INVALID_SLOT_COLOR;
						for (let l = 0; l < spaces.length; l++) {
							spaces[l].style.background = color;
						}
					}
				}
				else {
					_prevFirstSpace = null;
					_validPosition = false;
				}
			}
		};

		const validatePosition = (containers, spaces, position) => {
			let containerWidth;
			let size = _item ? _item.base.width * _item.base.height : 0;

			if (!_item || containers.length > 1 || spaces.length < size) {
				return false;
			}

			switch (containers[0]) {
				case 0:
					_container = _inventoryImage.tab;
					containerWidth = MENU.INVENTORY_TAB_WIDTH;
					break;
				case 1:
					_container = _inventoryImage.heirlooms;
					containerWidth = MENU.Inventory.heirloomContainerWidth;
					break;
				default:
					return false;
			}

			let index = 0;
			_swapWith = null;
			_coords = [];

			for (let i = 0; i < size; i++) {
				index = position + (i % _item.base.width) + Math.floor(i / _item.base.width) * containerWidth;

				if (index < _container.length && _container[index] && _container[index] != _item.id) {
					if (!_swapWith) {
						_swapWith = _container[index];
					}
					else if (_swapWith != _container[index]) {
						return false;
					}
				}

				_coords.push(index);
			}

			return true;
		}

		const validateTarget = (target) => {
			if (Array.isArray(_target)) {
				return _target.indexOf(target) >= 0 || target == "item" && _target.indexOf("equipment") >= 0;
			}
			return _target == target || target == "item" && _target == "equipment";
		};
	};
})();
