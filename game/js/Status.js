STATUS = {};

STATUS.Status = function (_elm, _status, _onclick) {
	let _bodyBar = _elm.getElementsByClassName("body_bar")[0];
	let _mindBar = _elm.getElementsByClassName("mind_bar")[0];

	let _body = _status.body;
	let _mind = _status.mind;

	let _maxBody = _status.maxBody;
	let _maxMind = _status.maxMind;

	let _roleplaying = _status.roleplaying;

	let _tooltip;

	Object.defineProperties(this, {
		'body': {
			get: () => _body
		},
		'mind': {
			get: () => _mind
		},
	});

	_elm.onmouseenter = (e) => _tooltip = TOOLTIP.instance.ShowStatus(e, {
		body: _body,
		maxBody: _maxBody,
		mind: _mind,
		maxMind: _maxMind,
		roleplaying: _roleplaying,
	});
	_elm.ontouchstart = _elm.onmouseenter;

	if (_onclick) {
		_elm.onclick = _onclick;
		_tooltip = null;
	}

	this.SetStatus = function (status) {
		const prevBody = _body;
		const prevMind = _mind;
		const prevMaxBody = _maxBody;
		const prevMaxMind = _maxMind;

		if (status.body !== undefined) {
			_body = status.body;
		}

		if (status.mind !== undefined) {
			_mind = status.mind;
		}

		if (status.maxBody !== undefined) {
			_maxBody = status.maxBody;
		}

		if (status.maxMind !== undefined) {
			_maxMind = status.maxMind;
		}

		if (status.roleplaying !== undefined) {
			_roleplaying = status.roleplaying;
		}

		_bodyBar.style.height = `${(_body / _maxBody) * 100}%`;
		_mindBar.style.height = `${(_mind / _maxMind) * 100}%`;

		if (
			prevBody != _body ||
			prevMind != _mind ||
			prevMaxBody != _maxBody ||
			prevMaxMind != _maxMind
		) {
			if (_tooltip) {
				_tooltip.getElementsByClassName("body")[0].innerHTML = `Body: ${_body}/${_maxBody}`;
				_tooltip.getElementsByClassName("mind")[0].innerHTML = `Mind: ${_mind}/${_maxMind}`;
			}
			this.Show();
		}
	}

	this.Show = () => _elm.classList.remove("inactive");

	this.Hide = () => _elm.classList.add("inactive");

	if (_body < _maxBody || _mind < _maxMind) {
		this.SetStatus(_status);
	}
};
