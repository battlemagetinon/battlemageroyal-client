
(() => {
	let _draftDefault;
	let _draftsByUsername = {};
	let _receiver;
	let _editable;
	let _checked;
	let _paramsByChecked;

	let _ratio;
	let _prevRatio;
	let _scrollDown;

	let _messagesList;
	let _filter;
	let _menu;
	let _username;

	MENU.Messages = {
		View: {
			Inbox: 0,
			Sent: 1,
			Chat: 2,
		},
		Open: (username = null, messages = null, view = null) => {
			if (_menu) {

				if (username) {
					_receiver.value = username;
					_receiver.disabled = true;
					_menu.className = "new full";

					if (_editable.textContent.trim().length > 0) {
						if (!_username) {
							_draftDefault = _editable.innerHTML;
						}
						else {
							_draftsByUsername[_username] = _editable.innerHTML;
						}
					}

					_editable.innerHTML = _draftsByUsername[username] !== undefined ? _draftsByUsername[username] : "";

					_username = username;

					MENU.Messages.LoadChat(username);

					displayMenu(_menu, "full");
				}
				else if (messages) {

					if (_menu.parentNode) {
						const tabs = _menu.getElementsByClassName("tabs")[0];

						const reply = tabs.getElementsByClassName("reply")[0];
						const del = tabs.getElementsByClassName("delete")[0];

						let s;
						let m;
						let h;

						_checked = [];
						_paramsByChecked = [];

						if (
							view == MENU.Messages.View.Inbox && _menu.classList.contains("inbox") ||
							view == MENU.Messages.View.Sent && _menu.classList.contains("sent")
						) {
							for (let i = 0; i < messages.length; i++) {
								const user = view == MENU.Messages.View.Inbox ? messages[i].sender : messages[i].receiver;
								const read = view == MENU.Messages.View.Inbox ? messages[i].receiver_read == "1" : messages[i].sender_read == "1";

								const tr = document.createElement("tr");
								const toggle = document.createElement("td");
								toggle.className = "checkbox";
								tr.appendChild(toggle);

								let td = document.createElement("td");
								let span = document.createElement("span");
								span.innerHTML = user.character.first_name + (user.character.last_name ? ` ${user.character.last_name}` : "");
								td.appendChild(span);

								span = document.createElement("span");
								span.innerHTML = user.username;
								td.appendChild(span);
								tr.appendChild(td);

								td = document.createElement("td");
								let div = document.createElement("div");
								div.className = "delete";
								td.appendChild(div);

								div = document.createElement("div");
								div.className = "toggle_read";
								td.appendChild(div);

								let list = td.getElementsByTagName("div");
								const username = user.username;
								const id = messages[i].id;

								toggle.onclick = () => {
									toggleClass(toggle, "checked");

									if (toggle.classList.contains("checked")) {
										_checked.push(toggle);
										_paramsByChecked.push({ id: id, username: username });
									}
									else {
										const index = _checked.indexOf(toggle);
										_checked.splice(index, 1);
										_paramsByChecked.splice(index, 1);
									}

									if (_checked.length > 1) {
										reply.classList.add("inactive");
										del.classList.remove("inactive");
									}
									else if (_checked.length == 1) {
										reply.classList.remove("inactive");
										del.classList.remove("inactive");
									}
									else {
										reply.classList.add("inactive");
										del.classList.add("inactive");
									}
								};

								list[0].onclick = () => deleteMessages([id]);
								list[1].onclick = () => toggleRead(id, tr);

								tr.appendChild(td);

								td = document.createElement("td");
								td.innerHTML = parseMessage(messages[i].message);
								tr.appendChild(td);

								td = document.createElement("td");

								if (messages[i].seen) {
									td.className = "seen";
								}

								td.innerHTML = unixSecondsToElapsedTimeLabel(messages[i].seen ? messages[i].seen : messages[i].created_at);
								tr.appendChild(td);

								if (read) {
									tr.className = "read";
								}

								list = tr.getElementsByTagName("td");

								list[1].onclick = () => MENU.Messages.Open(username);
								list[3].onclick = list[1].onclick;
								list[4].onclick = list[1].onclick;

								_messagesList.appendChild(tr);
							}
							_filter.Update();
						}
						else if (view == MENU.Messages.View.Chat && _menu.classList.contains("new") && _username) {
							for (let i = messages.length - 1; i >= 0; i--) {
								const tr = document.createElement("tr");
								const message = messages[i];

								if (message.sender) {
									tr.className = "sender";
								}

								const elm = document.createElement("td");
								elm.innerHTML = parseMessage(message.message);

								elm.onclick = (e) => {
									DROPDOWN.instance.Open(e, [
										{
											label: "Copy text",
											onclick: () => {
												const textHolder = document.createElement("textarea");
												textHolder.style.position = "absolute";
												textHolder.style.opacity = "0";
												textHolder.style.pointerEvents = "none";
												textHolder.value = elm.textContent;

												document.body.appendChild(textHolder);
												textHolder.select();

												document.execCommand("Copy");

												if (textHolder.parentNode) {
													textHolder.parentNode.removeChild(textHolder);
												}
											}
										},
										{
											label: "Forward",
											onclick: () => {
												_receiver.value = "";
												_receiver.disabled = false;

												if (_username) {
													if (_editable.textContent.trim().length > 0) {
														_draftsByUsername[_username] = _editable.innerHTML;
													}

													_editable.innerHTML = _draftDefault || "";
													_username = null;
												}

												_editable.innerHTML = elm.innerHTML;

												const datalist = _menu.getElementsByTagName("datalist")[0];
												const usernames = GAME_MANAGER.instance.GetKnownUsernames();

												clearHTML(_messagesList);
												clearHTML(datalist);

												for (let i = 0; i < usernames.length; i++) {
													const option = document.createElement("option");
													option.innerHTML = usernames[i];
													datalist.appendChild(option);
												}
											}
										},
										{
											label: "Delete",
											onclick: async () => {
												if (await deleteMessages([message.id]) && tr.parentNode) {
													tr.parentNode.removeChild(tr);
												};
											}
										},
										!message.sender ? {
											label: "Report",
											onclick: () => window.open(`/report/${_username}?message_id=${message.id}`)
										} : null,
									]);
								};

								tr.appendChild(elm);

								const td = document.createElement("td");
								td.innerHTML = unixSecondsToTimeLabel(message.created_at);

								tr.appendChild(td);
								_messagesList.appendChild(tr);
							}

							scrollDown();
							MENU.Messages.Update();
						}
					}
				}
				else if (!_menu.parentNode) {
					if (!_menu.classList.contains("new") || _username) {

						if (_menu.classList.contains("new")) {
							MENU.Messages.LoadChat(MENU.Messages.receiver.value);
						}
						else if (_menu.classList.contains("sent")) {
							loadSent();
						}
						else {
							loadInbox();
						}

						_menu.style.pointerEvents = "none";
					}

					displayMenu(_menu, "full");
				}

				return;
			}

			_menu = document.createElement("div");
			_menu.id = "menu_messages";

			const form = createForm();

			const datalist = document.createElement("datalist");
			datalist.id = "messages_usernames";

			_receiver = createInputField("messages_receiver", "Enter a username");
			_receiver.setAttribute("list", datalist.id);

			form.appendChild(_receiver);
			form.appendChild(datalist);

			_menu.appendChild(form);

			let tabs = ["New", "Reply", "Delete", "Send"];
			let ul = document.createElement("ul");
			ul.className = "tabs";

			for (let i = 0; i < tabs.length; i++) {
				const li = document.createElement("li");

				const span = document.createElement("span");
				span.innerHTML = tabs[i];

				li.className = tabs[i].toLowerCase();
				li.appendChild(span);

				switch (li.className) {
					case "new":
						li.onclick = () => {
							if (_username) {
								_receiver.value = "";
								_receiver.disabled = false;

								if (_editable.textContent.trim().length > 0) {
									_draftsByUsername[_username] = _editable.innerHTML;
								}

								_editable.innerHTML = _draftDefault || "";
								_username = null;
							}

							if (_menu.classList.contains(li.className)) {
								return;
							}

							_menu.className = li.className + " full";

							clearHTML(datalist);
							clearHTML(_messagesList);

							const tabs = _menu.getElementsByClassName("tabs")[0];
							tabs.getElementsByClassName("reply")[0].classList.add("inactive");
							tabs.getElementsByClassName("delete")[0].classList.add("inactive");

							const usernames = GAME_MANAGER.instance.GetKnownUsernames();

							for (let i = 0; i < usernames.length; i++) {
								const option = document.createElement("option");
								option.innerHTML = usernames[i];
								datalist.appendChild(option);
							}
						};
						break;
					case "send":
						li.onclick = sendPrivateMessage;
						break;
					case "delete":
						li.onclick = () => {
							if (_paramsByChecked.length.length == 0) {
								return;
							}

							let ids = [];

							for (let i = 0; i < _paramsByChecked.length; i++) {
								ids.push(_paramsByChecked[i].id);
							}

							deleteMessages(ids);
						};
						break;
					case "reply":
						li.onclick = () => _paramsByChecked[0] && MENU.Messages.Open(_paramsByChecked[0].username);
						break;
				}

				if (li.className == "reply" || li.className == "delete") {
					li.classList.add("inactive");
				}

				ul.appendChild(li);
			}

			_menu.appendChild(ul);

			_messagesList = document.createElement("table");
			ul = document.createElement("ul");
			ul.className = "tabs";

			_filter = new FILTER.Filter(_messagesList, "messages_filter", "Search messages", true, () => _menu.classList.contains("new") && _username);
			form.appendChild(_filter.input);

			let div = document.createElement("div");
			div.className = "table";

			_editable = document.createElement("div");
			_editable.className = "editable";
			_editable.setAttribute("contenteditable", true);

			_editable.onpaste = (e) => {
				e.preventDefault();
				const contentHolder = document.createElement("div");
				contentHolder.innerHTML = escapeHTMLOperators((e.clipboardData || window.clipboardData).getData('text'));
				document.execCommand('inserttext', false, (e.clipboardData || window.clipboardData).getData('text'));
			};

			_editable.onkeydown = (e) => {
				switch (e.key) {
					case "Enter":
						e.ctrlKey && _menu.style.pointerEvents != "none" && sendPrivateMessage();
						break;
				}
			};

			div.appendChild(_messagesList);
			div.appendChild(ul);
			div.appendChild(_editable);

			_menu.appendChild(div);

			tabs = ["Inbox", "Sent"];

			for (let i = 0; i < tabs.length; i++) {
				const li = document.createElement("li");

				const span = document.createElement("span");
				span.innerHTML = tabs[i];

				li.className = tabs[i].toLowerCase();
				li.appendChild(span);
				li.onclick = () => {
					switch (li.className) {
						case "inbox":
							MENU.Messages.ShowInbox();
							break;
						case "sent":
							MENU.Messages.ShowSent();
							break;
					}
				};

				ul.appendChild(li);
			}

			if (username) {
				_receiver.value = username;
				_receiver.disabled = true;
				_menu.className = "new";
				_username = username;
				MENU.Messages.LoadChat(username);
			}
			else {
				_menu.className = tabs[0].toLowerCase();
				loadInbox();
			}

			_menu.style.pointerEvents = "none";

			displayMenu(_menu, "full");
		},
		Close: () => {
			if (document.getElementById("menu_messages")) {
				MENU.CloseAll();
				return true;
			}
			return false;
		},
		Toggle: (username, messages, view) => !MENU.Messages.Close() && MENU.Messages.Open(username, messages, view),
		ShowInbox: () => {
			_menu.className = "inbox full";
			loadInbox();
		},
		ShowSent: () => {
			_menu.className = "sent full";
			loadSent();
		},
		LoadChat: async (correspondent) => {
			if (MENU.Messages.menu && GAME_MANAGER.instance.ready) {
				prepareMenu();
				processResponse(await GAME_MANAGER.instance.Api("game/social/getchat.php", { correspondent: correspondent }), MENU.Messages.View.Chat);
			}
		},
		Update: () => {
			if (!_menu || !_menu.parentNode) {
				return;
			}

			_ratio = (1 - _editable.offsetHeight / _editable.parentNode.offsetHeight);

			if (_ratio != _prevRatio) {

				_scrollDown = (
					_messagesList.offsetHeight >= _messagesList.scrollHeight ||
					_messagesList.scrollTop >= _messagesList.scrollHeight - _messagesList.offsetHeight
				);

				_messagesList.style.height = `${_ratio * 100}%`;

				if (_scrollDown) {
					scrollDown();
				}

				_prevRatio = _ratio;
			}
		},
	};

	Object.defineProperties(MENU.Messages, {
		'menu': {
			get: () => _menu
		},
		'filter': {
			get: () => _filter
		},
		'receiver': {
			get: () => _receiver
		},
		'username': {
			get: () => _username
		}
	});

	function unixSecondsToElapsedTimeLabel(timestamp) {
		const date = new Date(timestamp * 1000);
		let s = Math.floor((Date.now() - date.getTime()) / 1000);

		let i;
		let str;

		if (s < 60) {
			i = 0;
			str = "<1 minute";
		}
		else if (s < 60 * 60) {
			i = Math.floor(s / 60);
			str = `${i} minute`;
		}
		else if (s < 60 * 60 * 24) {
			i = Math.floor(s / (60 * 60));
			str = `${i} hour`;
		}
		else if (s < 60 * 60 * 24 * 7) {
			i = Math.floor(s / (60 * 60 * 24));
			str = `${i} day`;
		}
		else if (s < 60 * 60 * 24 * 7 * 4) {
			i = Math.floor(s / (60 * 60 * 24 * 7));
			str = `${i} week`;
		}
		else if (s < 60 * 60 * 24 * 365) {
			i = Math.floor(s / (60 * 60 * 24 * 7 * 4));
			str = `${i} month`;
		}
		else {
			i = Math.floor(s / (60 * 60 * 24 * 365));
			str = `${i} year`;
		}

		return str + ((i > 1) ? "s ago" : " ago");
	}

	function unixSecondsToTimeLabel(timestamp) {
		const date = new Date(timestamp * 1000);
		const now = new Date();

		let h = date.getHours();
		let m = date.getMinutes();

		if (h < 10) {
			h = `0${h}`;
		}

		if (m < 10) {
			m = `0${m}`;
		}

		if (date.toDateString() === now.toDateString()) {
			return innerHTML = `${h}:${m}`;
		}

		const yesterday = new Date();
		yesterday.setDate(yesterday.getDate() - 1);

		if (date.toDateString() === yesterday.toDateString()) {
			return `Yesterday ${h}:${m}`;
		}

		const s = Math.floor((now.now - date.getTime()) / 1000);

		if (s < 60 * 60 * 24 * 7) {
			return `${CALENDAR.instance.ToWeekday(date.getDay())} ${h}:${m}`;
		}

		if (date.getYear() == now.getYear() && date.getMonth() === now.getMonth()) {
			return `${CALENDAR.instance.ToMonth(date.getMonth())} ${date.getDate()}`;
		}

		const list = date.toDateString().split(" ");
		return `${list[1]} ${list[2]} ${list[3]}`;
	}

	function submit(receiver, elm) {
		GAME_MANAGER.instance.Send({ action: "Message", receiver: receiver, message: reduceLineBreaks(contentToText(elm).trim()) });
	}

	function scrollDown() {
		if (_messagesList.offsetHeight < _messagesList.scrollHeight) {
			_messagesList.scrollTop = _messagesList.scrollHeight - _messagesList.offsetHeight;
		}
	};

	function sendPrivateMessage() {
		const user = _receiver.value.trim();
		if (user.length == 0) {
			GUI.instance.Alert("Please enter a receiver for the message", "Close", null);
		}
		else if (_editable.textContent.trim().length == 0) {
			GUI.instance.Alert("Please enter a message to send", "Close", null);
		}
		else {
			submit(user, _editable);
			_menu.style.pointerEvents = "none";
		}
	};

	async function loadInbox() {
		if (MENU.Messages.menu && GAME_MANAGER.instance.ready) {
			prepareMenu();
			processResponse(await GAME_MANAGER.instance.Api("game/social/getinbox.php"), MENU.Messages.View.Inbox);
		}
	}

	function parseMessage(message) {
		return escapeHTMLOperators(reduceLineBreaks(message.trim()));
	};

	async function deleteMessages(ids) {
		if (
			!MENU.Messages.menu || !GAME_MANAGER.instance.ready ||
			!(await GUI.instance.Alert(`Are you sure you want to delete ${ids.length > 1 ? "these messages?" : "this message?"}`, "Confirm"))
		) {
			return false;
		}

		MENU.Messages.menu.style.pointerEvents = "none";

		const obj = await GAME_MANAGER.instance.Api("game/social/deletemessage.php", { ids: ids.join(",") }, false);

		if (obj.success) {
			if (MENU.Messages.menu.classList.contains("sent")) {
				loadSent();
			}
			else if (!MENU.Messages.menu.classList.contains("new")) {
				loadInbox();
			}
		}

		if (!obj.success || MENU.Messages.menu.classList.contains("new")) {
			MENU.Messages.menu.style.pointerEvents = "";
		}

		return obj.success;
	}

	async function loadSent() {
		if (MENU.Messages.menu && GAME_MANAGER.instance.ready) {
			prepareMenu();
			processResponse(await GAME_MANAGER.instance.Api("game/social/getsent.php"), MENU.Messages.View.Sent);
		}
	}

	function prepareMenu() {
		MENU.Messages.menu.style.pointerEvents = "none";
		MENU.Messages.filter.Clear();

		clearHTML(MENU.Messages.menu.getElementsByTagName("table")[0]);

		const tabs = MENU.Messages.menu.getElementsByClassName("tabs")[0];
		tabs.getElementsByClassName("reply")[0].classList.add("inactive");
		tabs.getElementsByClassName("delete")[0].classList.add("inactive");
	}

	async function toggleRead(id, elm) {
		if (!MENU.Messages.menu && GAME_MANAGER.instance.ready) {
			return false;
		}

		MENU.Messages.menu.style.pointerEvents = "none";

		const obj = await GAME_MANAGER.instance.Api("game/social/toggleread.php", { id: id, read: elm.classList.contains("read") ? 0 : 1 }, false);

		if (obj.success) {
			toggleClass(elm, "read");

			let unreadMessages = parseInt(document.getElementById("frame_top_right").getElementsByClassName("icons")[0].getElementsByClassName("unread_messages")[0].getElementsByTagName("span")[0].innerHTML);

			if (isNaN(unreadMessages)) {
				unreadMessages = 0;
			}

			if (elm.classList.contains("read")) {
				unreadMessages = Math.max(unreadMessages - 1, 0);
			}
			else {
				unreadMessages++;
			}

			GUI.instance.SetUnreadMessages(unreadMessages);
		}

		MENU.Messages.menu.style.pointerEvents = "";

		return obj.success;
	}

	function processResponse(obj, view) {
		if (obj.success) {
			MENU.Messages.Open(null, obj.messages, view);
			if (obj.unread_messages !== undefined) {
				GUI.instance.SetUnreadMessages(obj.unread_messages);
			}
		}
		MENU.Messages.menu.style.pointerEvents = "";
	}
})();
