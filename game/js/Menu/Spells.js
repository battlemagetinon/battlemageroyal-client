(() => {
	let _menu;
	let _filer;
	let _spells;
	let _cooldown;
	let _progress;
	let _expandedSpell;
	let _scrollTop;
	let _params;

	let _timestamp = 0;

	MENU.Spells = {
		Open: (params) => {
			let spellList;
			let title;

			_params = params;

			if (_menu) {
				if (_menu.parentNode) {
					removeMenu(_menu.classList.contains("left") ? "left" : "right");
				}

				spellList = _menu.getElementsByTagName("table")[0];
				title = _menu.getElementsByClassName("title")[0];

				clearHTML(spellList);

				_menu.className = "";
			}
			else {
				_menu = document.createElement("div");
				_menu.id = "menu_spells";

				title = document.createElement("div");
				title.className = "title";
				title.innerHTML = "Discovered Spells";

				_menu.appendChild(title);

				spellList = document.createElement("table");
				_spells = document.createElement("div");
				_spells.className = "item_list spells";
				_cooldown = document.createElement("div");
				_cooldown.className = "cooldown";

				_spells.appendChild(spellList);
				_spells.appendChild(_cooldown);
				_menu.appendChild(_spells);

				_filer = new FILTER.Filter(spellList, "spells_filter", "Search spells", false, "variant");

				const form = createForm();
				form.appendChild(_filer.input);

				_menu.appendChild(form);
			}

			if (params.target) {
				title.innerHTML = "Cast spell" + (params.isSelf ? " on self" : " on " + LOCATION.instance.opponent.name);
			}
			else {
				title.innerHTML = "Your Spellbook";
			}

			let spells = GAME_MANAGER.instance.spells;

			const list = [];
			const spellNames = Object.keys(spells);
			spellNames.sort();

			for (let i = 0; i < spellNames.length; i++) {
				let spell = spells[spellNames[i]];
				list.push(spell);

				if (spell.variants !== undefined) {
					let variantNames = Object.keys(spell.variants);
					variantNames.sort();

					for (let j = 0; j < variantNames.length; j++) {
						let variant = spell.variants[variantNames[j]];
						list.push(variant);
					}
				}
			}

			spells = list;

			for (let i = 0; i < spells.length; i++) {
				const spell = spells[i];

				if (!params.target || params.onlySpellActions == spell.spell_cost > 0 && (params.isSelf || spell.tags.includes("Target"))) {
					const tr = document.createElement("tr");

					if (spell.spell_id !== undefined) {
						tr.className = `variant spell_${spell.spell_id}`;
						if (_expandedSpell != spell.spell_id) {
							tr.classList.add("hidden");
						}
					}

					let td = document.createElement("td");
					td.className = "spell_name";
					td.innerHTML = spell.spell_name;
					tr.appendChild(td);

					td = document.createElement("td");
					td.className = "short_desc";
					td.innerHTML = spell.short_desc || "";
					tr.appendChild(td);

					td = document.createElement("td");
					td.className = "description";
					td.innerHTML = spell.description || "";
					tr.appendChild(td);

					td = document.createElement("td");
					td.className = "tags";
					td.innerHTML = spell.tags;
					tr.appendChild(td);

					if (spell.menu_items) {
						const ul = document.createElement("ul");
						td = document.createElement("td");
						td.className = "menu_items";
						td.appendChild(ul);

						for (let j = 0; j < spell.menu_items.length; j++) {
							const li = document.createElement("li");
							li.innerHTML = spell.menu_items[j];
							ul.appendChild(li);
						}

						tr.appendChild(td);
					}

					td = document.createElement("td");
					td.className = "cost";

					if (spell.variants !== undefined) {
						let match = false;

						if (GAME_MANAGER.instance.actions.actions > 0) {
							let next = i + 1;
							while (!match && next < spells.length && spells[next].spell_id !== undefined) {
								match = activeSpell(spell, params.target, params.isSelf);
								next++;
							}
						}

						if (!match) {
							tr.classList.add("inactive");
						}

						tr.onmouseenter = (e) => TOOLTIP.instance.Show(e, TOOLTIP.GetContent(_expandedSpell == spell.id ? TOOLTIP.Spell.Collapse : TOOLTIP.Spell.Expand));
						tr.ontouchstart = tr.onmouseenter;
						tr.onclick = () => {
							let list;
							if (_expandedSpell) {
								list = spellList.getElementsByClassName(`spell_${_expandedSpell}`);
								for (let i = 0; i < list.length; i++) {
									list[i].classList.add("hidden");
								}
							}
							if (_expandedSpell != spell.id) {
								_expandedSpell = spell.id;
								list = spellList.getElementsByClassName(`spell_${_expandedSpell}`);
								for (let i = 0; i < list.length; i++) {
									list[i].classList.remove("hidden");
								}
							}
							else {
								_expandedSpell = null;
							}
						};
					}
					else {
						let div;

						if (spell.spell_cost > 0) {
							div = document.createElement("div");
							div.className = "token spell";
							td.appendChild(div);
						}

						if (spell.action_cost > 0) {
							div = document.createElement("div");
							div.className = "token action";
							td.appendChild(div);
						}

						tr.appendChild(td);

						tr.onmouseenter = (e) => TOOLTIP.instance.ShowSpell(e, spell);
						tr.ontouchstart = tr.onmouseenter;

						if (!activeSpell(spell, params.target, params.isSelf)) {
							tr.classList.add("inactive");
						}
						else {
							tr.onclick = async () => {
								let value;
								if (spell.materials !== undefined && (LOCATION.instance.opponent || spell.consume_materials_on_scenario)) {
									if (!await GUI.instance.Alert(`Cast ${spell.spell_name} and consume its materials?`, "Cast", "Cancel")) {
										return;
									}
								}
								if (spell.prompt !== undefined) {
									value = await GUI.instance.Prompt(spell.prompt.message, spell.prompt.default, "Cast");
									if (value === false) {
										return;
									}
								}
								if (spell.spell_id !== undefined) {
									GAME_MANAGER.instance.CastSpell(spell.spell_id, params.position !== GUI.Position.Left, value, spell.id);
								}
								else {
									switch (spell.id) {
										case 29:
											value = { username: params.position === GUI.Position.Left && LOCATION.instance.opponent && LOCATION.instance.opponent.username || GAME_MANAGER.instance.username };
											break;
									}
									GAME_MANAGER.instance.CastSpell(spell.id, params.position !== GUI.Position.Left, value);
								}
								if (_timestamp < Date.now() && !spell.tags.includes("Crafting") && !GAME_MANAGER.instance.InScenario()) {
									_timestamp = Date.now() + MENU.COOLDOWN_DURATION;
									_progress = 0;
								}
							};
						}
					}

					tr.appendChild(td);
					spellList.appendChild(tr);
				}
			}

			_filer.Update();
			displayMenu(_menu, params.position === GUI.Position.Left ? "left" : "right");

			if (_scrollTop) {
				_spells.scrollTop = _scrollTop;
				_scrollTop = 0;
			}
		},
		Close: () => {
			if (_menu && _menu.parentNode) {
				closeMenu(_menu.classList.contains("left") ? "left" : "right");
				return true;
			}
			return false;
		},
		Refresh: () => {
			if (_menu && _menu.parentNode) {
				_scrollTop = _menu.getElementsByClassName("item_list")[0].scrollTop;
				MENU.Spells.Open(_params);
			}
		},
		Toggle: () => !MENU.Spells.Close() && MENU.Spells.Open(GUI.Position.Right, false),
		Update: () => {
			if (!_menu || !_menu.parentNode) {
				return;
			}

			_progress = clamp(1 - (_timestamp - Date.now()) / MENU.COOLDOWN_DURATION, 0, 1)

			_cooldown.style.width = _progress * 100 + "%";
			_cooldown.style.backgroundColor = `rgba(0, 255, 255, ${(1 - _progress) * 0.8})`;
			_cooldown.style.top = _spells.scrollTop + "px";
		},
	};

	Object.defineProperties(MENU.Spells, {
		'menu': {
			get: () => _menu && _menu.parentNode ? _menu : null
		},
		'target': {
			get: () => _menu && _menu.parentNode && !_menu.classList.contains("right") ? LOCATION.instance.opponent : LOCATION.instance.player
		},
	});

	function activeSpell(spell, target, isSelf) {
		if (spell.action_cost !== undefined && spell.action_cost > GAME_MANAGER.instance.actions.actions) {
			return false
		}
		if (spell.spell_cost !== undefined && spell.spell_cost > GAME_MANAGER.instance.actions.spells) {
			return false
		}
		if (spell.tags.includes("Resting") && !LOCATION.instance.resting) {
			return false;
		}
		if (spell.materials !== undefined && !GAME_MANAGER.instance.HasMeterials(spell.materials)) {
			return false;
		}
		return true;
	};
})();
