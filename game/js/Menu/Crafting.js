(() => {
	let _params = {};
	let _scrollTop;
	let _filter;
	let _item;
	let _selection;
	let _callback;
	let _option;
	let _sortedOptions;
	let _showContainer;
	let _containerEnabled;
	let _full;

	MENU.Crafting = {
		Open: (params) => {
			let primary;
			let secondary;
			let div;

			if (!params) {
				params = _params;
			}

			if (params.npc && params.npc === _params.npc) {
				let menu = MENU.Crafting.menu;
				if (menu) {
					params.selection = _selection;
					_scrollTop = menu.getElementsByClassName("item_list")[0].scrollTop;
				}
			}

			_params = Object.assign({}, params || {});

			let options = params.options || [];
			_sortedOptions = [];

			let sortingIndice = [];

			for (let i = 0; i < options.length; i++) {
				sortingIndice.push(options[i].sort_index ? options[i].sort_index : 0);
			}

			sortingIndice = unique(sortingIndice);
			sortingIndice.sort();

			for (let i = 0; i < sortingIndice.length; i++) {
				const index = sortingIndice[i];
				const sortingList = [];
				for (let j = 0; j < options.length; j++) {
					if ((options[j].sort_index ? options[j].sort_index : 0) == index) {
						sortingList.push(options[j]);
					}
				}
				sortingList.sort((a, b) => a.label.localeCompare(b.label));
				_sortedOptions = _sortedOptions.concat(sortingList);
			}

			_item = typeof params.item === "number" || params.item && params.item.id ? GAME_MANAGER.instance.GetItem(typeof params.item === "number" ? params.item : params.item.id) : params.item;
			_selection = !_params.tabs ? _params.selection : _params.selection && typeof _params.selection === "object" ? _params.selection : {};
			_callback = _params.callback;
			_option = _params.option;

			let cancellable = params.cancellable !== false;

			let menu = MENU.Crafting.menu;

			if (menu) {
				menu.parentNode.removeChild(menu);
				clearHTML(menu);
			}
			else {
				menu = document.createElement("div");
				menu.id = "menu_crafting";
			}

			if (_params.tabs !== undefined && _params.tabs.length > 0) {
				const ul = document.createElement("ul");
				ul.className = "tabs";
				for (let i = 0; i < _params.tabs.length; i++) {
					const tab = _params.tabs[i];
					const li = document.createElement("li");
					const span = document.createElement("span");

					span.textContent = tab;
					li.className = tab.toLowerCase();
					li.appendChild(span);
					ul.appendChild(li);

					li.onclick = () => {
						_filter.Clear();
						menu.className = li.className + " left";
					};

					if (i == 0) {
						menu.className = li.className;
					}
				}
				menu.appendChild(ul);
			}
			else {
				div = document.createElement("div");
				div.className = "title";
				div.innerHTML = params.title || "";
				menu.appendChild(div);
			}

			div = document.createElement("div");

			const subdiv = document.createElement("div");
			const wrapper = document.createElement("div");
			const optionList = document.createElement("table");

			wrapper.appendChild(div);

			subdiv.className = "item_list options";
			subdiv.appendChild(optionList);

			const value = _filter ? _filter.input.value : "";
			_filter = new FILTER.Filter(optionList, "crafting_filter", "Search", false);

			const enchantments = _item ? GAME_MANAGER.instance.GetEnchantments(_item.id) : null;

			if (enchantments) {
				for (let i = 0; i < enchantments.length; i++) {
					if (enchantments[i].primary) {
						primary = enchantments[i];
					}
					else if (enchantments[i].secondary) {
						secondary = enchantments[i];
					}
				}
			}

			_showContainer = _sortedOptions.length === 0 || _params.spellId === 29 || _item && _item.workorder;

			for (let i = 0; i < _sortedOptions.length; i++) {
				const option = Object.assign({}, _sortedOptions[i]);
				const index = i;

				_showContainer = _showContainer || option.target === "equipment" || option.target === "item";

				if (option.variants !== undefined) {
					for (let prop in option.variants) {
						let b = false;
						switch (prop) {
							case "magic":
								b = primary && primary.magic || secondary && secondary.magic;
								break;
							case "occult":
								b = primary && primary.occult || secondary && secondary.occult;
								break;
							case "blessed":
								b = primary && primary.blessed || secondary && secondary.blessed;
								break;
							case "hexed":
								b = _item && ConjuredType.IsHexed(_item.conjured_type);
								break;
						}
						if (b) {
							Object.assign(option, option.variants[prop]);
						}
					}
				}

				const tr = document.createElement("tr");
				let td = document.createElement("td");
				td.innerHTML = option.label;
				tr.appendChild(td);

				td = document.createElement("td");
				td.innerHTML = option.tags !== undefined ? option.tags.join(",") : "";
				td.className = "tags";
				tr.appendChild(td);

				td = document.createElement("td");

				if (option.materials !== undefined) {
					for (let prop in option.materials) {
						let image;
						switch (prop) {
							case "Sol Talisman":
								image = "assets/items/deprecated/sol_talisman.png";
								break;
							case "Luna Talisman":
								image = "assets/items/luna_talisman.png";
								break;
							case "Blood Moon Talisman":
								image = "assets/items/blood_moon_talisman.png";
								break;
							case "Talisman of Eclipsing":
								image = "assets/items/deprecated/talisman_of_eclipsing.png";
								break;
							case "Triple Goddess Talisman":
								image = "assets/items/deprecated/triple_goddess_talisman.png";
								break;
							case "Chromatic Dye":
								image = "assets/items/deprecated/chromatic_dye.png";
								break;
							case "Ornamental Charm":
								image = "assets/items/deprecated/ornamental_charm.png";
								break;
							case "Malachite Beads":
								image = "assets/items/deprecated/malachite_beads.png";
								break;
							case "Cleansing Soap":
								image = "assets/items/deprecated/cleansing_soap.png";
								break;
							case "Candy Bone":
								image = "assets/items/deprecated/canine_candy.png";
								break;
							case "Cat Candy":
								image = "assets/items/deprecated/cat_candy.png";
								break;
							case "Candy Cane":
								image = "assets/items/deprecated/candy_cane.png";
								break;
							case "Holly":
								image = "assets/items/deprecated/holly.png";
								break;
							default:
								image = null;
								break;
						}

						const span = document.createElement("span");
						span.innerHTML = `${option.materials[prop] > 0 ? option.materials[prop] : 1}x`;

						const div = document.createElement("div");
						div.className = "token";

						if (image) {
							div.style.backgroundImage = `url(${image})`;
						}

						td.appendChild(span);
						td.appendChild(div);
					}
				}
				else {
					const div = document.createElement("div");
					div.className = "token";
					td.appendChild(div);
				}

				tr.appendChild(td);

				const validateItem = _params.spellId !== 29;

				if (!validOption(option, validateItem)) {
					tr.classList.add("invalid");
				}

				if (option.tab) {
					tr.classList.add(option.tab);
					if (_selection[option.tab] === index) {
						tr.classList.add("selected");
					}
				}
				else if (_selection === index) {
					tr.classList.add("selected");
					_option = option;
				}

				if (option.known === true) {
					tr.classList.add("known");
				}

				tr.onmouseenter = (e) => TOOLTIP.instance.ShowCraftingOption(e, option, _item);
				tr.onclick = () => {
					if (option.tab) {
						const selected = tr.parentNode.getElementsByClassName(`selected ${option.tab}`)[0];
						if (selected) {
							selected.classList.remove("selected");
						}
						if (_selection[option.tab] === index) {
							tr.classList.remove("selected");
							delete _selection[option.tab];
						}
						else {
							tr.classList.add("selected");
							_selection[option.tab] = index;
						}
						if (_params.spellId === 29) {
							updateConjurePreview();
						}
					}
					else if (_selection !== index) {
						const selected = tr.parentNode.getElementsByClassName("selected")[0];
						if (selected) {
							selected.classList.remove("selected");
						}
						tr.classList.add("selected");
						_selection = index;
						_option = option;
					}
				};

				optionList.appendChild(tr);
			}

			_full = _showContainer && _params.spellId !== 29 && (!_item || !_item.workorder);

			form = createForm();
			form.appendChild(_filter.input);

			const footer = document.createElement("div");
			footer.className = "footer";
			footer.appendChild(form);

			div.appendChild(subdiv);
			div.appendChild(footer);

			menu.appendChild(wrapper);

			_filter.Update(true);
			_filter.input.value = value;
			_filter.input.onchange();

			let button;

			if (options.length != 0) {
				button = document.createElement("div");
				button.className = "craft";

				const span = document.createElement("span");
				span.innerHTML = _params.buttonLabel || "Apply";

				button.onclick = () => apply();
				button.appendChild(span);
			}

			if (_showContainer) {
				const itemContainer = document.createElement("div");
				itemContainer.className = "item_container";

				const grid = document.createElement("div");
				grid.className = "grid";
				grid.appendChild(itemContainer);

				div = document.createElement("div");
				div.appendChild(grid);

				const elm = MENU.Inventory.DrawItem(itemContainer, _item);
				if (elm && (_item.workorder || GAME_MANAGER.instance.IsEquipped(_item.id))) {
					elm.onmousedown = null;
					elm.ontouchstart = null;
					elm.ontouchmove = null;
					elm.onclick = null;
					_containerEnabled = false;
				}
				else {
					_containerEnabled = true;
				}

				_containerEnabled = _containerEnabled && _params.spellId !== 29;

				button && div.appendChild(button);
				wrapper.appendChild(div);

				if (_params.spellId === 29) {
					updateConjurePreview();
				}
				else {
					MENU.Inventory.Open();
				}
			}
			else {
				button && footer.appendChild(button);
				_containerEnabled = false;
			}

			displayMenu(menu, "left", cancellable && _callback ? () => {
				_callback({ index: _selection });
				_callback = null;
				MENU.Crafting.Close();
			} : _full || !cancellable ? false : null);

			if (_scrollTop) {
				optionList.parentNode.scrollTop = _scrollTop;
				_scrollTop = 0;
			}
		},
		Close: () => {
			if (MENU.Crafting.menu) {
				MENU.CloseLeftMenu();
				MENU.Inventory.Close();
				return true;
			}
			return false;
		},
		Refresh: () => {
			if (MENU.Crafting.menu) {
				_scrollTop = MENU.Crafting.menu.getElementsByClassName("item_list")[0].scrollTop;
				MENU.Crafting.Open(MENU.Crafting.params);
			}
		},
		SetItem: (item) => {
			const menu = MENU.Crafting.menu;
			if ((menu || MENU.Crafting.pending) && !(_item && _item.workorder)) {
				if (menu) {
					_scrollTop = menu.getElementsByClassName("item_list")[0].scrollTop;
				}
				MENU.Crafting.Open(Object.assign(MENU.Crafting.params, { item: item }));
			}
		},
	};

	Object.defineProperties(MENU.Crafting, {
		'menu': {
			get: () => document.getElementById("menu_crafting")
		},
		'pending': {
			get: () => _callback != null
		},
		'containerEnabled': {
			get: () => _containerEnabled
		},
		'params': {
			get: () => Object.assign(_params, { selection: _selection, callback: _callback, option: _option }),
		},
		'full': {
			get: () => _full,
		},
	});

	function updateConjurePreview() {
		const menu = MENU.Crafting.menu;

		if (!menu || !_selection || typeof _selection !== "object") {
			return
		}
		if (_params.workorderBase === undefined) {
			_params.workorderBase = _item && _item.base ? _item.base : null;
		}

		if (_selection.appearance !== undefined) {
			const appearance = _sortedOptions[_selection.appearance];
			const height = appearance.height;
			const width = appearance.width;
			const image_url = appearance.image_url;
			const worn_image_url = appearance.worn_image_url;
			_item = Object.assign(_item || {}, { base: { item_name: appearance.label, height, width, image_url, worn_image_url } });
		}
		else if (_params.workorderBase) {
			_item = Object.assign(_item || {}, { base: _params.workorderBase });
		}
		else {
			_item = null;
		}

		if (_item) {
			if (_selection.color !== undefined) {
				const color = _sortedOptions[_selection.color];
				_item.variant_color = color.label;
			}
			else if (_item.variant_color !== undefined) {
				delete _item.variant_color;
			}

			if (_selection.accessory !== undefined) {
				const accessory = _sortedOptions[_selection.accessory];
				_item.variant_accessory = nameToRef(accessory.variant);
			}
			else if (_item.variant_accessory !== undefined) {
				delete _item.variant_accessory;
			}
		}

		const itemContainer = menu.getElementsByClassName("item_container")[0];
		if (itemContainer) {
			clearHTML(itemContainer);
			if (_item) {
				const elm = MENU.Inventory.DrawItem(itemContainer, _item);
				elm.onmousedown = null;
				elm.ontouchstart = null;
				elm.ontouchmove = null;
				elm.onclick = null;
			}
		}
	}

	function validOption(option, validateItem) {
		if (option.materials !== undefined && !GAME_MANAGER.instance.HasMeterials(option.materials)) {
			return false;
		}
		if (validateItem) {
			if (_item) {
				if (option.usable_on_conjured !== true && ConjuredType.IsConjured(_item.conjured_type)) {
					return false;
				}
				switch (_params.spellId) {
					case 22:
						if (option.usable_on_hexed !== true && ConjuredType.IsHexed(_item.conjured_type)) {
							return false;
						}
						break;
				}
			}
			switch (option.target) {
				case "item":
					return _item != null;
				case "equipment":
					return _item && _item.base.type == "equipment";
				default:
					return _item == null;
			}
		}
		return true;
	}

	function apply() {
		if (_selection && typeof _selection === "object" ? Object.keys(_selection).length === 0 : !Number.isInteger(_selection)) {
			return GUI.instance.DisplayMessage("You must select a recipe from the list");
		}
		if (Number.isInteger(_selection)) {
			if (_showContainer) {
				if (_sortedOptions[_selection].target !== undefined && !_item) {
					return GUI.instance.DisplayMessage("You must place an item in the container for the selected recipe");
				}
				if (_sortedOptions[_selection].target === undefined && _item) {
					return GUI.instance.DisplayMessage("The item container muct be empty for this recipe");
				}
			}
			if (validOption(_option, _showContainer)) {
				if (_callback) {
					_callback({ index: _sortedOptions[_selection].index, itemId: _item && !_item.workorder && _item.id });
					_callback = null;
					MENU.Crafting.Close();
				}
				else if (_params.npc) {
					GAME_MANAGER.instance.Send({ action: "crafting", npc: true, itemId: _item && _item.id, optionId: _sortedOptions[_selection].id });
				}
				else {
					GAME_MANAGER.instance.Send({ action: "crafting", spellId: _params.spellId, itemId: _item && _item.id, optionId: _sortedOptions[_selection].id });
				}
			}
		}
		else {
			const obj = {};
			for (let prop in _selection) {
				const option = _sortedOptions[_selection[prop]];
				if (!validOption(option, false)) {
					return false;
				}
				switch (prop) {
					case "appearance":
						obj.appearance = option.id;
						break;
					case "accessory":
						obj.accessory = option.variant;
						break;
					case "color":
						obj.color = option.label;
						break;
				}
			}

			if (obj.appearance === undefined) {
				obj.appearance = _item && _item.base && _item.base.id;
				if (!obj.appearance) {
					return GUI.instance.DisplayMessage("You must select an appearance for the conjured item");
				}
			}

			if (_callback) {
				_callback(Object.assign(obj, { itemId: _item && !_item.workorder && _item.id }));
				_callback = null;
				MENU.Crafting.Close();
			}
			else {
				obj.username = _params.username;
				obj.index = _params.index;
				obj.slot = _params.slot;
				GAME_MANAGER.instance.Send(Object.assign(obj, { action: "crafting", spellId: _params.spellId, itemId: _item && _item.id }));
			}
		}
	};
})();
