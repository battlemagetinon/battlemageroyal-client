(() => {
	const QUALITY_GRID_HEIGHT = 3;
	const QUALITY_GRID_WIDTH = 3;

	const _stats = ["Strength", "Dexterity", "Intelligence", "Willpower"];
	const _filterTags = ["absorption", "animal", "anthro", "inanimate", "monster", "sissification", "transgender"];
	const _natures = ["pure", "tough", "cunning", "sexy", "curious"];

	const _myself = {
		menu: null,
	};

	const _inspect = {
		menu: null,
		token: null,
		username: null,
	};

	const _missingImages = [];

	MENU.Myself = {
		Open: async () => {
			if (!_myself.menu) {
				createMenu(_myself);
				_myself.menu.id = "menu_myself";
			}
			GUI.instance.ShowPlayerStatus();
			displayMenu(_myself.menu, "left");
			MENU.Myself.ClearHighlightedSlots();
			if (CURSOR.instance.heldItem) {
				CURSOR.instance.UpdateEquipmentHighlights();
			}
			if (LOCATION.instance.player) {
				GAME_MANAGER.instance.Send({ action: "inspect", myself: true });
			}
		},
		Close: () => {
			if (_myself.menu && _myself.menu.parentNode) {
				closeMenu("left");
				return true;
			}
			return false;
		},
		ClearSlotByItem: (itemId) => {
			const list = _myself.menu && _myself.menu.getElementsByClassName("item_slot");
			if (list) {
				const equipmentSlots = MENU.Inventory.equipmentSlots;
				for (let i = 0; i < list.length; i++) {
					const slot = list[i].classList[1];
					const item = slot === "accessory" ? _myself.equipment.items[6 + getSiblingIndex(list[i])] : _myself.equipment.items[equipmentSlots.indexOf(slot)];
					if (item && item.id === itemId) {
						clearHTML(list[i]);
						break;
					}
				}
			}
		},
		ClearHighlightedSlots: () => {
			if (_myself.menu) {
				const equipmentSlots = MENU.Inventory.equipmentSlots;
				for (let i = 0; i < 6; i++) {
					_myself.menu.classList.remove(equipmentSlots[i]);
				}
				_myself.menu.classList.remove("accessory");
			}
		},
		Refresh: () => MENU.Myself.menu && GAME_MANAGER.instance.Send({ action: "inspect", myself: true }),
		Update: (data) => updateMenu(_myself, data),
		Toggle: () => !MENU.Myself.Close() && MENU.Myself.Open(),
	};

	MENU.Inspect = {
		Open: async (position, token, username) => {
			MENU.Inspect.Close();
			if (!_inspect.menu || username && username.toLowerCase() != _inspect.username || token && token != _inspect.token) {
				createMenu(_inspect);
				_inspect.username = username ? username.toLowerCase() : null;
				_inspect.token = token ? token : null;
				_inspect.menu.id = "menu_inspect";
			}
			else {
				_inspect.menu.classList.remove("right");
				_inspect.menu.classList.remove("left");
			}
			displayMenu(_inspect.menu, position === GUI.Position.Right ? "right" : "left");
			if (LOCATION.instance.player) {
				GAME_MANAGER.instance.Send({ action: "inspect", username: username, idToken: token });
			}
		},
		Close: (position) => {
			if (_inspect.menu && _inspect.menu.parentNode && (!position && position !== 0 || _inspect.menu.classList.contains(position === GUI.Position.Right ? "right" : "left"))) {
				closeMenu(_inspect.menu.classList.contains("right") ? "right" : "left");
				return true;
			}
			return false;
		},
		Refresh: (username) => MENU.Inspect.menu && _inspect.username == username && GAME_MANAGER.instance.Send({ action: "inspect", username: _inspect.username, idToken: _inspect.token }),
		Update: (data) => updateMenu(_inspect, data),
		Toggle: (position, token) => !MENU.Inspect.Close(position) && MENU.Inspect.Open(position, token),
	};

	Object.defineProperties(MENU.Myself, {
		'menu': {
			get: () => _myself.menu && _myself.menu.parentNode ? _myself.menu : null
		},
	});

	Object.defineProperties(MENU.Inspect, {
		'menu': {
			get: () => _inspect.menu && _inspect.menu.parentNode ? _inspect.menu : null
		},
	});

	function updateMenu(obj, data) {
		obj.qualities = data.qualities;
		obj.tier = data.tier;
		obj.filter = data.filter;
		obj.stats = data.stats || {};
		obj.equipment = data.equipment || { items: [], worn: [] };
		obj.username = data.username.toLowerCase();

		for (let i = 0; i < _natures.length; i++) {
			obj.menu.classList.remove(_natures[i]);
		}

		obj.menu.classList.add(data.character.nature.toLowerCase());

		let list = obj.menu.getElementsByClassName("character_header")[0].getElementsByTagName("span");

		list[0].className = data.character.gender.toLowerCase();
		list[1].innerHTML = data.character.names;
		list[2].innerHTML = data.username;

		if (data.username_color) {
			list[2].style.color = data.username_color;
		}

		list = obj.menu.getElementsByClassName("filter")[0].getElementsByTagName("span");

		for (let i = 0; i < list.length; i++) {
			if (obj.filter[shortFormToTag(list[i].textContent)]) {
				list[i].classList.add("enabled");
			}
			else {
				list[i].classList.remove("enabled");
			}
		}

		list = obj.menu.getElementsByClassName("item_slot");
		const equipmentSlots = MENU.Inventory.equipmentSlots;

		for (let i = 0; i < list.length; i++) {
			const slot = list[i].classList[1];
			const item = obj.equipment.items[slot === "accessory" ? 6 + getSiblingIndex(list[i]) : equipmentSlots.indexOf(slot)];
			const worn = item && obj.equipment.worn[slot === "accessory" ? 6 + getSiblingIndex(list[i]) : equipmentSlots.indexOf(slot)];

			clearHTML(list[i]);

			if (item) {
				let div = document.createElement("div");
				div.style.backgroundImage = `url(${IMAGE_PROCESSING.getWornItemImage(item, obj.tier)})`;

				if (item.base.height > item.base.width || item.base.worn_image_url) {
					div.style.backgroundSize = "auto 90%";
				}

				if (worn) {
					list[i].classList.add("worn");
				}
				else {
					list[i].classList.remove("worn");
				}

				list[i].appendChild(div);
			}
		}

		list = obj.menu.getElementsByClassName("stats")[0].getElementsByTagName("div");

		for (let i = 0; i < _stats.length; i++) {
			let value = obj.stats[_stats[i].toLowerCase()];
			let span = list[i].getElementsByTagName("span")[1];
			span.innerHTML = value > 0 ? clamp(Math.round(value), 0, 20) : 0;
		}

		const qualities = obj.menu.getElementsByClassName("qualities")[0];
		const parent = qualities.parentNode;
		qualities.parentNode.removeChild(qualities);
		drawQualityGrid(obj, parent, QUALITY_GRID_WIDTH, QUALITY_GRID_HEIGHT);

		MENU.Myself.ClearHighlightedSlots();
		if (CURSOR.instance.heldItem) {
			CURSOR.instance.UpdateEquipmentHighlights();
		}

		if (isDescendant(obj.menu, TOOLTIP.instance.trigger) && CURSOR.instance.IsMouseOver(TOOLTIP.instance.trigger)) {
			TOOLTIP.instance.trigger.onmouseenter(TOOLTIP.instance.event);
		}
	}

	function createMenu(obj) {
		obj.equipment = { items: [], worn: [] };
		obj.qualities = [];
		obj.filter = {};
		obj.stats = {};

		const menu = document.createElement("div");
		menu.className = "menu_inspect";

		const container = document.createElement("div");

		const leftRow = document.createElement("div");
		leftRow.className = "equipment";

		drawItemSlot(obj, leftRow, "undershirt", 4);
		drawItemSlot(obj, leftRow, "underpants", 5);
		drawAccessories(obj, leftRow);

		const rightRow = document.createElement("div");
		rightRow.className = "equipment";

		drawItemSlot(obj, rightRow, "head", 0).classList.add("accessory");
		drawItemSlot(obj, rightRow, "shirt", 1);
		drawItemSlot(obj, rightRow, "pants", 2);
		drawItemSlot(obj, rightRow, "shoes", 3);

		const centerRow = document.createElement("div");

		drawCharacterHeader(centerRow);
		drawCharacterDetails(obj, centerRow);
		drawFocus(centerRow);

		container.appendChild(leftRow);
		container.appendChild(centerRow);
		container.appendChild(rightRow);

		menu.appendChild(container);
		obj.menu = menu;
	};

	function expandQualities(obj) {
		const menu = document.createElement("div");
		menu.className = "menu_inspect";
		const div = document.createElement("div");
		drawQualityGrid(obj, div, 7, 4);
		menu.appendChild(div);
		displayMenu(menu, obj.menu.classList.contains("right") ? "right" : "left");
	};

	function getPublicActions(baseId) {
		return LOCATION.instance.publicActions[baseId] !== undefined ? LOCATION.instance.publicActions[baseId] : [];
	}

	function showEquippedItemOptions(e, index, itemId, isTouch) {
		if (CURSOR.instance.locked || GAME_MANAGER.instance.IsDazed()) {
			return;
		}

		const item = GAME_MANAGER.instance.GetItem(itemId);
		const actions = [];

		if (item.base.actions !== undefined) {
			for (let prop in item.base.actions) {
				const label = prop;
				const action = item.base.actions[label];
				if (!action.public) {
					switch (action ? action.target : null) {
						case "item":
						case "equipment":
							actions.push({ label: label, onclick: () => CURSOR.instance.SetItem(item, label, action.target) });
							break;
						default:
							actions.push({ label: label, onclick: () => GAME_MANAGER.instance.Send({ action: "Item", itemId: item.id, itemAction: label }) });
							break;
					}
				}
			}
		}

		if (isTouch) {
			actions.push({
				label: "Unequip",
				onclick: () => unequipItem(e, itemId),
			});
		}

		const worn = GAME_MANAGER.instance.IsWorn(itemId);

		if (worn) {
			const publicActions = getPublicActions(item.base.id);
			for (let i = 0; i < publicActions.length; i++) {
				const label = publicActions[i];
				actions.unshift({
					label: label,
					onclick: () => GAME_MANAGER.instance.Send({ action: "Item", itemId: itemId, itemAction: label })
				});
			}
		}

		if (GAME_MANAGER.instance.HasSpell(29)) {
			actions.push({
				label: "Conjure",
				onclick: () => conjure(index, GAME_MANAGER.instance.GetItem(itemId), GAME_MANAGER.instance.username)
			});
		}

		if (!item.conjured_type && GAME_MANAGER.instance.HasSpell(23)) {
			actions.push({
				label: "Transform",
				onclick: () => MENU.Crafting.pending ? !GAME_MANAGER.instance.GetCraftingBench() && MENU.Crafting.SetItem(itemId) : GAME_MANAGER.instance.CastSpell(23, true, itemId)
			});
		}

		actions.unshift({
			label: worn ? "Remove" : "Wear",
			onclick: () => GAME_MANAGER.instance.WearItem(itemId, !worn)
		});

		DROPDOWN.instance.Open(e, actions);
	};

	function conjure(slot, item, username) {
		GAME_MANAGER.instance.CastSpell(29, true, { slot, username, base: item ? item.base.id : undefined });
	}

	function drawAccessories(obj, parent) {
		const div = document.createElement("div");
		div.className = "accessories";
		drawItemSlot(obj, div, "accessory", 6);
		drawItemSlot(obj, div, "accessory", 7);
		parent.appendChild(div);
		return div;
	};

	function drawItemSlot(obj, parent, slot, index) {
		const div = document.createElement("div");
		div.className = `item_slot ${slot}`;

		div.onmouseenter = (e) => {
			const item = obj.equipment.items[index];
			if (item) {
				TOOLTIP.instance.ShowItem(e, item, obj.tier);
			}
			else {
				TOOLTIP.instance.Show(e, slot === "head" ? "Headwear<br/>Accessory" : firstToUpperCase(slot));
			}
		};

		div.oncontextmenu = (e) => {
			div.onclick(e);
			e.preventDefault();
		};

		let touchEvent = null;

		div.ontouchstart = (e) => {
			div.onmouseenter(e);
			div.onmousedown();
			touchEvent = e;
		};

		div.ontouchmove = (e) => touchEvent = e;

		div.onmousedown = obj !== _myself ? () => { } : async (e) => {
			const itemId = !CURSOR.instance.locked && GAME_MANAGER.instance.GetItemBySlot(index);
			if (itemId && await CURSOR.instance.WaitForDragStart()) {
				unequipItem(e, itemId);
			}
		};

		div.onclick = (e) => {
			const touch = touchEvent != null;
			touchEvent = null;

			if (touch || e.button === 0 || e.button === 2) {
				e.preventDefault();
				let itemId;
				let item;
				const actions = [];

				if (obj === _myself) {
					itemId = !CURSOR.instance.locked && GAME_MANAGER.instance.GetItemBySlot(index);
					if (itemId) {
						if (touch || e.button == 2) {
							return showEquippedItemOptions(e, index, itemId, touch);
						}
						return unequipItem(e, itemId);
					}
				}
				else {
					item = obj.equipment.items[index];
					const publicActions = item ? getPublicActions(item.base.id) : [];
					itemId = item && item.id;
					for (let i = 0; i < publicActions.length; i++) {
						const label = publicActions[i];
						actions.push({
							label: label,
							onclick: () => GAME_MANAGER.instance.Send({ action: "Opponent", itemId: itemId, itemAction: label })
						});
					}
				}

				if (GAME_MANAGER.instance.HasSpell(29)) {
					const opponent = LOCATION.instance.opponent;
					if (obj === _myself && (touch || e.button === 2) || opponent && opponent.username === _inspect.username) {
						actions.push({
							label: "Conjure",
							onclick: () => conjure(index, item, obj.username)
						});
					}
				}

				if (actions.length > 0) {
					DROPDOWN.instance.Open(e, actions);
				}
			}
		};

		parent.appendChild(div);
		return div;
	};

	function unequipItem(e, itemId) {
		if (GAME_MANAGER.instance.InScenario()) {
			return GUI.instance.DisplayMessage("You cannot change what you a wearing during a scenario");
		}
		const item = GAME_MANAGER.instance.GetItem(itemId);
		switch (item.conjured_type) {
			case ConjuredType.TemporaryHex:
			case ConjuredType.ConcealingHex:
			case ConjuredType.IdentifiedHex:
			case ConjuredType.UnidentifiedHex:
				return GUI.instance.DisplayMessage("Unable to unequip item because it's stuck to your body!");
			case ConjuredType.Temporary:
			default:
				GAME_MANAGER.instance.UnequipItem(itemId);
				break;
		}
	}

	function drawCharacterHeader(parent) {
		const div = document.createElement("div");
		div.className = "character_header";

		let span = document.createElement("span");
		div.appendChild(span);

		span = document.createElement("span");
		span.onmouseenter = (e) => e.target.clientWidth < e.target.scrollWidth && TOOLTIP.instance.Show(e, e.target.innerHTML);
		span.ontouchstart = span.onmouseenter;
		div.appendChild(span);

		span = document.createElement("span");
		div.appendChild(span);
		parent.appendChild(div);

		return div;
	}

	function drawStats(obj, parent) {
		const div = document.createElement("div");
		div.className = "stats";

		for (let i = 0; i < _stats.length; i++) {
			const subdiv = document.createElement("div");
			const label = _stats[i];

			let span = document.createElement("span");
			span.innerHTML = _stats[i];
			subdiv.appendChild(span);

			span = document.createElement("span");
			span.innerHTML = "-";
			subdiv.appendChild(span);

			subdiv.onmouseenter = (e) => {
				let div = document.createElement("div");
				let subdiv = document.createElement("div");

				subdiv.innerHTML = TOOLTIP.GetContent(TOOLTIP.Stat[label]);
				subdiv.className = "hint";
				div.appendChild(subdiv);

				let stats = calculateStats(obj, label);

				if (Object.keys(stats).length == 0) {
					return;
				}

				for (let prop in stats) {
					subdiv = document.createElement("div");

					let span = document.createElement("span");
					span.innerHTML = firstToUpperCase(prop) + ": ";
					subdiv.appendChild(span);

					span = document.createElement("span");
					subdiv.appendChild(span);

					switch (prop) {
						case "body":
						case "mind":
							span.innerHTML = stats[prop] ? Math.floor(stats[prop].total) : 0;
							break;
						default:
							span.innerHTML = stats[prop] ? stats[prop].total : 0;
							break;
					}

					if (stats[prop] && stats[prop].bonus !== undefined && stats[prop].bonus != 0) {
						span = document.createElement("span");
						span.innerHTML = ` (${stats[prop].base + (stats[prop].bonus >= 0 ? " + " : " - ") + Math.abs(stats[prop].bonus)})`;
						span.style.opacity = "0.5";
						subdiv.appendChild(span);
					}

					div.insertBefore(subdiv, div.firstChild);
				}

				TOOLTIP.instance.Show(e, div);
			};
			subdiv.ontouchstart = subdiv.onmouseenter;
			div.appendChild(subdiv);
		}

		parent.appendChild(div);
		return div;
	}

	function drawFilter(obj, parent) {
		const div = document.createElement("div");
		div.className = "filter";

		for (let i = 0; i < _filterTags.length; i++) {
			const span = document.createElement("span");
			const tag = _filterTags[i];
			span.innerHTML = tagToShortForm(tag);
			span.onmouseenter = (e) => obj.filter && obj.filter[tag] !== undefined && TOOLTIP.instance.ShowFilterTag(e, firstToUpperCase(tag), obj.filter[tag] == true);
			span.ontouchstart = div.onmouseenter;
			div.appendChild(span);
		}

		parent.appendChild(div);
	}

	function drawCharacterDetails(obj, parent) {
		let div = document.createElement("div");

		drawFilter(obj, drawStats(obj, div));
		drawQualityGrid(obj, div, QUALITY_GRID_WIDTH, QUALITY_GRID_HEIGHT);

		parent.appendChild(div);
		return div;
	}

	function drawFocus(parent) {
		const div = document.createElement("div");
		div.className = "talismans";

		for (let i = 0; i < 9; i++) {
			const subdiv = document.createElement("div");
			if (i != 4) {
				subdiv.className = "talisman";
			}
			else {
				subdiv.className = "focus";
				subdiv.onmouseenter = (e) => TOOLTIP.instance.Show(e, "Focus");
				subdiv.ontouchstart = subdiv.onmouseenter;
			}
			div.appendChild(subdiv);
		}

		parent.appendChild(div);
		return div;
	}

	function drawQualityGrid(obj, parent, width, height) {
		let row;

		let size = width * height;
		let div = document.createElement("div");
		div.className = "qualities";

		for (let i = 0; i < size; i++) {
			const quality = obj.qualities[i];
			const elm = document.createElement("div");
			elm.className = "quality";

			if (obj.qualities.length > size && i == size - 1) {
				elm.onclick = () => expandQualities(obj);
				elm.onmouseenter = (e) => TOOLTIP.instance.Show(e, TOOLTIP.GetContent(TOOLTIP.Quality.OpenTab));
				elm.ontouchstart = elm.onmouseenter;

				const span = document.createElement("span");
				span.innerHTML = `+${obj.qualities.length - size + 1}`;

				elm.appendChild(span);
				elm.style.backgroundImage = "url(assets/gui/quality_tab_icon.png)";
			}
			else if (quality) {
				const showImage = quality && quality.image_url !== undefined && (quality.nsfw !== true || !SETTINGS.Get("sfw", false));

				elm.onmouseenter = (e) => TOOLTIP.instance.ShowQuality(e, quality, obj === _myself, showImage && _missingImages.indexOf(quality.image_url) < 0 ? quality.image_url : null);
				elm.ontouchstart = elm.onmouseenter;

				if (showImage && _missingImages.indexOf(quality.image_url) < 0) {
					elm.style.backgroundImage = `url(${quality.image_url})`;
					loadImage(quality.image_url).catch(() => {
						elm.style.backgroundImage = "url(assets/gui/default_icon.png)";
						if (_missingImages.indexOf(quality.image_url) < 0) {
							_missingImages.push(quality.image_url);
						}
					});
				}
				else {
					elm.style.backgroundImage = "url(assets/gui/default_icon.png)"
				}
			}

			if (i % width == 0) {
				row = document.createElement("div");
				div.appendChild(row);
			}

			row.appendChild(elm);
		}

		parent.appendChild(div);
		return div;
	};

	function calculateStats(obj, label) {
		const stats = {};
		if (obj.stats) {
			let stat = obj.stats[label.toLowerCase()];
			switch (stat && label) {
				case "Strength":
					stats.action = { total: Math.max(stat / 3 + obj.stats.action, 1).toFixed(2), base: (stat / 3).toFixed(2), bonus: obj.stats.action.toFixed(2) };
					stats.body = { total: Math.max(stat * 50 + obj.stats.body, 0), base: Math.floor(stat * 50), bonus: Math.floor(obj.stats.body) };
					stats.strength = { total: stat.toFixed(2) };
					break;
				case "Dexterity":
					stats.evasion = { total: Math.max(stat + obj.stats.evasion, 0).toFixed(2), base: stat.toFixed(2), bonus: obj.stats.evasion.toFixed(2) };
					stats.hit = { total: Math.max(stat + obj.stats.hit, 0).toFixed(2), base: stat.toFixed(2), bonus: obj.stats.hit.toFixed(2) };
					stats.dexterity = { total: stat.toFixed(2) };
					break;
				case "Intelligence":
					stats.spell = { total: Math.max(stat / 3 + obj.stats.spell, 1).toFixed(2), base: (stat / 3).toFixed(2), bonus: obj.stats.spell.toFixed(2) };
					stats.mind = { total: Math.max(stat * 50 + obj.stats.mind, 0), base: Math.floor(stat * 50), bonus: Math.floor(obj.stats.mind) };
					stats.intelligence = { total: stat.toFixed(2) };
					break;
				case "Willpower":
					stats.resistance = { total: Math.max(stat + obj.stats.resistance, 0).toFixed(2), base: stat.toFixed(2), bonus: obj.stats.resistance.toFixed(2) };
					stats.penetration = { total: Math.max(stat + obj.stats.penetration, 0).toFixed(2), base: stat.toFixed(2), bonus: obj.stats.penetration.toFixed(2) };
					stats.willpower = { total: stat.toFixed(2) };
					break;
			}
		}
		return stats;
	}

	function tagToShortForm(tag) {
		switch (tag) {
			case "absorption":
				return "Abs";
			case "animal":
				return "Ani";
			case "anthro":
				return "Ant";
			case "inanimate":
				return "Ina";
			case "monster":
				return "Mon";
			case "sissification":
				return "Si";
			case "transgender":
				return "Tg";
			default:
				return null;
		}
	}

	function shortFormToTag(shortForm) {
		switch (shortForm) {
			case "Abs":
				return "absorption";
			case "Ani":
				return "animal";
			case "Ant":
				return "anthro";
			case "Ina":
				return "inanimate";
			case "Mon":
				return "monster";
			case "Si":
				return "sissification";
			case "Tg":
				return "transgender";
			default:
				return null;
		}
	}
})();