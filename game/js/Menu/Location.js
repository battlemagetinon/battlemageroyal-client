
(() => {
	let _currentTab;

	MENU.Location = {
		Open: () => {
			const turnEvents = GAME_MANAGER.instance.turnEvents;

			let menu = MENU.Location.menu;
			let table;
			let tr;
			let ul;
			let toggle;

			if (menu) {
				table = menu.getElementsByTagName("table")[0];
				ul = menu.getElementsByTagName("ul")[0];
				toggle = menu.getElementsByTagName("input")[0];
				clearHTML(table);
			}
			else {
				menu = document.createElement("div");
				menu.id = "menu_locations";

				const div = document.createElement("div");
				div.innerHTML = "Select next location";

				menu.appendChild(div);

				ul = document.createElement("ul");
				ul.className = "tabs";

				const tabs = ["Campus", "Events", "Faculty"];

				for (let i = 0; i < tabs.length; i++) {
					const li = document.createElement("li");
					const tabLabel = tabs[i].toLowerCase();

					const span = document.createElement("span");
					span.innerHTML = tabs[i];

					li.className = tabLabel;
					li.appendChild(span);
					li.onclick = () => {
						menu.className = tabLabel + " right";
						_currentTab = tabLabel;
						MENU.Location.Open();
					};

					switch (tabLabel) {
						case "campus":
							menu.className = tabLabel;
							break;
						case "faculty":
							li.style.display = LOCATION.GetLocationsByType("faculty").length > 0 ? "" : "none";
							break;
					}

					ul.appendChild(li);
				}

				menu.appendChild(ul);

				table = document.createElement("table");
				menu.appendChild(table);

				const form = createForm();
				ul = document.createElement("ul");

				toggle = drawToggle(ul, "wait_for_encounter", "Wait for Encounter", "When enabled, will wait indefinitely for an encounter to happen instead of entering the location immediately");
				toggle.checked = SETTINGS.Get("wait_for_encounter", false);
				toggle.onchange = (e) => {
					SETTINGS.Set("wait_for_encounter", e.target.checked);
					MENU.Location.Refresh();
				};

				form.appendChild(ul);
				menu.appendChild(form);

				displayMenu(menu, "right");
			}

			_currentTab = _currentTab || "campus";
			menu.className = _currentTab + " right";

			if (_currentTab == "campus") {
				toggle.parentNode.style.opacity = "";
				toggle.parentNode.style.pointerEvents = "";
			}
			else {
				toggle.parentNode.style.opacity = "0.5";
				toggle.parentNode.style.pointerEvents = "none";
			}

			let type;
			switch (_currentTab) {
				case "faculty":
				case "campus":
					type = _currentTab;
					break;
				default:
					type = "campus";
			}

			const locations = LOCATION.GetLocationsByType(type);

			for (let i = 0; i < 6; i++) {
				if (i % 3 == 0) {
					tr = document.createElement("tr");
					table.appendChild(tr);
				}

				const td = document.createElement("td");
				const elm = document.createElement("div");
				elm.className = "location";

				td.appendChild(elm);
				tr.appendChild(td);

				if (i >= locations.length) {
					elm.style.border = "none";
					elm.style.background = "#000";
					elm.style.opacity = 0.5;
					continue;
				}

				const locationLabel = locations[i];
				const location = GAME_MANAGER.instance.discoveredLocations[locationLabel];
				const backgroundURL = LOCATION.GetBackgroundURL(location, CALENDAR.instance.daytime, true);

				elm.style.backgroundImage = `url(${backgroundURL})`;

				const div = document.createElement("div");
				let span = document.createElement("span");
				span.innerHTML = location.name;

				div.appendChild(span);
				elm.appendChild(div);

				elm.onclick = () => {
					GAME_MANAGER.instance.Send({ action: "Location", nextLocation: locationLabel, event: _currentTab === "events", waitForEncounter: SETTINGS.Get("wait_for_encounter", false) });
					MENU.CloseRightMenu();
				};

				switch (_currentTab) {
					case "faculty":
						if (GAME_MANAGER.instance.enabledFacultyLocations.indexOf(locationLabel) < 0) {
							span = document.createElement("span");
							span.innerHTML = "Closed for the day";
							div.appendChild(span);
							elm.style.opacity = 0.5;
							elm.onclick = null;
						}
						break;
					case "events":
						if (locationLabel in turnEvents) {
							span = document.createElement("span");
							span.innerHTML = turnEvents[locationLabel];
							div.appendChild(span);
							elm.classList.add("event");
						}
						else {
							elm.style.opacity = 0.5;
							elm.onclick = null;
						}
						break;
				}
			}
		},
		Close: () => {
			if (MENU.Location.menu) {
				MENU.CloseRightMenu();
				return true;
			}
			return false;
		},
		Refresh: () => MENU.Location.menu && MENU.Location.Open(),
		Toggle: () => !MENU.Location.Close() && MENU.Location.Open(),
	};

	Object.defineProperties(MENU.Location, {
		'menu': {
			get: () => document.getElementById("menu_locations")
		},
	});
})();
