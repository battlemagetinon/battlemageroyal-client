
(() => {
	let _menu;

	MENU.Skills = {
		Open: () => {
			let prevScrolTop;
			let skillList;
			let title;

			if (_menu) {
				title = _menu.getElementsByClassName("title")[0];
				skillList = _menu.getElementsByTagName("table")[0];
				prevScrolTop = skillList.parentNode.scrollTop;

				if (_menu.parentNode) {
					MENU.CloseLeftMenu();
				}

				clearHTML(skillList);
			}
			else {
				_menu = document.createElement("div");
				_menu.id = "menu_skills";

				title = document.createElement("div");
				title.className = "title";

				_menu.appendChild(title);

				skillList = document.createElement("table");
				let div = document.createElement("div");
				div.className = "item_list skills";
				div.appendChild(skillList);

				_menu.appendChild(div);
			}

			title.innerHTML = LOCATION.instance.player ? `Skills for ${LOCATION.instance.player.names.split(" ")[0]}` : "Skills";

			const skills = GAME_MANAGER.instance.skills;
			const keys = Object.keys(skills);

			if (keys.length > 0) {
				keys.sort();

				for (let i = 0; i < keys.length; i++) {
					const skill = skills[keys[i]];
					const tr = document.createElement("tr");

					let td = document.createElement("td");
					td.className = "skill_name";
					td.innerHTML = skill.skill_name;
					tr.appendChild(td);

					td = document.createElement("td");
					td.className = "level_label";
					switch (skill.level) {
						case 1:
							td.innerHTML = "Beginner";
							break;
						case 2:
							td.innerHTML = "Intermediate";
							break;
						case 3:
							td.innerHTML = "Expert";
							break;
					}
					tr.appendChild(td);

					tr.onmouseenter = (e) => TOOLTIP.instance.ShowSkill(e, skill);
					tr.ontouchstart = tr.onmouseenter;

					skillList.appendChild(tr);
				}
			}
			else {
				const tr = document.createElement("tr");
				tr.className = "hint";

				const td = document.createElement("td");
				td.innerHTML = "Attend classes to earn skills";
				tr.appendChild(td);

				skillList.appendChild(tr);
			}

			displayMenu(_menu, "left");

			if (prevScrolTop) {
				skillList.parentNode.scrollTop = prevScrolTop;
			}
		},
		Close: () => {
			if (MENU.Skills.menu) {
				MENU.CloseLeftMenu();
				return true;
			}
			return false;
		},
		Refresh: () => MENU.Skills.menu && MENU.Skills.Open(),
		Toggle: () => !MENU.Skills.Close() && MENU.Skills.Open(),
	};

	Object.defineProperties(MENU.Skills, {
		'menu': {
			get: () => document.getElementById("menu_skills")
		},
	});
})();
