(() => {
	const _equipmentSlots = ['head', 'shirt', 'pants', 'shoes', 'undershirt', 'underpants', 'accessory_0', 'accessory_1'];

	let _menu;
	let _heirloomContainerWidth = 3;

	MENU.Inventory = {
		UpdateTier: (tier) => {
			switch (tier) {
				case 5:
				case 4:
					_heirloomContainerWidth = 6;
					break;
				case 3:
					_heirloomContainerWidth = 5;
					break;
				case 2:
					_heirloomContainerWidth = 4;
					break;
				case 1:
				case 0:
				default:
					_heirloomContainerWidth = 3;
					break;
			}
		},
		Open: () => {
			let inventoryTab;
			let heirloomContainer;

			const inventory = GAME_MANAGER.instance.inventory;

			if (_menu) {
				inventoryTab = _menu.getElementsByClassName("inventory")[0].getElementsByClassName("items")[0];
				heirloomContainer = _menu.getElementsByClassName("heirlooms")[0].getElementsByClassName("items")[0];
				clearHTML(inventoryTab);
				clearHTML(heirloomContainer);
			}
			else {
				_menu = document.createElement("div");
				_menu.id = "menu_inventory";

				let grid = document.createElement("table");
				inventoryTab = document.createElement("table");
				inventoryTab.className = "items";

				let div = document.createElement("div");
				div.className = "grid inventory";
				div.appendChild(grid);
				div.appendChild(inventoryTab);

				for (let row = 0; row < MENU.INVENTORY_TAB_HEIGHT; row++) {
					let tr = document.createElement("tr");

					for (let column = 0; column < MENU.INVENTORY_TAB_WIDTH; column++) {
						let td = document.createElement("td");
						tr.appendChild(td);
					}

					grid.appendChild(tr);
				}

				_menu.appendChild(div);

				grid = document.createElement("table");
				heirloomContainer = document.createElement("table");
				heirloomContainer.className = "items";

				div = document.createElement("div");
				div.className = "grid heirlooms";
				div.appendChild(grid);
				div.appendChild(heirloomContainer);

				grid.onmouseenter = (e) => TOOLTIP.instance.Show(e, "If this character is lost, items<br/>in this container carry over<br/>to your next characters");
				grid.ontouchstart = grid.onmouseenter;

				_menu.appendChild(div);

				for (let row = 0; row < MENU.HEIRLOOM_CONTAINER_HEIGHT; row++) {
					let tr = document.createElement("tr");

					for (let column = 0; column < _heirloomContainerWidth; column++) {
						let td = document.createElement("td");
						tr.appendChild(td);
					}

					grid.appendChild(tr);
				}
			}

			drawItemContainer(inventoryTab, inventory.tab, MENU.INVENTORY_TAB_WIDTH, MENU.INVENTORY_TAB_HEIGHT);
			drawItemContainer(heirloomContainer, inventory.heirlooms, _heirloomContainerWidth, MENU.HEIRLOOM_CONTAINER_HEIGHT);

			displayMenu(_menu, "right", MENU.Crafting.pending ? false : null);
		},
		Redraw: () => {
			let menu = MENU.Inventory.menu;
			_menu = null;
			if (menu) {
				MENU.Inventory.Open();
			}
		},
		Close: () => {
			if (_menu && _menu.parentNode) {
				MENU.CloseRightMenu();
				return true;
			}
			return false;
		},
		Refresh: () => {
			if (_menu) {
				_menu.parentNode && MENU.Inventory.Open();
			}
			MENU.Loot.Refresh();
		},
		Toggle: () => !MENU.Inventory.Close() && MENU.Inventory.Open(),
		DrawItem: (parent, item) => {
			if (!item) {
				return null;
			}

			let div = document.createElement("div");
			div.className = `item_${item.base.width}x${item.base.height}`;

			if (item.base.stack > 1 && item.stack > 1) {
				const subdiv = document.createElement("div");
				subdiv.innerHTML = item.stack;
				div.append(subdiv);
			}

			let touchEvent = null;

			div.style.backgroundImage = `url('${IMAGE_PROCESSING.getItemImage(item)}')`;
			div.onmouseenter = (e) => TOOLTIP.instance.ShowItem(e, item);
			div.onmousedown = async () => await CURSOR.instance.WaitForDragStart() && holdItem(item, div);
			div.ontouchstart = (e) => {
				div.onmouseenter(e);
				div.onmousedown();
				touchEvent = e;
			};
			div.ontouchmove = (e) => touchEvent = e;

			div.oncontextmenu = (e) => {
				div.onclick && div.onclick(e);
				e.preventDefault();
			};

			div.onclick = (e) => {
				const touch = touchEvent != null;
				touchEvent = null;
				if (CURSOR.instance.locked) {
					return;
				}
				if (e.shiftKey) {
					if (item.base.stack > 1 && item.stack > 1) {
						UNSTACK.instance.Show(e, item);
					}
				}
				else if (touch || e.button == 2) {
					const actions = [];
					if (item.base.actions !== undefined) {
						for (let prop in item.base.actions) {
							const label = prop;
							if (!item.base.actions[label].public) {
								const action = item.base.actions[label];
								switch (action ? action.target : null) {
									case "item":
									case "equipment":
										actions.push({ label: label, onclick: () => CURSOR.instance.SetItem(item, label, action.target) });
										break;
									default:
										actions.push({ label: label, onclick: () => GAME_MANAGER.instance.Send({ action: "Item", itemId: item.id, itemAction: label }) });
										break;
								}
							}
						}
					}
					if (touch) {
						if (item.base.stack > 1 && item.stack > 1) {
							actions.push({ label: "Unstack", onclick: () => UNSTACK.instance.Show(e, item) });
						}
						if (MENU.Loot.menu) {
							actions.push({ label: "Discard", onclick: () => GAME_MANAGER.instance.DiscardItem(item.id) });
						}
						if (actions.length > 0) {
							actions.unshift({ label: "Move", onclick: () => holdItem(item, div) });
							DROPDOWN.instance.Open(e, actions);
						}
						else {
							holdItem(item, div);
						}
					}
					else if (actions.length > 0 || MENU.Loot.menu) {
						if (MENU.Loot.menu) {
							actions.push({ label: "Discard", onclick: () => GAME_MANAGER.instance.DiscardItem(item.id) });
						}
						if (actions.length === 1 && actions[0].label === "Use") {
							actions[0].onclick(e);
						}
						else {
							DROPDOWN.instance.Open(e, actions);
						}
					}
				}
				else if (e.button == 0) {
					holdItem(item, div);
				}
			};

			parent.appendChild(div);
			return div;
		},
	};

	Object.defineProperties(MENU.Inventory, {
		'menu': {
			get: () => _menu && _menu.parentNode ? _menu : null
		},
		'equipmentSlots': {
			get: () => _equipmentSlots
		},
		'heirloomContainerWidth': {
			get: () => _heirloomContainerWidth
		},
	});

	MENU.Loot = {
		Open: () => {
			let menu = MENU.Loot.menu;

			if (menu) {
				menu.parentNode.removeChild(menu);
				clearHTML(menu);
			}
			else {
				menu = document.createElement("div");
				menu.id = "menu_loot";
			}

			const itemContainer = document.createElement("div");
			itemContainer.className = "item_container";

			const div = document.createElement("div");
			div.className = "grid";
			div.appendChild(itemContainer);

			menu.appendChild(div);

			MENU.Inventory.DrawItem(itemContainer, GAME_MANAGER.instance.GetLoot());
			displayMenu(menu, "left", MENU.Loot.Close);
		},
		Close: () => {
			if (MENU.Loot.menu) {
				const item = GAME_MANAGER.instance.GetLoot();
				if (item) {
					GUI.instance.Alert("Closing the container will throw away the item", "Close").then(close => {
						if (close) {
							GAME_MANAGER.instance.Send({ action: "Item", discard: true, itemId: item.id });
							MENU.CloseLeftMenu();
						}
					});
				}
				else {
					MENU.CloseLeftMenu();
				}
				return true;
			}
		},
		Refresh: () => MENU.Loot.menu && MENU.Loot.Open(),
		Toggle: () => !MENU.Loot.Close() && MENU.Loot.Open(),
	};

	Object.defineProperties(MENU.Loot, {
		'menu': {
			get: () => document.getElementById("menu_loot")
		},
	});

	function drawItemContainer(elm, container, containerWidth, containerHeight) {
		if (container) {
			const items = [];

			for (let row = 0; row < containerHeight; row++) {
				const tr = document.createElement("tr");

				for (let column = 0; column < containerWidth; column++) {
					const td = document.createElement("td");
					tr.appendChild(td);

					const i = row * containerWidth + column;
					if (container[i] && items.indexOf(container[i]) < 0) {
						const item = GAME_MANAGER.instance.GetItem(container[i]);
						if (item) {
							items.push(item.id);
							MENU.Inventory.DrawItem(td, item);
							for (let l = 0; l < item.base.width * item.base.height; l++) {
								const index = i + Math.floor(l / item.base.width) * containerWidth + (l % item.base.width);
								if (!container[index]) {
									container[index] = item.id;
								}
							}
						}
					}
				}

				elm.appendChild(tr);
			}
			return items.length;
		}
		return 0;
	}

	function holdItem(item, node) {
		if (!CURSOR.instance.locked) {
			if (node && node.parentNode) {
				node.parentNode.removeChild(node);
			}
			GAME_MANAGER.instance.HoldItem(item.id);
			CURSOR.instance.SetItem(item);
		}
	}
})();
