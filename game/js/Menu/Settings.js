
(() => {
	const allowedMessagesLabels = ["Everyone", "Confirmed users, Recently met and Friends", "Confirmed users and Friends", "Recently met and Friends", "Friends"];

	MENU.Settings = {
		Open: () => {
			let tr;
			let td;
			let i;

			const dialog = document.getElementById("dialog");
			let menu = MENU.Settings.menu;

			if (menu) {
				menu.parentNode.removeChild(menu);
				clearHTML(menu);
			}
			else {
				menu = document.createElement("div");
				menu.id = "menu_settings";
			}

			let form = createForm();
			form.className = "item_list";

			drawHeader("Display", form);

			let ul = document.createElement("ul");
			let li = document.createElement("li");
			let div = document.createElement("div");
			div.innerHTML = "Text size for chat";

			let select = document.createElement("select");

			let sizes = ["Tiny", "Small", "Normal", "Large"];

			for (i = 0; i < sizes.length; i++) {
				const option = document.createElement("option");
				option.value = sizes[i].toLowerCase();
				option.innerHTML = sizes[i];
				select.appendChild(option);
				dialog.classList.remove(option.value);
			}

			select.value = SETTINGS.Get("text_size_chat", "normal");
			select.onchange = (e) => {
				SETTINGS.Set("text_size_chat", e.target.value);
				for (let i = 0; i < sizes.length; i++) {
					dialog.classList.remove(sizes[i].toLowerCase());
				}
				dialog.classList.add(e.target.value);
				LOCAL_CHAT.Resize();
			};
			dialog.classList.add(select.value);

			li.appendChild(div);
			li.appendChild(select);
			ul.appendChild(li);

			let input = drawToggle(ul, "settings_fullscreen", "Fit to window", "When enabled, game screen will always scale up to fit the window<br/>Default when the browser is in fullscreen");
			input.checked = SETTINGS.Get("fullscreen", false);
			input.onchange = (e) => {
				SETTINGS.Set("fullscreen", e.target.checked);
				document.body.onresize();
			};

			input = drawToggle(ul, "settings_chat_timestamp", "Timestamps in chat", "When enabled, the chat box will display the timestamp for every message");
			input.checked = SETTINGS.Get("chat_timestamp", false);
			input.onchange = (e) => {
				SETTINGS.Set("chat_timestamp", e.target.checked);
				e.target.checked ? dialog.classList.add("timestamped") : dialog.classList.remove("timestamped")
			};

			input = drawToggle(ul, "settings_background_parallax", "Background parallax", "When enabled, the background will have an animated parallax effect");
			input.checked = SETTINGS.Get("background_parallax", true);
			input.onchange = (e) => {
				SETTINGS.Set("background_parallax", e.target.checked);
				GUI.instance.parallaxEnabled = e.target.checked;
			};

			input = drawToggle(ul, "settings_sfw", "SFW", "When enabled, NSFW images will not be displayed");
			input.checked = SETTINGS.Get("sfw", false);
			input.onchange = (e) => {
				SETTINGS.Set("sfw", e.target.checked);
				MENU.Inspect.Refresh();
				MENU.Myself.Refresh();
			};

			form.appendChild(ul);

			drawHeader("Content filter", form);

			let table = document.createElement("table");
			table.className = "content_filter";

			const tags = ["Absorption", "Animal", "Anthro", "Inanimate", "Monster", "Sissification", "Transgender"];
			const filter = GAME_MANAGER.instance.filter;

			for (i = 0; i < tags.length; i++) {
				const tag = tags[i];

				if (i % 3 == 0) {
					tr = document.createElement("tr");
					table.appendChild(tr);
				}

				td = document.createElement("td");
				td.className = "enable";

				const input = document.createElement("input");
				input.checked = filter[tag.toLowerCase()];
				input.type = "checkbox";
				input.id = "settings_" + tag.toLowerCase();

				const label = document.createElement("label");
				label.htmlFor = input.id;

				input.onchange = () => {
					GAME_MANAGER.instance.Send({ action: "Settings", filter: { [tag.toLowerCase()]: input.checked } });
					input.checked = !input.checked;
				};
				label.onmouseenter = (e) => TOOLTIP.instance.Show(e, TOOLTIP.GetContent(TOOLTIP.Tag[tag]));
				label.ontouchstart = label.onmouseenter;

				const span = document.createElement("span");
				span.innerHTML = tag;

				label.appendChild(span);
				td.appendChild(input);
				td.appendChild(label);
				tr.appendChild(td);
			}

			form.appendChild(table);

			drawHeader("Hotkeys", form);

			const hotkeysByLabel = CONTROLS.GetHotkeysByLabel();
			const hotkeysTable = document.createElement("table");
			hotkeysTable.className = "hotkeys";

			i = 0;
			for (let prop in hotkeysByLabel) {
				const label = prop;
				let prevHotkey = hotkeysByLabel[prop];

				if (i++ % 3 === 0) {
					tr = document.createElement("tr");
					hotkeysTable.appendChild(tr);
				}

				td = document.createElement("td");

				const input = document.createElement("input");
				input.type = "text";
				input.value = hotkeysByLabel[prop];

				input.onblur = () => input.value = prevHotkey;
				input.onfocus = () => {
					prevHotkey = input.value;
					input.value = "";
				}
				input.onkeydown = (e) => {
					switch (e.key) {
						case "Escape":
						case "Enter":
							input.blur();
							e.stopImmediatePropagation();
							break;
						case "Alt":
						case "Control":
						case "Shift":
							break;
						default:
							const hotkey = CONTROLS.InputToHokey(e);
							if (hotkey !== prevHotkey) {
								for (let label in hotkeysByLabel) {
									if (hotkeysByLabel[label] === hotkey) {
										hotkeysByLabel[label] = "";
										const list = hotkeysTable.getElementsByTagName("input");
										for (let i = 0; i < list.length; i++) {
											if (list[i].value == hotkey) {
												list[i].value = "";
											}
										}
									}
								}
								prevHotkey = hotkey;
								hotkeysByLabel[label] = hotkey;
								SETTINGS.Set("hotkeys", hotkeysByLabel);
							}
							input.blur();
							e.stopImmediatePropagation();
							break;
					}
				}

				div = document.createElement("div");
				div.innerHTML = prop;
				td.appendChild(div);
				td.appendChild(input);
				tr.appendChild(td);
			}

			form.appendChild(hotkeysTable);

			drawHeader("Social", form);

			ul = document.createElement("ul");
			li = document.createElement("li");

			div = document.createElement("div");
			div.innerHTML = "Username color";
			li.appendChild(div);

			div = document.createElement("div");
			div.className = "username_color";
			div.style.color = "#0E0E0E";

			if (GAME_MANAGER.instance.settings.usernameColor) {
				div.innerHTML = colorCodeToName(GAME_MANAGER.instance.settings.usernameColor);
				div.style.color = GAME_MANAGER.instance.settings.usernameColor;
			}
			else {
				div.innerHTML = "Default";
				div.style.color = colorRefToCode(null);
			}

			table = document.createElement("table");
			table.className = "color_palette";

			if (GAME_MANAGER.instance.tier < 2) {
				table.classList.add("disabled");
			}

			let footer = document.createElement("tfoot");
			td = document.createElement("td");
			td.innerHTML = "Exclusive to players who pledge $5 on Patreon";
			footer.appendChild(td);

			let colors = [
				"Indian Red", "Violet", "Sky Blue", "Cyan", "Aqua Marine",
				"Red", "Purple", "Sapphire", "Teal", "Spring Green",
				"Dark Red", "Amethyst", "Azure", "Genoa", "Emerald",
				"Carrot", "Cream", "Gold", "Amaranth", "Lime",
				"Orange", "Sunset", "Mustard", "Hot Pink", "Water Sprout",
				"Rust", "Bronze", "Medallion", "Gunmetal", "Default",
			];

			for (let i = 0; i < colors.length; i++) {
				if (i % 5 == 0) {
					tr = document.createElement("tr");
					table.appendChild(tr);
				}

				td = document.createElement("td");
				td.className = nameToRef(colors[i]);
				const span = document.createElement("span");
				span.innerHTML = colors[i];
				td.style.background = colorRefToCode(td.className);
				td.onclick = (e) => GAME_MANAGER.instance.Send({ action: "Settings", usernameColor: colorRefToCode(e.currentTarget.className) });
				td.appendChild(span);
				tr.appendChild(td);
			}

			table.appendChild(footer);

			li.appendChild(div);
			ul.appendChild(li);
			form.appendChild(ul)
			form.appendChild(table);

			ul = document.createElement("ul");

			input = drawToggle(ul, "settings_roleplaying", "Roleplaying", "Signals to the game and other players that you are interested in roleplaying");
			input.checked = GAME_MANAGER.instance.settings.roleplaying;
			input.onchange = (e) => {
				GAME_MANAGER.instance.Send({ action: "Settings", roleplaying: e.currentTarget.checked });
				e.currentTarget.checked = !e.currentTarget.checked;
			};

			li = document.createElement("li");
			div = document.createElement("div");
			div.innerHTML = "Allow messages from";
			div.onmouseenter = (e) => TOOLTIP.instance.Show(e, "Used to prevent strangers from sending you private messages, friend requests, meetup requests, and invites.<br/>Confirmed users are accounts with a confirmed email address and recently met are the last up to 20 characters you have encountered.");

			select = document.createElement("select");
			select.id = "allowed_messages";

			for (i = 0; i < allowedMessagesLabels.length; i++) {
				const option = document.createElement("option");
				option.value = allowedMessagesLabels[i].toLowerCase();
				option.innerHTML = allowedMessagesLabels[i];
				select.appendChild(option);
			}

			select.value = allowedMessagesIntToLabel(GAME_MANAGER.instance.settings.allowedMessages);
			select.onchange = (e) => {
				GAME_MANAGER.instance.Send({ action: "Settings", allowedMessages: allowedMessagesLabelToInt(e.target.value) })
				select.value = allowedMessagesIntToLabel(GAME_MANAGER.instance.settings.allowedMessages);
			};

			li.appendChild(div);
			li.appendChild(select);
			ul.appendChild(li);
			
			form.appendChild(ul)

			drawHeader("Notifications", form);

			ul = document.createElement("ul");
			li = document.createElement("li");
			li.className = "enable";

			const browserNotificationInput = document.createElement("input");
			browserNotificationInput.type = "checkbox";
			browserNotificationInput.id = "settings_browser_notifications";

			let label = document.createElement("label");
			label.onmouseenter = (e) => TOOLTIP.instance.Show(e, "Displays browser notifications when things happen in the game while away.<br/>Select when you want to be notified from the list below.");
			label.ontouchstart = label.onmouseenter;

			if (!NOTIFICATION.browserSupport) {
				li.classList.add("inactive");
			}
			else {
				if (SETTINGS.Get("browser_notifications", false)) {
					if (Notification.permission === "granted") {
						browserNotificationInput.checked = true;
					}
					else {
						SETTINGS.Remove("browser_notifications");
					}
				}
				label.htmlFor = browserNotificationInput.id;
				browserNotificationInput.onchange = async () => {
					if (browserNotificationInput.checked && Notification.permission !== "granted") {
						browserNotificationInput.checked = false;
						if (Notification.permission === "denied") {
							GUI.instance.DisplayMessage("Browser notifications are blocked. Please update permissions in your browser settings");
						}
						else {
							let permission = await Notification.requestPermission();
							if (permission === "granted") {
								browserNotificationInput.checked = true;
							}
							SETTINGS.Set("browser_notifications", browserNotificationInput.checked);
							menu.getElementsByClassName("browser_notifications")[0].classList.remove("inactive");
						}
					}
					else {
						SETTINGS.Set("browser_notifications", browserNotificationInput.checked);
						if (SETTINGS.Get("browser_notifications", false)) {
							menu.getElementsByClassName("browser_notifications")[0].classList.remove("inactive");
						}
						else {
							menu.getElementsByClassName("browser_notifications")[0].classList.add("inactive");
						}
					}
				};
			}

			let span = document.createElement("span");
			span.innerHTML = "Browser Notifications";

			label.appendChild(span);
			li.appendChild(browserNotificationInput);
			li.appendChild(label);
			ul.appendChild(li);

			li = document.createElement("li");
			li.style.marginBottom = "0";
			div = document.createElement("div");
			div.innerHTML = "Notify on";
			li.appendChild(div);
			ul.appendChild(li);

			table = document.createElement("table");
			table.className = "browser_notifications";

			if (!NOTIFICATION.browserSupport || Notification.permission !== "granted" || !SETTINGS.Get("browser_notifications", false)) {
				table.classList.add("inactive");
			}

			const browserNotifications = [
				{ label: "Private message", tooltip: "Get a notification when you receive a new private message", key: "on_private_message" },
				{ label: "Chat message", tooltip: "Get a notification when you receive a new chat message", key: "on_chat_message" },
				{ label: "Combat message", tooltip: "Get a notification when you become the target of an attack", key: "on_combat_log" },
				{ label: "Meetup", tooltip: "Get a notification when you receive a meetup invite or request", key: "on_meetup" },
				{ label: "Encounter", tooltip: "Get a notification when you encounter another player while changing location", key: "on_encounter" },
				{ label: "Encounter ended", tooltip: "Get a notification when an opponent has left your location", key: "on_encounter_ended" },
				{ label: "Friend request", tooltip: "Get a notification when you receive a new friend request", key: "on_friend_request" },
				{ label: "Friend online", tooltip: "Get a notification when a friend comes online", key: "on_friend_online" },
			];

			for (let i = 0; i < browserNotifications.length; i++) {
				const notification = browserNotifications[i];

				if (i % 3 == 0) {
					tr = document.createElement("tr");
					table.appendChild(tr);
				}

				td = document.createElement("td");
				td.className = "enable";

				const input = document.createElement("input");
				input.checked = SETTINGS.Get(`browser_notification_${notification.key}`, true);
				input.type = "checkbox";
				input.id = `settings_browser_notification_${notification.key}`;
				input.onchange = () => {
					SETTINGS.Set(`browser_notification_${notification.key}`, input.checked);
					NOTIFICATION.instance.BrowserNotification("Friend came online");
				};

				const label = document.createElement("label");
				label.htmlFor = input.id;
				label.onmouseenter = (e) => TOOLTIP.instance.Show(e, notification.tooltip);
				label.ontouchstart = label.onmouseenter;

				const span = document.createElement("span");
				span.innerHTML = notification.label;

				label.appendChild(span);
				td.appendChild(input);
				td.appendChild(label);
				tr.appendChild(td);
			}

			form.appendChild(ul)
			form.appendChild(table);

			drawHeader("Inventory", form);

			ul = document.createElement("ul");

			input = drawToggle(ul, "settings_auto_wear", "Wear when equipped", "While enabled, equipment will be worn immediately after being equipped");
			input.checked = GAME_MANAGER.instance.settings.autoWear;
			input.onchange = (e) => {
				GAME_MANAGER.instance.Send({ action: "Settings", autoWear: e.currentTarget.checked });
				e.currentTarget.checked = !e.currentTarget.checked;
			};

			input = drawToggle(ul, "settings_item_image_tooltip", "Display item images in tooltips", "While enabled, images of each item will be displayed in their tooltip");
			input.checked = SETTINGS.Get("item_image_tooltip", true);
			input.onchange = (e) => SETTINGS.Set("item_image_tooltip", e.target.checked);

			form.appendChild(ul);

			input = drawToggle(ul, "settings_tooltip_background_animated", "Animate tooltip accessory backgrounds", "While enabled, background will be animated for tooltips with item accessory graphics.");
			input.checked = SETTINGS.Get("tooltip_background_animated", true);
			input.onchange = (e) => SETTINGS.Set("tooltip_background_animated", e.target.checked);

			form.appendChild(ul);

			div = document.createElement("div");
			div.className = "reset button";
			input = document.createElement("input");
			input.type = "button";
			input.value = "Reset To Default";
			input.onclick = async () => {
				if (await GUI.instance.Alert("Do you wish to reset all settings to default?", "Yes", "No")) {
					SETTINGS.Reset();
					MENU.Settings.Refresh();
				}
			};
			div.appendChild(input);
			form.appendChild(div);

			menu.appendChild(form);

			displayMenu(menu, "left");
		},
		UpdateFilter: (filter) => {
			let input;
			for (let tag in filter) {
				input = document.getElementById(`settings_${tag}`);
				if (input) {
					input.checked = filter[tag];
				}
			}
		},
		UpdateRoleplaying: (enabled) => {
			let input = document.getElementById("settings_roleplaying");
			if (input) {
				input.checked = enabled;
			}
		},
		UpdateTier: (tier) => {
			if (MENU.Settings.menu) {
				let elm = MENU.Settings.menu.getElementsByClassName("color_palette")[0];
				elm.getElementsByClassName("default")[0].style.background = colorRefToCode("default");
				tier >= 2 ? elm.classList.remove("disabled") : elm.classList.add("disabled");
			}
		},
		UpdateUsernameColor: (colorCode) => {
			if (MENU.Settings.menu) {
				let elm = MENU.Settings.menu.getElementsByClassName("username_color")[0];
				elm.innerHTML = colorCodeToName(colorCode);
				elm.style.color = colorCode;
			}
		},
		UpdateAutoWear: (enabled) => {
			let input = document.getElementById("settings_auto_wear");
			if (input) {
				input.checked = enabled;
			}
		},
		UpdateAllowedMessages: (messages) => {
			let selection = document.getElementById("allowed_messages");
			if (selection) {
				selection.value = allowedMessagesIntToLabel(messages);
			}
		},
		Close: () => {
			if (MENU.Settings.menu) {
				MENU.CloseLeftMenu();
				return true;
			}
			return false;
		},
		Refresh: () => MENU.Settings.menu && MENU.Settings.Open(),
		Toggle: () => !MENU.Settings.Close() && MENU.Settings.Open(),
	};

	Object.defineProperties(MENU.Settings, {
		'menu': {
			get: () => document.getElementById("menu_settings")
		},
	});

	function allowedMessagesLabelToInt(label) {
		label = label.toLowerCase();
		for (let i = 0; i < allowedMessagesLabels.length; i++) {
			if (allowedMessagesLabels[i].toLowerCase() == label) {
				return i;
			}
		}
		return 0;
	}

	function allowedMessagesIntToLabel(i) {
		return allowedMessagesLabels[clamp(i, 0, allowedMessagesLabels.length)].toLowerCase();
	}

	function colorRefToCode(ref) {
		switch (ref) {
			case "default":
			default:
				return GAME_MANAGER.instance.tier >= 2 ? "#FFFF00" : "#FFFFFF";
			case "indian_red":
				return "#F1948A";
			case "red":
				return "#E74C3C";
			case "dark_red":
				return "#B03A2E";
			case "violet":
				return "#BB8FCE";
			case "purple":
				return "#8E44AD";
			case "amethyst":
				return "#6C3483";
			case "sky_blue":
				return "#85C1E9";
			case "sapphire":
				return "#3498DB";
			case "azure":
				return "#2874A6";
			case "cyan":
				return "#73C6B6";
			case "teal":
				return "#16A085";
			case "genoa":
				return "#107964";
			case "aqua_marine":
				return "#82E0AA";
			case "spring_green":
				return "#2ECC71";
			case "emerald":
				return "#239B56";
			case "gold":
				return "#F7DC6F";
			case "mustard":
				return "#F1C40F";
			case "medallion":
				return "#B7950B";
			case "cream":
				return "#F8C471";
			case "sunset":
				return "#F39C12";
			case "bronze":
				return "#B9770E";
			case "carrot":
				return "#E59866";
			case "orange":
				return "#D35400";
			case "rust":
				return "#A04000";
			case "water_sprout":
				return "#0FFFFF";
			case "hot_pink":
				return "#FF69B4";
			case "gray":
				return "#626567";
			case "gunmetal":
				return "#808B96";
			case "amaranth":
				return "#E52B50";
			case "lime":
				return "#00FF00";
		}
	};

	function colorCodeToName(code) {
		switch (code && code.toUpperCase()) {
			case "#F1948A":
				return "Indian Red";
			case "#E74C3C":
				return "Red";
			case "#B03A2E":
				return "Dark Red";
			case "#BB8FCE":
				return "Violet";
			case "#8E44AD":
				return "Purple";
			case "#6C3483":
				return "Amethyst";
			case "#85C1E9":
				return "Sky Blue";
			case "#3498DB":
				return "Sapphire";
			case "#2874A6":
				return "Azure";
			case "#73C6B6":
				return "Cyan";
			case "#16A085":
				return "Teal";
			case "#107964":
				return "Genoa";
			case "#82E0AA":
				return "Aqua Marine";
			case "#2ECC71":
				return "Spring Green";
			case "#239B56":
				return "Emerald";
			case "#F7DC6F":
				return "Gold";
			case "#F1C40F":
				return "Mustard";
			case "#B7950B":
				return "Medallion";
			case "#F8C471":
				return "Cream";
			case "#F39C12":
				return "Sunset";
			case "#B9770E":
				return "Bronze";
			case "#E59866":
				return "Carrot";
			case "#D35400":
				return "Orange";
			case "#A04000":
				return "Rust";
			case "#0FFFFF":
				return "Water Sprout";
			case "#FF69B4":
				return "Hot Pink";
			case "#626567":
				return "Gray";
			case "#808B96":
				return "Gunmetal";
			case "#E52B50":
				return "Amaranth";
			case "#00FF00":
				return "Lime";
			default:
				return "Default";
		}
	};
})();
