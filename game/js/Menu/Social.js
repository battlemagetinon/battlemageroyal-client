(() => {
	const MAX_SCENARION_DETAILS_LENGTH = 1000;

	let _characterFilter;
	let _roleplayScenarios;
	let _details;
	let _titleInput;
	let _loadSymbol;

	let _searchId = 0;
	let _detailsByUsername = {};

	const FriendRequest = {
		Sent: 0,
		Received: 1,
		Accepted: 2,
		Online: 3,
	};

	MENU.Social = {
		Open: () => {
			let menu = MENU.Social.menu;
			let friends = GAME_MANAGER.instance.friends;
			let recentCharacters = GAME_MANAGER.instance.recentCharacters;
			let ignoredUsers = GAME_MANAGER.instance.ignoredUsers;
			let prevScrolTop;
			let characters;
			let span;
			let div;

			if (menu) {
				characters = menu.getElementsByClassName("characters")[0];
				prevScrolTop = characters.scrollTop;

				clearHTML(characters);

				if ('pointerEvents' in menu.style) {
					menu.style.pointerEvents = "";
				}
			}
			else {
				characters = document.createElement("ul");
				characters.className = "characters";
			}

			for (let i = 0; i < friends.length; i++) {
				const arr = [];
				let b = false;
				for (let prop in friends[i]) {
					let obj = Object.assign({ username: prop }, friends[i][prop]);
					if (obj.last_online) {
						obj.last_online = Date.now() - new Date(obj.last_online * 1000).getTime();
					}
					for (let l = 0; l < arr.length; l++) {
						switch (i) {
							case FriendRequest.Accepted:
								if (obj.last_online !== undefined) {
									b = obj.last_online < arr[l].last_online;
									break;
								}
							case FriendRequest.Online:
							case FriendRequest.Received:
							case FriendRequest.Sent:
								b = obj.username < arr[l].username;
								break;
						}
						if (b) {
							arr.splice(l, 0, obj);
							break;
						}
					}
					if (!b) {
						arr.push(obj);
					}
				}
				friends[i] = arr;
			}

			friends.splice(2, 0, friends.pop());

			for (let i = 0; i < friends.length; i++) {
				for (let l = 0; l < friends[i].length; l++) {
					const obj = friends[i][l];
					const username = obj.username;
					const li = document.createElement("li");
					li.className = "friend";

					let characterNames = getCharacterNames(obj);
					span = document.createElement("span");
					span.innerHTML = characterNames;
					li.appendChild(span);

					span = document.createElement("span");
					span.innerHTML = obj.username;
					if (obj.username_color) {
						span.style.color = obj.username_color;
					}
					if (characterNames.length == 0) {
						span.style.marginLeft = "0";
					}
					li.appendChild(span);

					if (i < 2) {
						div = document.createElement("div");
						div.className = "request";

						if (i == 1) {
							span = document.createElement("span");
							span.className = "accept";
							div.appendChild(span);

							span.onclick = () => {
								GAME_MANAGER.instance.AcceptFriend(username);
								menu.style.pointerEvents = "none";
							};
						}

						span = document.createElement("span");
						span.className = i == 0 ? "cancel" : "reject";
						div.appendChild(span);

						span.onclick = () => {
							GAME_MANAGER.instance.RemoveFriend(username);
							menu.style.pointerEvents = "none";
						};

						li.appendChild(div);
					}
					else {
						let actions = [
							{
								label: "Inspect",
								onclick: () => MENU.Inspect.Open(GUI.Position.Left, null, username)
							},
							{
								label: "Profile",
								onclick: () => window.open(`/user/${username}`)
							},
						];

						span = document.createElement("span");
						if (obj.status) {
							actions = actions.concat([
								{
									label: "Invite",
									onclick: async () => GAME_MANAGER.instance.Invite(username)
								},
								{
									label: "Request Invite",
									onclick: async () => GAME_MANAGER.instance.Invite(username, true)
								}
							]);
							span.innerHTML = obj.status;
						}
						else if (obj.last_online !== undefined) {
							s = Math.floor(obj.last_online / 1000);

							if (s < 60) {
								m = 0;
								span.innerHTML = "A moment";
							}
							else if (s < 60 * 60) {
								m = Math.floor(s / 60);
								span.innerHTML = m + " minute";
							}
							else if (s < 60 * 60 * 24) {
								m = Math.floor(s / (60 * 60));
								span.innerHTML = m + " hour";
							}
							else if (s < 60 * 60 * 24 * 7) {
								m = Math.floor(s / (60 * 60 * 24));
								span.innerHTML = m + " day";
							}
							else if (s < 60 * 60 * 24 * 7 * 4) {
								m = Math.floor(s / (60 * 60 * 24 * 7));
								span.innerHTML = m + " week";
							}
							else if (s < 60 * 60 * 24 * 365) {
								m = Math.floor(s / (60 * 60 * 24 * 7 * 4));
								span.innerHTML = m + " month";
							}
							else {
								m = Math.floor(s / (60 * 60 * 24 * 365));
								span.innerHTML = m + " year";
							}
							span.innerHTML += (m > 1) ? "s ago" : " ago";
							span.className = "last_online";
							li.classList.add("offline");
						}

						li.appendChild(span);

						actions = actions.concat([
							{
								label: "Send Message",
								onclick: () => MENU.Messages.Open(username)
							},
							{
								label: "Edit Note",
								onclick: async () => {
									let note = await GUI.instance.Prompt("Edit Note", obj.note || "", "Submit");
									if (note !== false) {
										GAME_MANAGER.instance.Send({ action: "Friend", user: username, note: note });
									}
								}
							},
							{
								label: "Remove Friend",
								onclick: async () => {
									if (await GUI.instance.Alert(`Remove ${username} as friend?`, "Yes", "No")) {
										GAME_MANAGER.instance.RemoveFriend(username);
										menu.style.pointerEvents = "none";
									}
								}
							},
						]);

						li.onclick = (e) => DROPDOWN.instance.Open(e, actions);
						li.onmouseenter = (e) => {
							if (obj.note) {
								const noScripting = document.createElement("div");
								noScripting.textContent = obj.note;
								TOOLTIP.instance.Show(e, noScripting);
							}
						}
					}

					characters.appendChild(li);
				}
			}

			for (let i = 0; i < recentCharacters.length; i++) {
				const li = document.createElement("li");
				li.className = "recent";

				span = document.createElement("span");
				span.innerHTML = getCharacterNames(recentCharacters[i]);
				li.appendChild(span);

				span = document.createElement("span");
				span.innerHTML = recentCharacters[i].username;
				if (recentCharacters[i].username_color) {
					span.style.color = recentCharacters[i].username_color;
				}
				li.appendChild(span);

				const username = recentCharacters[i].username;
				const token = recentCharacters[i].id_token;

				li.onclick = (e) => DROPDOWN.instance.Open(e, [
					{
						label: "Inspect",
						onclick: () => MENU.Inspect.Open(GUI.Position.Left, token)
					},
					{
						label: "Profile",
						onclick: () => window.open(`/character/${token}`)
					},
					{
						label: "Invite",
						onclick: async () => GAME_MANAGER.instance.Invite(username)
					},
					{
						label: "Request Invite",
						onclick: async () => GAME_MANAGER.instance.Invite(username, true)
					},
					{
						label: "Send Message",
						onclick: () => MENU.Messages.Open(username)
					},
					{
						label: "Add Friend",
						onclick: () => {
							GAME_MANAGER.instance.AddFriend(username, false, token);
							menu.style.pointerEvents = "none";
						}
					},
					GAME_MANAGER.instance.IsIgnored(username) ? {
						label: "Unignore",
						onclick: () => {
							GAME_MANAGER.instance.UnignoreUser(username);
							menu.style.pointerEvents = "none";
						}
					} : {
						label: "Ignore",
						onclick: () => {
							GAME_MANAGER.instance.IgnoreUser(username);
							menu.style.pointerEvents = "none";
						}
					},
					{
						label: "Report",
						onclick: () => window.open(`/report/${username}`)
					},
				]);

				characters.appendChild(li);
			}

			for (let prop in ignoredUsers) {
				const li = document.createElement("li");
				const username = prop;
				li.className = "ignored";

				span = document.createElement("span");
				span.innerHTML = prop;
				span.style.marginLeft = "0";

				if (ignoredUsers[prop].username_color) {
					span.style.color = ignoredUsers[prop].username_color;
				}

				li.appendChild(span);

				li.onclick = (e) => DROPDOWN.instance.Open(e, [
					{
						label: "Remove",
						onclick: () => {
							GAME_MANAGER.instance.UnignoreUser(username);
							menu.style.pointerEvents = "none";
						}
					},
				]);

				characters.appendChild(li);
			}

			if (menu) {
				_characterFilter.Update();
				if (prevScrolTop) {
					characters.scrollTop = prevScrolTop;
				}
				return;
			}

			const tabs = ["Friends", "Recent", "Ignored", "Roleplay"];

			menu = document.createElement("div");
			menu.id = "menu_friends";

			const ul = document.createElement("ul");
			ul.className = "tabs";

			for (let i = 0; i < tabs.length; i++) {
				const li = document.createElement("li");

				span = document.createElement("span");
				span.textContent = tabs[i];

				li.className = tabs[i].toLowerCase();
				li.appendChild(span);
				li.onclick = () => {
					document.getElementById("add_friend").value = "";
					_characterFilter.Clear();

					menu.className = li.className + " right";

					switch (li.className) {
						case "friends":
							input.value = "Add Friend";
							break;
						case "ignored":
							input.value = "Ignore";
							break;
						case "roleplay":
							MENU.Social.ShowRoleplayScenarios();
							break;
					}
				};

				if (i == 0) {
					menu.className = li.className;
				}

				ul.appendChild(li);
			}

			menu.appendChild(ul);

			const searchRoleplay = document.createElement("div");
			searchRoleplay.className = "search_roleplay";

			const filter = createForm();
			filter.appendChild(createInputField("filter_roleplay", "Filter"));
			filter.maxLength = 120;
			filter.onsubmit = () => {
				if (filter.classList.contains("inactive")) {
					return false;
				}
				try {
					MENU.Social.ShowRoleplayScenarios();
				}
				finally {
					return false;
				}
			}

			_loadSymbol = document.createElement("div");
			_loadSymbol.className = "load_symbol";
			_loadSymbol.innerHTML = document.getElementById("load_symbol").innerHTML;
			filter.appendChild(_loadSymbol);

			searchRoleplay.appendChild(filter);

			const scenarioCreation = document.createElement("div");
			scenarioCreation.className = "scenario_creation";

			let form = createForm();

			div = document.createElement("div");
			div.innerHTML = "Title";
			form.appendChild(div);

			_titleInput = createInputField("scenario_title");
			_titleInput.maxLength = 120;
			form.appendChild(_titleInput);

			div = document.createElement("div");
			div.innerHTML = "Details";

			const detailsCharacterCount = document.createElement("span");
			div.appendChild(detailsCharacterCount);
			form.appendChild(div);

			_details = document.createElement("div");
			_details.className = "editable item_list details";
			_details.contentEditable = true;
			form.appendChild(_details);

			_details.oninput = () => {
				const text = contentToText(_details).trim();
				detailsCharacterCount.innerHTML = `${text.length}/${MAX_SCENARION_DETAILS_LENGTH}`;
				if (text.length > MAX_SCENARION_DETAILS_LENGTH) {
					detailsCharacterCount.classList.add("error");
				}
				else {
					detailsCharacterCount.classList.remove("error");
				}
				_titleInput.oninput();
			};

			_details.onpaste = (e) => {
				e.preventDefault();
				const contentHolder = document.createElement("div");
				contentHolder.innerHTML = (e.clipboardData || window.clipboardData).getData('text');
				document.execCommand('inserttext', false, contentHolder.textContent);
				_details.oninput();
			};

			div = document.createElement("div");
			div.className = "close submit";
			div.onclick = () => {
				GAME_MANAGER.instance.Send({ action: "Social", roleplayScenario: false });
				MENU.Social.ShowRoleplayScenarios();
			}
			form.appendChild(div);

			const setScenario = document.createElement("div");
			setScenario.className = "submit";
			setScenario.onclick = () => {
				GAME_MANAGER.instance.Send({
					action: "Social",
					roleplayScenario: {
						title: _titleInput.value,
						details: contentToText(_details).trim(),
					}
				});
				menu.style.pointerEvents = "";
			};
			form.appendChild(setScenario);

			scenarioCreation.appendChild(form);

			_titleInput.oninput = () => {
				if (_titleInput.value.trim().length == 0 || contentToText(_details).trim().length > MAX_SCENARION_DETAILS_LENGTH) {
					setScenario.classList.add("inactive");
				}
				else {
					setScenario.classList.remove("inactive");
				}
			};

			menu.appendChild(searchRoleplay);
			menu.appendChild(scenarioCreation);
			menu.appendChild(characters);

			form = createForm();
			form.onsubmit = () => {
				let value = document.getElementById("add_friend").value.trim();

				if (value.length == 0) {
					return false;
				}

				try {
					if (menu.classList.contains("friends")) {
						GAME_MANAGER.instance.AddFriend(value);
					}
					else if (menu.classList.contains("ignored")) {
						GAME_MANAGER.instance.IgnoreUser(value);
					}
					else {
						return false;
					}

					menu.style.pointerEvents = "none";
				}
				catch (e) {
					menu.style.pointerEvents = "";
				}

				return false;
			};

			form.appendChild(createInputField("add_friend", "Enter a username"));

			const input = document.createElement("input");
			div = document.createElement("div");
			div.className = "submit";
			div.appendChild(input);

			input.type = "submit";
			input.value = "Add Friend";

			_characterFilter = new FILTER.Filter(characters, "friends_filter", "Search characters");

			form.appendChild(div);
			form.appendChild(_characterFilter.input);

			_roleplayScenarios = document.createElement("table");
			div = document.createElement("div");
			div.className = "roleplay_scenarios item_list";
			div.appendChild(_roleplayScenarios);
			menu.appendChild(div);

			div = document.createElement("div");
			div.className = "create_scenario submit";
			div.onclick = () => MENU.Social.OpenScenarioCreation();

			menu.appendChild(form);
			menu.appendChild(div);

			displayMenu(menu, "right");
		},
		Close: () => {
			if (MENU.Social.menu) {
				MENU.CloseRightMenu();
				return true;
			}
			return false;
		},
		OpenIgnored: () => {
			if (!MENU.Social.menu) {
				MENU.Social.Open();
			}
			MENU.Social.menu.getElementsByClassName("tabs")[0].getElementsByClassName("ignored")[0].onclick();
		},
		OpenScenarioCreation: () => {
			if (!MENU.Social.menu) {
				MENU.Social.Open();
			}
			const roleplayScenario = GAME_MANAGER.instance.roleplayScenario;
			_titleInput.value = roleplayScenario.title;
			_titleInput.oninput();
			const text = formatEditableContent(roleplayScenario.details);
			if (text) {
				_details.innerHTML = text;
			}
			else {
				clearHTML(_details);
			}
			_details.oninput();
			MENU.Social.menu.className = "scenario_creation right";
		},
		ShowRoleplayScenarios: async () => {
			const filter = document.getElementById("filter_roleplay");
			const form = filter.parentNode;
			if (form.classList.contains("inactive")) {
				return;
			}
			_detailsByUsername = {};
			clearHTML(_roleplayScenarios);
			MENU.Social.menu.className = "roleplay right";
			GAME_MANAGER.instance.Send({ action: "Social", getScenarios: true, filter: filter && filter.value, id: ++_searchId });
			form.classList.add("inactive");
			await waitForSeconds(1.5);
			form.classList.remove("inactive");
		},
		ShowRoleplayScenarioDetails: (username, title = null, details = null) => {
			if (details) {
				_detailsByUsername[username] = { details: details, title: title };
			}
			else if (_detailsByUsername[username] !== undefined) {
				details = _detailsByUsername[username].details;
				title = _detailsByUsername[username].title;
			}
			else {
				return GAME_MANAGER.instance.Send({ action: "Social", getScenarioDetails: true, username: username });
			}
			MENU.RoleplayDetails.Open(title, details);
		},
		AddRoleplayScenarios: (scenarios, id) => {
			if (id == _searchId) {
				for (let i = 0; i < scenarios.length; i++) {
					const tr = document.createElement("tr");
					const td = document.createElement("td");
					tr.className = `tier_${(scenarios[i].tier ? scenarios[i].tier : 0)}`;

					const title = document.createElement("div");
					const poster = document.createElement("div");
					const username = document.createElement("span");
					const characterNames = document.createElement("span");

					if (scenarios[i].color !== undefined) {
						username.style.color = scenarios[i].color;
						username.style.opacity = 1;
					}

					title.textContent = scenarios[i].title;
					username.textContent = scenarios[i].username;
					characterNames.textContent = scenarios[i].names;

					poster.appendChild(username);
					poster.appendChild(characterNames);

					td.appendChild(title);
					td.appendChild(poster);

					tr.onmouseenter = (e) => title.clientWidth < title.scrollWidth && TOOLTIP.instance.Show(e, title.textContent);
					tr.appendChild(td);

					_roleplayScenarios.appendChild(tr);

					const actions = [
						{
							label: "Inspect",
							onclick: () => MENU.Inspect.Open(GUI.Position.Left, null, username.textContent)
						},
						{
							label: "Profile",
							onclick: () => window.open(`/user/${username.textContent}`)
						},
						{
							label: "Request Invite",
							onclick: async () => GAME_MANAGER.instance.Invite(username.textContent, true)
						},
						{
							label: "Send Message",
							onclick: () => MENU.Messages.Open(username.textContent)
						}
					];

					if (scenarios[i].details) {
						actions.unshift({
							label: "View Details",
							onclick: () => MENU.Social.ShowRoleplayScenarioDetails(username.textContent)
						});
					}

					tr.onclick = (e) => DROPDOWN.instance.Open(e, actions);
				}
			}
			for (let tier = 0; tier <= 5; tier++) {
				const list = toArray(_roleplayScenarios.getElementsByClassName(`tier_${tier}`));
				shuffleArray(list);
				for (let i = 0; i < list.length; i++) {
					_roleplayScenarios.prepend(list[i]);

				}
			}
		},
		Refresh: () => MENU.Social.menu && MENU.Social.Open(),
		Toggle: () => !MENU.Social.Close() && MENU.Social.Open(),
	};

	Object.defineProperties(MENU.Social, {
		'menu': {
			get: () => document.getElementById("menu_friends")
		},
	});

	MENU.RoleplayDetails = {
		Open: (title, details) => {
			const menu = document.createElement("div");
			menu.id = "menu_roleplay_details";

			const titleDiv = document.createElement("div");
			titleDiv.className = "title";
			titleDiv.textContent = title;
			titleDiv.onmouseenter = (e) => titleDiv.clientWidth < titleDiv.scrollWidth && TOOLTIP.instance.Show(e, titleDiv.textContent);

			const detailsDiv = document.createElement("div");
			detailsDiv.className = "item_list";
			detailsDiv.innerHTML = escapeHTMLOperators(reduceLineBreaks(details));

			menu.appendChild(titleDiv);
			menu.appendChild(detailsDiv);

			displayMenu(menu, "left");
		},
		Close: () => {
			if (MENU.RoleplayDetails.menu) {
				MENU.CloseLeftMenu();
				return true;
			}
			return false;
		},
	};

	Object.defineProperties(MENU.RoleplayDetails, {
		'menu': {
			get: () => document.getElementById("menu_roleplay_details")
		},
	});

	function formatEditableContent(input) {
		if (typeof input === "string") {
			const lines = escapeHTMLOperators(input).split("\n");
			if (lines.length > 0) {
				let output = lines.shift();
				for (let i = 0; i < lines.length; i++) {
					output += lines[i].length > 0 ? `<div>${lines[i]}</div>` : "<div><br/></div>";
				}
				return output;
			}
		}
		return null;
	}

	function getCharacterNames(obj) {
		let names = (obj.first_name ? obj.first_name : "") + " " + (obj.last_name ? obj.last_name : "");
		return names.trim();
	}
})();
