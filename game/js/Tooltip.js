(() => {
	TOOLTIP = {
		Token: {
			Action: 0,
			Spell: 1,
			Card: 2,
		},
		Stat: {
			Strength: 3,
			Dexterity: 4,
			Intelligence: 5,
			Willpower: 6,
		},
		Spell: {
			Expand: 7,
			Collapse: 8,
		},
		Quality: {
			OpenTab: 9,
		},
		Tag: {
			Absorption: 10,
			Animal: 11,
			Anthro: 12,
			Inanimate: 13,
			Monster: 14,
			Sissification: 15,
			Transgender: 16,
		},
	};

	let _instance;
	Object.defineProperty(TOOLTIP, "instance", {
		get: () => {
			if (!_instance) {
				_instance = new TOOLTIP.Tooltip();
			}
			return _instance;
		}
	});
})();

TOOLTIP.GetContent = (tooltip) => {
	switch (tooltip) {
		case TOOLTIP.Token.Action:
			return "Used for performing actions and casting spells<br/>Recharges each turn";
		case TOOLTIP.Token.Spell:
			return "Used for casting spells<br/>Replenishes after spending a turn in Your Room";
		case TOOLTIP.Token.Card:
			return "Fortune cards collected on this character<br/>Used for customizing your next character<br/>Not implemented yet";
		case TOOLTIP.Stat.Strength:
			return "Strength is a measurement of your physical constitution and is the basis for the number of actions you can perform each turn. For every one point you gain 50 Max Body and for every third point you gain 1 Action.";
		case TOOLTIP.Stat.Dexterity:
			return "Dexterity is a measurement of your agility and physical prowess. Having more dexterity improves your ability to land and avoid hits and powerful attacks. Damage dealt is increased by your Hit - opponent's Evasion &times; 10% and damage taken is reduced by your Evasion - opponent's Hit &times; 10%.";
		case TOOLTIP.Stat.Intelligence:
			return "Intelligence is a measurement of your mental constitution and is the basis for the number of spells you can cast between rests. For every one point you gain 50 Max Mind and and for every third point you gain 1 Spell action. You have a chance to gain a second wind equal to your remaining fractions of a Spell action when you spend your last Spell action once between every rest, replenishing one Spell action.";
		case TOOLTIP.Stat.Willpower:
			return "Willpower is a measurement of your strength of character and mental prowess. Having more willpower makes it easier for you to force your will on others while resisting theirs in turn. Damage dealt is increased by your Penetration - opponent's Resistance &times; 10% and damage taken is reduced by your Resistance - opponent's Penetration &times; 10%.";
		case TOOLTIP.Spell.Expand:
			return "Click to show spell variants";
		case TOOLTIP.Spell.Collapse:
			return "Click to hide spell variants";
		case TOOLTIP.Quality.OpenTab:
			return "Click to show all qualities";
		case TOOLTIP.Tag.Absorption:
			return "Enables content where people are absorbed into someone else's body.<br/>This tag is required for the possibility of becoming someone else's breasts, vagina, or penis, among other things.";
		case TOOLTIP.Tag.Animal:
			return "Enables content where people turn into animals.<br/>This tag is required for the possibility of turning into an animal and gaining particularly animalistic qualities, such as becoming quadruped.";
		case TOOLTIP.Tag.Anthro:
			return "Enables content where people become anthropomorphic.<br/>This tag is required for the possibility of faces to become animal-like.";
		case TOOLTIP.Tag.Inanimate:
			return "Enables content where people turn into inanimate objects.<br/>This tag is required for the possibility of turning into clothes, furniture, sex toys, and other items.";
		case TOOLTIP.Tag.Monster:
			return "Enables content where people become monster girls or boys.<br/>This tag enables some animal qualities and is required for the possibility of gaining most qualities associated with mythical and otherworldly creatures.";
		case TOOLTIP.Tag.Sissification:
			return "Enables content where men are made to look womanly.<br/>This tag is required for male characters to grow breasts.";
		case TOOLTIP.Tag.Transgender:
			return "Enables content where men turn into women and vice versa.<br/>When paired with sissification, gender transformations will usually happen more gradually.";
		default:
			return null;
	}
};

TOOLTIP.Tooltip = function () {
	const _tooltip = document.getElementById("tooltip");
	let _touchCount;
	let _trigger;
	let _event;

	Object.defineProperties(this, {
		'trigger': {
			get: () => _trigger
		},
		'event': {
			get: () => _event
		},
	});

	this.ShowStatus = (e, params) => {
		const div = document.createElement("div");
		let subdiv = document.createElement("div");
		subdiv.className = "body";
		subdiv.innerHTML = `Body: ${Math.floor(params.body)}/${Math.floor(params.maxBody)}`;
		div.appendChild(subdiv);

		subdiv = document.createElement("div");
		subdiv.className = "mind";
		subdiv.innerHTML = `Mind: ${Math.floor(params.mind)}/${Math.floor(params.maxMind)}`;
		div.appendChild(subdiv);

		if (params.roleplaying) {
			subdiv = document.createElement("div");
			subdiv.className = "roleplaying";
			div.appendChild(subdiv);
		}

		subdiv = document.createElement("div");
		subdiv.className = "hint";
		subdiv.innerHTML = "Click to inspect";
		div.appendChild(subdiv);

		return this.Show(e, div);
	}

	this.ShowSpell = (e, spellDetails) => {
		let subdiv;

		const div = document.createElement("div");
		div.className = "spell_details";

		const spellName = document.createElement("div");
		spellName.innerHTML = spellDetails.spell_name;
		div.appendChild(spellName);

		if (spellDetails.tags !== undefined) {
			subdiv = document.createElement("div");
			subdiv.innerHTML = spellDetails.tags.join(", ");
			div.appendChild(subdiv);

			if (spellDetails.tags.includes("Body")) {
				div.classList.add("body");
			}

			if (spellDetails.tags.includes("Mind")) {
				div.classList.add("mind");
			}
		}

		subdiv = document.createElement("div");
		subdiv.innerHTML = spellDetails.description !== undefined ? spellDetails.description : spellDetails.short_desc;
		div.appendChild(subdiv);

		if (spellDetails.menu_items !== undefined) {
			const ul = document.createElement("ul");

			for (let i = 0; i < spellDetails.menu_items.length; i++) {
				const li = document.createElement("li");
				li.innerHTML = spellDetails.menu_items[i];
				ul.appendChild(li);
			}

			div.appendChild(ul);
		}

		if (spellDetails.materials !== undefined) {
			subdiv = document.createElement("div");
			subdiv.className = "materials";

			const materials = [];
			for (let prop in spellDetails.materials) {
				materials.push(spellDetails.materials[prop] > 1 ? `${spellDetails.materials[prop]}x ${prop}` : prop);
			}

			subdiv.innerHTML = `Material${materials.length > 1 ? "s" : ""}: ${materials.join(", ")}`;

			if (!GAME_MANAGER.instance.HasMeterials(spellDetails.materials)) {
				subdiv.classList.add("missing");
			}

			div.appendChild(subdiv);
		}

		return this.Show(e, div);
	}

	this.ShowSkill = (e, skillDetails) => {
		const skillName = document.createElement("div");
		const description = document.createElement("div");
		const menuItems = document.createElement("table");
		const div = document.createElement("div");

		skillName.innerHTML = skillDetails.skill_name;
		description.innerHTML = skillDetails.description;

		for (let prop in skillDetails.menu_items) {
			let tr = document.createElement("tr");
			let td = document.createElement("td");
			td.innerHTML = prop;
			tr.appendChild(td);

			td = document.createElement("td");
			td.innerHTML = "&nbsp:&nbsp";
			tr.appendChild(td);

			td = document.createElement("td");
			td.innerHTML = skillDetails.menu_items[prop];
			tr.appendChild(td);

			menuItems.appendChild(tr);
		}

		div.className = "skill_details";

		div.appendChild(skillName);
		div.appendChild(description);
		div.appendChild(menuItems);

		return this.Show(e, div);
	}

	this.ShowItem = (e, itemDetails, tier) => {
		let prefix;
		let description;

		const wrapper = document.createElement("div");
		wrapper.className = "item_tooltip";

		if (SETTINGS.Get("item_image_tooltip", true)) {
			let imageDiv = document.createElement("div");

			imageDiv.style.backgroundImage = `url(${IMAGE_PROCESSING.getWornItemImage(itemDetails, tier)})`;
			imageDiv.style.backgroundSize = itemDetails.base.worn_image_url ? "contain" : null;
			imageDiv.className = "item_image " + (itemDetails.base.worn_image_url ? `item_2x3` : `item_${itemDetails.base.width}x${itemDetails.base.height}`);

			wrapper.appendChild(imageDiv);
		}

		const div = document.createElement("div");
		div.className = "item_details";
		wrapper.appendChild(div);

		const itemName = document.createElement("div");
		itemName.className = "item_name";
		div.appendChild(itemName);

		if (itemDetails.variant_color && itemDetails.variant_color.length > 0) {
			let subdiv = document.createElement("div");
			subdiv.className = "color_variant";
			subdiv.innerHTML = itemDetails.variant_color;

			let color = nameToRef(itemDetails.variant_color);
			switch (color) {
				case "black":
				case "navy":
				case "blue":
				case "indigo":
				case "dark_blue":
					subdiv.classList.add("black");
					subdiv.style.color = color.replace("_", "");
					break;
				case "vermilion":
				case "metallic":
				case "silver":
				case "gold":
				case "cream":
				case "invisible":
				case "sapphire":
				case "mustard":
				case "sunset":
				case "carrot":
				case "rust":
				case "amaranth":
				case "indian_red":
				case "amethyst":
				case "cow_pattern":
					subdiv.classList.add(color);
					break;
				default:
					subdiv.style.color = color.replace("_", "");
					break;
			}

			div.appendChild(subdiv);
		}

		if (itemDetails.base.stack && itemDetails.base.stack > 1) {
			let subdiv = document.createElement("div");
			subdiv.className = "stack";
			subdiv.innerHTML = itemDetails.base.stack;
			div.appendChild(subdiv);
		}

		let ul = document.createElement("ul");

		if (itemDetails.base.menu_items && itemDetails.base.menu_items.length > 0) {
			for (let i = 0; i < itemDetails.base.menu_items.length; i++) {
				let li = document.createElement("li");
				li.innerHTML = itemDetails.base.menu_items[i];
				ul.appendChild(li);
			}

			div.appendChild(ul);
		}

		let itemTier = Number.MAX_VALUE;

		if (itemDetails.conjured_type == ConjuredType.UnidentifiedHex) {
			const random = new Random(itemDetails.id);
			const glyphs = "AEGNOcdeghkinxus";
			for (let i = 0; i < 2; i++) {
				const li = document.createElement("li");
				let g = glyphs;
				let str = "";
				do {
					const c = g[Math.floor(random.NextNumber() * g.length)];
					str += c;
					g = g.replace(c, "");
				}
				while (str.length < 8);
				li.innerHTML = str;
				li.className = "hex unknown";
				ul.appendChild(li);
			}
			div.appendChild(ul);
		}
		else {
			const enchantments = itemDetails.enchantments ? itemDetails.enchantments : GAME_MANAGER.instance.GetEnchantments(itemDetails.id);
			if (enchantments && enchantments.length > 0) {
				for (let i = 0; i < enchantments.length; i++) {
					if (enchantments[i].tier) {
						itemTier = Math.min(itemTier, enchantments[i].tier);
					}
					if (enchantments[i].mods) {
						for (let mod in enchantments[i].mods) {
							const li = document.createElement("li");
							if (enchantments[i].mods[mod] >= 0) {
								li.innerHTML = "+";
							}
							else {
								li.className = "hex";
							}
							switch (mod) {
								case "body":
								case "mind":
									li.innerHTML += `${enchantments[i].mods[mod]} ${firstToUpperCase(mod)}`;
									break;
								default:
									li.innerHTML += `${enchantments[i].mods[mod].toFixed(2)} ${firstToUpperCase(mod)}`;
									break;
							}
							ul.appendChild(li);
						}
						div.appendChild(ul);
					}
				}
			}
		}

		switch (itemDetails.conjured_type) {
			case ConjuredType.ConcealingHex:
			case ConjuredType.TemporaryHex:
			case ConjuredType.IdentifiedHex:
			case ConjuredType.UnidentifiedHex:
				prefix = "Hexed";
				break;
			default:
				switch (itemTier) {
					case 3:
						prefix = "Magic";
						break;
					case 2:
						prefix = "Occult";
						break;
					case 1:
						prefix = "Blessed";
						break;
				}
				break;
		}

		itemName.innerHTML = (prefix ? prefix + " " : "") + itemDetails.base.item_name;
		const accessoryReference = itemDetails.variant_accessory && itemDetails.variant_accessory.length > 0 ? nameToRef(itemDetails.variant_accessory) : null;

		if (itemDetails.description !== undefined) {
			description = itemDetails.description;
		}
		else if (accessoryReference) {
			switch (accessoryReference) {
				case "cross":
					description = "This item has a cross symbol";
					break;
				case "heart":
					description = "This item has a heart symbol";
					break;
				case "bat":
					description = "This item has a bat symbol";
					break;
				case "skull":
					description = "This item has a skull mark";
					break;
				case "yin_yang":
					description = "This item has a yin-yang symbol";
					break;
				case "triquetra":
					description = "This item has a triquetra symbol";
					break;
				case "pentacle":
					description = "This item has a pentacle mark";
					break;
				case "moon":
					description = "This item has a moon mark";
					break;
				case "sun":
					description = "This item has a sun mark";
					break;
				case "ouroboros":
					description = "This item has an ouroboros symbol";
					break;
				case "snake":
					description = "This item has a snake mark";
					break;
				case "ankh":
					description = "This item has a ankh symbol";
					break;
				case "heavy_chains":
					description = "This item has been decorated with heavy chains";
					break;
				case "fine_chains":
					description = "This item has been decorated with fine chains";
					break;
				case "cone_studs":
					description = "This item has been decorated with cone studs";
					break;
				case "pyramid_studs":
					description = "This item has been decorated with pyramid studs";
					break;
				case "star_studs":
					description = "This item has been decorated with star studs";
					break;
				case "spikes":
					description = "This item has been decorated with spikes";
					break;
				case "ribbons":
					description = "This item has been decorated with ribbons";
					break;
				case "key":
					description = "This item has a key attached";
					break;
				case "padlock":
					description = "This item has a padlock attached";
					break;
				case "bell":
					description = "This item has a bell attached";
					break;
				case "holly":
					description = "This item has been decorated with holly";
					break;
			}
		}

		if (description) {
			const subdiv = document.createElement("div");
			subdiv.className = "accessory_variant";
			subdiv.innerHTML = description;
			div.appendChild(subdiv);
		}

		const hints = itemDetails.base.hints !== undefined ? itemDetails.base.hints.slice() : [];

		if (!itemDetails.workorder) {
			if (itemDetails.base.actions !== undefined && itemDetails.base.actions.Use !== undefined) {
				switch (itemDetails.base.actions.Use.target) {
					case "item":
						hints.push("Use this on an item to apply its effects");
						break;
					case "equipment":
						hints.push("Use this on an equipment item to apply its effects");
						break;
				}
			}

			switch (itemDetails.base.type) {
				case "consumable":
					hints.push("Drag onto your character to use");
					break;
				case "material":
					hints.push("This item is used in spellcasting");
					break;
				case "equipment":
					if (!isDescendant(MENU.Inspect.menu, e.target) && !isDescendant(MENU.Myself.menu, e.target)) {
						hints.push("Drag onto character or equipment slot to equip");
					}
					break;
			}

			if (itemDetails.conjured_type === ConjuredType.Concealing) {
				hints.push("This item is concealing another item");
			}
			else if (itemDetails.conjured_type === ConjuredType.Temporary) {
				hints.push("This item will disappear when unequipped");
			}
		}

		for (let i = 0; i < hints.length; i++) {
			const subdiv = document.createElement("div");
			subdiv.className = "hint";
			subdiv.innerHTML = hints[i];
			div.appendChild(subdiv);
		}

		return this.Show(e, wrapper, accessoryReference ? "accessory" : null, accessoryReference, SETTINGS.Get("tooltip_background_animated", true) ? "animate" : null);
	}

	this.ShowCraftingOption = (e, option, item = null) => {
		const sprite = option.worn_image_url || option.image_url;
		const div = document.createElement("div");

		const wrapper = document.createElement("div");
		wrapper.className = "crafting_option_details";
		wrapper.appendChild(div);

		if (sprite) {
			const imageDiv = document.createElement("div");

			imageDiv.style.backgroundImage = `url(${(item ? IMAGE_PROCESSING.getWornItemImage(item, 0, sprite) : sprite)})`;
			imageDiv.style.backgroundSize = option.worn_image_url ? "contain" : null;
			imageDiv.className = "item_image " + (option.worn_image_url ? "item_2x3" : `item_${option.width}x${option.height}`);

			wrapper.prepend(imageDiv);
		}

		if (option.description !== undefined) {
			const description = document.createElement("div");
			description.innerHTML = option.description;
			div.appendChild(description);
		}

		if (option.materials !== undefined) {
			const subdiv = document.createElement("div");
			subdiv.className = "materials";

			const materials = [];
			for (let prop in option.materials) {
				materials.push(option.materials[prop] > 1 ? `${option.materials[prop]}x ${prop}` : prop);
			}

			subdiv.innerHTML = `Material${materials.length > 1 ? "s" : ""}: ${materials.join(", ")}`;

			if (!GAME_MANAGER.instance.HasMeterials(option.materials)) {
				subdiv.classList.add("missing");
			}

			div.appendChild(subdiv);
		}

		if (option.known === true) {
			const subdiv = document.createElement("div");
			subdiv.innerHTML = "Already known";
			subdiv.className = "known";
			div.appendChild(subdiv);
		}

		if (sprite || wrapper.textContent.trim().length > 0) {
			return this.Show(e, wrapper);
		}
	}

	this.ShowQuality = (e, details, isSelf, image) => {
		const qualityName = document.createElement("div");
		const description = document.createElement("div");
		const icon = document.createElement("div");
		const div = document.createElement("div");
		const subdiv = document.createElement("div");

		if (image) {
			loadImage(details.image_url)
				.then(() => icon.style.backgroundImage = `url(${details.image_url})`)
				.catch(e => icon.style.backgroundImage = "url(assets/gui/default_icon.png)");
		}
		else {
			icon.style.backgroundImage = "url(assets/gui/default_icon.png)";
		}

		icon.className = "icon";
		qualityName.innerHTML = details.quality_name;
		description.innerHTML = isSelf ? details.description : details.inspect;

		const modifiers = document.createElement("ul");
		const items = [[], []];

		if (details.mods !== undefined) {
			for (let mod in details.mods) {
				let item = document.createElement("li");
				item.innerHTML = `${details.mods[mod] > 0 ? "+" : ""}${(Number.isInteger(details.mods[mod]) ? details.mods[mod] : details.mods[mod].toFixed(2))} ${firstToUpperCase(mod)}`;
				items[details.mods[mod] >= 0 ? 0 : 1].push(item);
			}
		}

		for (let i = 0; i < items.length; i++) {
			for (let l = 0; l < items[i].length; l++) {
				if (i == 1) {
					items[i][l].className = "debuff";
				}
				modifiers.appendChild(items[i][l]);
			}
		}

		const menuItems = document.createElement("ul");

		if (details.menu_items !== undefined) {
			for (let i = 0; i < details.menu_items.length; i++) {
				const item = document.createElement("li");
				item.innerHTML = details.menu_items[i];
				menuItems.appendChild(item);
			}
		}

		subdiv.appendChild(icon);
		subdiv.appendChild(qualityName);
		div.appendChild(subdiv);
		div.appendChild(modifiers);
		div.appendChild(menuItems);
		div.appendChild(description);

		if (details.hint !== undefined) {
			const hint = document.createElement("div");
			hint.className = "hint";
			hint.innerHTML = details.hint;
			div.appendChild(hint);
		}

		div.className = "quality_details";

		return this.Show(e, div);
	}

	this.ShowFilterTag = (e, tag, enabled) => {
		const div = document.createElement("div");
		div.className = "filter";

		let subdiv = document.createElement("div");
		subdiv.innerHTML = tag;

		if (enabled) {
			subdiv.className = "enabled";
		}

		div.appendChild(subdiv);

		subdiv = document.createElement("div");
		subdiv.innerHTML = removeBreakRows(TOOLTIP.GetContent(TOOLTIP.Tag[tag]));
		subdiv.className = "hint";
		div.appendChild(subdiv);

		return this.Show(e, div);
	}

	this.Show = (e, content, ...classNames) => {
		if (!content || CURSOR.instance.heldItem || UNSTACK.instance.IsActive()) {
			return;
		}

		if (!_trigger || _trigger != e.target) {
			_touchCount = 0;
		}

		this.Hide();

		clearHTML(_tooltip);

		for (let i = 0; i < classNames.length; i++) {
			if (classNames[i] && classNames[i].toString().length > 0) {
				_tooltip.classList.add(classNames[i]);
			}
		}

		if (typeof content === "string") {
			let div = document.createElement("div");
			div.innerHTML = content;
			_tooltip.appendChild(div);
			content = div;
		}
		else {
			_tooltip.appendChild(content);

			if (content.classList.contains("spell_details") || content.classList.contains("quality_details")) {
				let list = content.getElementsByTagName("div");
				for (let i = 0; i < list.length; i++) {
					let fontSize = window.getComputedStyle(list[i]).fontSize;
					if (list[i].clientHeight < fontSize.substring(0, fontSize.indexOf("px")) * 2) {
						list[i].style.textAlign = "center";
					}
				}
			}
		}

		_event = e;

		if (_trigger) {
			_trigger.removeEventListener("click", this.Hide);
			_trigger.removeEventListener("mouseleave", this.Hide);
			_trigger.removeEventListener("touchend", cancelClick);
		}

		_trigger = e.target;
		_trigger.addEventListener("click", this.Hide);
		_trigger.addEventListener("mouseleave", this.Hide);
		_trigger.addEventListener("touchend", cancelClick);

		moveToNext(_tooltip, _trigger);

		_tooltip.classList.remove("inactive");

		return content;
	};

	const cancelClick = (e) => _touchCount++ == 0 && e.preventDefault();

	const moveToNext = (tooltip, element) => {
		const containerRect = container.getBoundingClientRect();
		const tooltipRect = tooltip.getBoundingClientRect();
		const elementRect = element.getBoundingClientRect();

		if (elementRect.left > (containerRect.left + containerRect.right) / 2) {
			tooltip.style.left = `${elementRect.left - tooltipRect.width - containerRect.left}px`;
		}
		else {
			tooltip.style.left = `${elementRect.right - containerRect.left}px`;
		}

		let triggerVerticalCenter = elementRect.top + elementRect.height / 2 - containerRect.top;
		if (triggerVerticalCenter - tooltipRect.height / 2 < 0) {
			tooltip.style.top = "0";
		}
		else if (triggerVerticalCenter + tooltipRect.height / 2 > containerRect.height) {
			tooltip.style.top = `${containerRect.height - tooltipRect.height}px`;
		}
		else {
			tooltip.style.top = `${triggerVerticalCenter - tooltipRect.height / 2}px`;
		}
	}

	this.Hide = (e) => _tooltip.className = "inactive";

	container.addEventListener("click", () => !_tooltip.classList.contains("inactive") && this.Hide());
}