(() => {
	const POSTED_MESSAGES_KEY = "local_chat:posted_messages";
	const EPSILON = 5;

	window.addEventListener("unload", () => _player && _player.SaveMessages());

	LOCAL_CHAT = {
		MAX_POSTED_MESSAGES: 20,
		TYPING_MIN_INTERVAL_SECONDS: 10,
		TYPING_INDICATOR_DURATION_SECONDS: 60,
		Channel: {
			Say: 0,
			OOC: 1,
			Emote: 2,
			Combat: 3,
		},
		Clear: () => {
			clearInterval(IsTyping.pingPause);
			IsTyping.pingPause = null;
			IsTyping.pendingPing = false;
			LOCAL_CHAT.Close(GUI.Position.Left);
			LOCAL_CHAT.Close(GUI.Position.Right);
			LOCAL_CHAT.SetTypingIndicator(false, GUI.Position.Left);
			LOCAL_CHAT.SetTypingIndicator(false, GUI.Position.Right);
			_player = null;
			_opponent = null;
		},
		Resize: () => _player && _player.Resize(),
		Close: async (position) => {
			if (position === GUI.Position.Left && _player || position === GUI.Position.Right && _opponent) {
				if (position === GUI.Position.Left) {
					if (_player.input) {
						_player.input.blur();
					}
					let select = _player.element && _player.element.getElementsByTagName("select")[0];
					if (select) {
						select.blur();
					}
				}
				return GUI.instance.HideDialog(position);
			}
		},
		Resume: (position) => position === GUI.Position.Left ? _player && _player.count > 0 && _player.Show() : _opponent && _opponent.count > 0 && _opponent.Show(),
		SetTypingIndicator: async (isTyping, position) => {
			let parent = document.getElementById("dialog");
			let indicators = parent.getElementsByClassName("is_typing");
			let indicator = null;
			let elm = position === GUI.Position.Right ? _opponent && _opponent.element : (_player && _player.element);

			if (!elm || elm.classList.contains("inactive")) {
				isTyping = false;
			}

			const alignment = position === GUI.Position.Right ? "right" : "left";
			for (let i = 0; i < indicators.length && !indicator; i++) {
				if (indicators[i].classList.contains(alignment)) {
					indicator = indicators[i];
				}
			}

			if (isTyping) {
				if (!indicator) {
					const span = document.createElement("span");
					indicator = document.createElement("div");
					indicator.className = `is_typing inactive ${alignment}`;
					indicator.appendChild(span);
				}

				if (IsTyping.indicatorTimeouts[position]) {
					clearTimeout(IsTyping.indicatorTimeouts[position]);
				}
				IsTyping.indicatorTimeouts[position] = setTimeout(() => {
					indicator.classList.add("inactive");
					IsTyping.indicatorTimeouts[position] = null;
				}, LOCAL_CHAT.TYPING_INDICATOR_DURATION_SECONDS * 1000);

				if (!indicator.parentNode) {
					parent.appendChild(indicator);
					await waitForFrames();
					if (!IsTyping.indicatorTimeouts[position]) {
						return;
					}
				}
				indicator.classList.remove("inactive");
			}
			else {
				indicator && indicator.classList.add("inactive");
				if (IsTyping.indicatorTimeouts[position]) {
					clearTimeout(IsTyping.indicatorTimeouts[position]);
					IsTyping.indicatorTimeouts[position] = null;
				}
			}
		},
		Update: (time) => {
			_dots = Math.ceil(time / 500 % 3);
			if (_dots !== _prevDots) {
				let indicators = document.getElementById("dialog").getElementsByClassName("is_typing");
				for (let i = 0; i < indicators.length; i++) {
					indicators[i].getElementsByTagName("span")[0].style.width = `${0.9 / 3 * _dots - 0.05}em`;
				}
			}
			_prevDots = _dots;
		}
	};

	let _dots = 0;
	let _prevDots;

	let _player;
	let _opponent;

	const IsTyping = {
		pingPause: null,
		pendingPing: null,
		indicatorTimeouts: []
	};

	const _findOperator = /(\*|\()/;
	const _findChannel = /^\/(s|say|o|ooc|e|em|me|emote)\b/;
	const _findCommand = /^\/\S+\b/;

	Object.defineProperties(LOCAL_CHAT, {
		'player': {
			get: () => {
				if (!_player) {
					_player = new LOCAL_CHAT.LocalChat(document.getElementById("dialog").getElementsByTagName("li")[1]);
				}
				return _player;
			}
		},
		'opponent': {
			get: () => {
				if (!_opponent) {
					_opponent = new LOCAL_CHAT.LocalChat(document.getElementById("dialog").getElementsByTagName("li")[2]);
				}
				return _opponent;
			}
		}
	});

	LOCAL_CHAT.LocalChat = function (_elm) {
		const _textField = _elm.getElementsByClassName("text_field")[0];
		const _inputField = _elm.getElementsByClassName("editable")[0];
		const _scroller = _textField.parentNode;
		const _list = [];

		let _showing = false;
		let _scrolledDown = false;

		let _postedMessages;
		let _channel;

		Object.defineProperties(this, {
			'count': {
				get: () => _list.length
			},
			'input': {
				get: () => _inputField
			},
			'element': {
				get: () => _elm
			},
		});

		this.Append = (channel, message, nature) => {
			const div = document.createElement("div");
			const timestampSpan = document.createElement("span");
			const messageSpan = document.createElement("span");
			const date = new Date();

			timestampSpan.textContent = date.toLocaleTimeString([], { hour12: false }).replace(/\./g, ":");
			timestampSpan.className = "timestamp";

			if (Array.isArray(message)) {
				for (let i = 0; i < message.length; i++) {
					const segment = document.createElement("span");
					segment.textContent = message[i].text;
					switch (message[i].channel) {
						case LOCAL_CHAT.Channel.Emote:
							segment.className = `emote ${nature}`;
							break;
						case LOCAL_CHAT.Channel.OOC:
							segment.className = "ooc";
							break;
					}
					messageSpan.appendChild(segment);
				}
				switch (message[0].channel) {
					case LOCAL_CHAT.Channel.OOC:
						timestampSpan.classList.add("ooc");
						break;
					case LOCAL_CHAT.Channel.Emote:
						timestampSpan.classList.add("emote");
						timestampSpan.classList.add(nature);
						break;
				}
			}
			else {
				messageSpan.textContent = message;
				switch (channel) {
					case LOCAL_CHAT.Channel.Say:
						break;
					case LOCAL_CHAT.Channel.OOC:
						messageSpan.className = "ooc";
						timestampSpan.classList.add("ooc");
						break;
					case LOCAL_CHAT.Channel.Emote:
						messageSpan.className = "emote";
						div.className = `emote ${nature}`;
						break;
					case LOCAL_CHAT.Channel.Combat:
						div.className = nature;
						break;
				}
			}

			div.appendChild(timestampSpan);
			div.appendChild(messageSpan);

			_list.push(div);

			if (_elm.classList.contains("chat")) {
				_textField.appendChild(div);
				_scrolledDown && scrollToBottom();
			}
			else {
				this.Show();
			}
		}

		const scrollToBottom = () => {
			if (_scroller.offsetHeight < _scroller.scrollHeight) {
				_scroller.scrollTop = _scroller.scrollHeight - _scroller.offsetHeight;
			}
		}

		this.Resize = () => {
			if (_inputField) {
				_textField.style.paddingBottom = `calc(${_inputField.offsetHeight}px + 0.3667em)`;
				_scrolledDown && scrollToBottom();
			}
		}

		this.SaveMessages = () => {
			if (_postedMessages && GAME_MANAGER.instance.remembered) {
				_postedMessages[0] = { message: _inputField.textContent, channel: _channel.value };
				localStorage.setItem(POSTED_MESSAGES_KEY, JSON.stringify(_postedMessages));
			}
		};

		this.Show = async () => {
			if (_showing || _elm.classList.contains("chat") && !_elm.classList.contains("inactive")) {
				return;
			}

			_showing = true;

			await GUI.instance.HideDialog(-1);

			const dialogList = document.getElementById("dialog").getElementsByTagName("li");
			const position = dialogList[1] == _elm ? GUI.Position.Left : GUI.Position.Right;
			if (!dialogList[position + 1].classList.contains("chat")) {
				await GUI.instance.HideDialog(position);
			}

			clearHTML(_textField);
			if (_list.length > 0) {
				for (let i = 0; i < _list.length; i++) {
					_textField.appendChild(_list[i]);
				}
				scrollToBottom();
			}

			_elm.classList.add("chat");
			_showing = false;

			if (_elm.classList.contains("inactive")) {
				_elm.classList.remove("inactive");
				await transitionEnded(_elm);
			}

			this.Resize();
			update();
		};

		const update = async () => {
			while (_elm.classList.contains("chat")) {
				_scrolledDown = _scroller.offsetHeight >= _scroller.scrollHeight || _scroller.scrollTop >= _scroller.scrollHeight - _scroller.offsetHeight - EPSILON;
				await waitForFrame();
			}
		}

		if (_inputField) {
			const _form = document.getElementById("local_chat");

			let _postedMessageIndex = 0;
			let _inputEnabled = true;
			let _prevSegments = [];

			_channel = document.getElementById("local_chat_channel");
			_channel.onchange = () => {
				_form.classList.remove("ooc");
				_form.classList.remove("emote");
				switch (_channel.value) {
					case "ooc":
					case "emote":
						_form.classList.add(_channel.value);
						break;
				}
			};

			_form.classList.add(LOCATION.instance.player.nature);
			_elm.onclick = () => _inputField.focus();

			try {
				if (GAME_MANAGER.instance.remembered) {
					_postedMessages = JSON.parse(localStorage.getItem(POSTED_MESSAGES_KEY));
				}
			}
			finally {
				if (!_postedMessages || !Array.isArray(_postedMessages) || _postedMessages.length == 0) {
					_postedMessages = [{ message: "", channel: "say" }];
				}
				if (_postedMessages[0].message && _postedMessages[0].message.trim().length > 0) {
					_inputField.innerHTML = _postedMessages[0].message;
					_channel.value = _postedMessages[0].channel;
					_channel.onchange();
				}
			}

			const updateChannel = (text) => {
				let match = text.toLowerCase().match(_findChannel);
				if (match) {
					switch (match[0].substr(1)) {
						case "say":
						case "s":
							_channel.value = "say";
							break;
						case "ooc":
						case "o":
							_channel.value = "ooc";
							break;
						case "emote":
						case "em":
						case "me":
						case "e":
							_channel.value = "emote";
							break;
						default:
							return text;
					}
					text = text.substr(match[0].length).trim();
					_inputField.textContent = text;
					_channel.onchange();
				}
				return text;
			}

			_inputField.oninput = (e) => {
				let text = contentToText(_inputField);
				if (e && e.data == " ") {
					text = updateChannel(text);
				}
				updateSegments(text);
				_postedMessageIndex = 0;
				if (text.trim().length === 0) {
					if (IsTyping.pingPause) {
						clearInterval(IsTyping.pingPause);
						IsTyping.pingPause = null;
						IsTyping.pendingPing = false;
					}
					GAME_MANAGER.instance.Send({ action: "LocalChat", isTyping: false });
					LOCAL_CHAT.SetTypingIndicator(false, GUI.Position.Left);
				}
				else if (IsTyping.pingPause) {
					IsTyping.pendingPing = true;
				}
				else {
					IsTyping.pingPause = setTimeout(() => {
						IsTyping.pingPause = null;
						if (IsTyping.pendingPing) {
							IsTyping.pendingPing = null;
							GAME_MANAGER.instance.Send({ action: "LocalChat", isTyping: true });
						}
					}, LOCAL_CHAT.TYPING_MIN_INTERVAL_SECONDS * 1000);
					GAME_MANAGER.instance.Send({ action: "LocalChat", isTyping: true });
					if (LOCATION.instance.opponent) {
						LOCAL_CHAT.SetTypingIndicator(true, GUI.Position.Left);
					}
				}
				if (_inputField.firstChild && _inputField.firstChild.tagName && _inputField.firstChild.tagName.toLowerCase() == "font") {
					const font = _inputField.firstChild;
					_inputField.removeChild(font);
					_inputField.innerHTML = font.textContent + _inputField.innerHTML;
					setCaretPosition(_inputField, _inputField.textContent.length);
				}
			};

			_inputField.onkeydown = async (e) => {
				switch (e.key) {
					case "Enter":
						e.preventDefault();
						if (_inputField.textContent.length == 0) {
							await waitForFrame();
							GUI.instance.HideDialog(GUI.Position.Left);
							_inputField.blur();
						}
						else {
							submit();
						}
						break;
					case "Tab":
						e.preventDefault();
						switch (_channel.value) {
							case "say":
							default:
								_channel.value = "ooc";
								break;
							case "ooc":
								_channel.value = "emote";
								break;
							case "emote":
								_channel.value = "say";
								break;
						}
						_channel.onchange();
						break;
					case "ArrowUp":
					case "ArrowDown":
						if (e.ctrlKey && _postedMessages.length > 1) {
							e.preventDefault();
							if (_postedMessageIndex == 0) {
								_postedMessages[0] = { message: _inputField.textContent, channel: _channel.value };
							}
							_postedMessageIndex = modulus(_postedMessageIndex + (e.key == "ArrowDown" ? 1 : -1), _postedMessages.length);
							_inputField.textContent = _postedMessages[_postedMessageIndex].message;
							setCaretPosition(_inputField, _inputField.textContent.length);
							updateSegments(contentToText(_inputField));
							if (_channel.value != _postedMessages[_postedMessageIndex].channel) {
								_channel.value = _postedMessages[_postedMessageIndex].channel;
								_channel.onchange();
							}
						}
						break;
				}
			};

			_inputField.onfocus = async () => {
				_inputEnabled = false;
				await waitForFrame();
				_inputEnabled = true;
			};

			_inputField.onpaste = (e) => {
				e.preventDefault();
				const contentHolder = document.createElement("div");
				contentHolder.innerHTML = escapeHTMLOperators(collapseLineBreaks((e.clipboardData || window.clipboardData).getData('text')));
				document.execCommand('inserttext', false, contentHolder.textContent);
				_inputField.oninput();
			};

			const submit = () => {
				if (!_inputEnabled) {
					return false;
				}
				try {
					let command;
					let text = contentToText(_inputField).trim();

					if (text.length > 0) {
						_postedMessages.push({ message: text, channel: _channel.value });

						if (_postedMessages.length > LOCAL_CHAT.MAX_POSTED_MESSAGES) {
							_postedMessages.shift();
						}

						_postedMessages[0] = "";
						_inputField.innerHTML = "";
						_postedMessageIndex = 0;
					}

					if (text.startsWith("/")) {
						text = updateChannel(text);
						command = findCommand(text.trim());
					}

					if (text.length > 0) {
						if (command) {
							GAME_MANAGER.instance.Send([command, text.substr(text.indexOf(command) + command.length).trim()]);
						}
						else {
							let channel;
							switch (_channel.value) {
								case "emote":
									channel = LOCAL_CHAT.Channel.Emote;
									break;
								case "ooc":
									channel = LOCAL_CHAT.Channel.OOC;
									break;
								default:
									channel = LOCAL_CHAT.Channel.Say;
									break;
							}
							GAME_MANAGER.instance.Send({ action: "LocalChat", message: text, channel: channel });
						}

						this.SaveMessages();
					}

					_inputField.oninput();
				}
				catch (e) {
					console.error(e);
				}
				return false;
			};

			const updateSegments = (text) => {
				let segments = findSegments(text);

				if (lookForChanges(segments, _prevSegments)) {
					let caretPosition = getCaretPosition(_inputField);
					let html = "";

					for (let i = 0; i < segments.length; i++) {
						switch (segments[i].channel) {
							case LOCAL_CHAT.Channel.Emote:
								html += `<span class="emote">*${segments[i].text}*</span>`;
								break;
							case LOCAL_CHAT.Channel.OOC:
								html += `<span class="ooc">(${segments[i].text})</span>`;
								break;
							default:
								html += segments[i].text;
								break;
						}
					}

					_inputField.innerHTML = html;
					setCaretPosition(_inputField, caretPosition);

					let emoteList = _inputField.getElementsByClassName("emote");
					let oocList = _inputField.getElementsByClassName("ooc");
					let nextEmote = 0;
					let nextOOC = 0;

					for (let i = 0; i < segments.length; i++) {
						switch (segments[i].channel) {
							case LOCAL_CHAT.Channel.Emote:
								segments[i].elm = emoteList[nextEmote++];
								break;
							case LOCAL_CHAT.Channel.OOC:
								segments[i].elm = oocList[nextOOC++];
								break;
						}
					}

					_prevSegments = segments;
				}
			}

			const lookForChanges = (segments) => {
				let patterns = ["", ""];
				let list;
				for (let l = 0; l < patterns.length; l++) {
					list = l == 0 ? segments : _prevSegments;
					for (i = 0; i < list.length; i++) {
						patterns[l] += list[i].channel;
					}
					while (patterns[l].endsWith("0")) {
						patterns[l] = patterns[l].substr(0, patterns[l].length - 1);
					}
				}
				if (patterns[0] != patterns[1]) {
					return true;
				}
				for (i = 0; i < _prevSegments.length; i++) {
					if (_prevSegments[i].elm) {
						if (!isDescendant(_inputField, _prevSegments[i].elm)) {
							return true;
						}
						switch (_prevSegments[i].channel) {
							case LOCAL_CHAT.Channel.Emote:
								if (!_prevSegments[i].elm.textContent.startsWith("*") || !_prevSegments[i].elm.textContent.endsWith("*")) {
									return true;
								}
								break;
							case LOCAL_CHAT.Channel.OOC:
								if (!_prevSegments[i].elm.textContent.startsWith("(") || !_prevSegments[i].elm.textContent.endsWith(")")) {
									return true;
								}
								break;
						}
					}
				}
				return false;
			};
		}
	}

	function findCommand(text) {
		let match = text.toLowerCase().match(_findCommand);
		return match && match[0].substr(1);
	};

	function findSegments(input) {
		let match;
		let beg;
		let end;

		let index = 0;
		let segments = [];
		let str = "";

		while (match = input.substr(index).match(_findOperator)) {
			str += input.substr(index, match.index);
			beg = index + match.index + 1;
			end = input.indexOf(match[0] == "(" ? ")" : match[0], beg);

			if (end >= 0) {
				segments.push({ channel: LOCAL_CHAT.Channel.Say, text: str });
				segments.push({ channel: operatorToChannel(match[0]), text: input.substring(beg, end) });
				str = "";
				index = end + 1;
			}
			else {
				str += match[0];
				index = beg;
			}
		}

		segments.push({ channel: LOCAL_CHAT.Channel.Say, text: str + input.substr(index) });

		for (let i = segments.length - 1; i >= 0; i--) {
			if (segments[i].channel == LOCAL_CHAT.Channel.Say && segments[i].text.length == 0) {
				segments.splice(i, 1);
			}
		}

		return segments;
	}

	function operatorToChannel(char) {
		switch (char) {
			case "*":
				return LOCAL_CHAT.Channel.Emote;
			case "(":
				return LOCAL_CHAT.Channel.OOC;
			default:
				return LOCAL_CHAT.Channel.Say;
		}
	}

	function getCaretPosition(elm) {
		let selection = window.getSelection();
		let charCount = -1;
		if (selection.focusNode) {
			if (isDescendant(elm, selection.focusNode)) {
				let node = selection.focusNode;
				charCount = selection.focusOffset;
				while (node) {
					if (node === elm) {
						break;
					}
					if (node.previousSibling) {
						node = node.previousSibling;
						charCount += node.textContent.length;
						continue;
					}
					node = node.parentNode;
					if (node === null) {
						break
					}
				}
			}
		}
		return charCount;
	};

	function setCaretPosition(elm, chars) {
		if (chars < 0) {
			return;
		}
		let selection = window.getSelection();
		let range = createRange(elm, { count: chars });
		if (range) {
			range.collapse(false);
			selection.removeAllRanges();
			selection.addRange(range);
		}
	};

	function createRange(node, chars) {
		let range = document.createRange()
		range.selectNode(node);
		range.setStart(node, 0);

		if (chars.count === 0) {
			range.setEnd(node, chars.count);
		}
		else if (node && chars.count > 0) {
			if (node.nodeType === Node.TEXT_NODE) {
				if (node.textContent.length < chars.count) {
					chars.count -= node.textContent.length;
				}
				else {
					range.setEnd(node, chars.count);
					chars.count = 0;
				}
			}
			else {
				for (let i = 0; i < node.childNodes.length; i++) {
					range = createRange(node.childNodes[i], chars, range);
					if (chars.count === 0) {
						break;
					}
				}
			}
		}

		return range;
	};
})();
