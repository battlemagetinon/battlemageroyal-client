(() => {
	const transitions = {
		'transition': 'transitionend',
		'WebkitTransition': 'webkitTransitionEnd',
		'MozTransition': 'transitionend',
		'OTransition': 'otransitionend'
	};

	const elm = document.createElement('div');

	for (let prop in transitions) {
		if (elm.style[prop] !== undefined) {
			window.transitionEnd = transitions[prop];
			break;
		}
	}

	if (!Date.now) {
		Date.now = () => new Date().getTime();
	}
})();

const ConjuredType = {
	Concealing: 1,
	Temporary: 2,
	ConcealingHex: 3,
	TemporaryHex: 4,
	IdentifiedHex: 5,
	UnidentifiedHex: 6,
	HiddenHex: 7,
	IsConjured: (conjuredType) => {
		switch (conjuredType) {
			case ConjuredType.Concealing:
			case ConjuredType.ConcealingHex:
			case ConjuredType.Temporary:
			case ConjuredType.TemporaryHex:
				return true;
			default:
				return false;
		}
	},
	IsHexed: (conjuredType) => {
		switch (conjuredType) {
			case ConjuredType.ConcealingHex:
			case ConjuredType.TemporaryHex:
			case ConjuredType.IdentifiedHex:
			case ConjuredType.UnidentifiedHex:
				return true;
			default:
				return false;
		}
	}
};

const findWhitespace = /(\s+){2,}/g;
const findLineBreaks = /(?:\r\n|\r|\n)+/g;
const findConsecutiveLineBreaks = /\n\s*\n\s*\n/g;
const findBreakRows = /\<br\/\>/g;

function modulus(i, m) {
	return (i % m + m) % m;
}

function nameToRef(name) {
	return name.toLowerCase().replace(/\'/g, '').replace(/[\s\-]/g, '_');
};

function toggleClass(elm, cls) {
	elm.classList.contains(cls) ? elm.classList.remove(cls) : elm.classList.add(cls);
}

function clearHTML(elm) {
	while (elm.firstChild) {
		elm.removeChild(elm.firstChild);
	}
}

function escapeHTMLOperators(str) {
	return str.replace(/&/g, "&amp;")
		.replace(/</g, "&lt;")
		.replace(/>/g, "&gt;")
		.replace(/"/g, "&quot;");
}

function collapseLineBreaks(str) {
	return str.replace(findLineBreaks, ' ');
}

function reduceLineBreaks(str) {
	return str.replace(findConsecutiveLineBreaks, "\n\n");
}

function collapseWhitespace(str) {
	return str.trim().replace(findWhitespace, ' ');
}

function removeBreakRows(str) {
	return str.replace(findBreakRows, ' ');
}

function clamp(number, min, max) {
	return Math.min(Math.max(number, min), max);
}

function clamp01(number) {
	return Math.min(Math.max(number, 0), 1);
}

function toArray(obj) {
	const arr = [];
	for (let i = 0; i < obj.length; i++) {
		arr[i] = obj[i];
	}
	return arr;
}

function shuffleArray(arr) {
	for (let i = arr.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		const temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}
}

function getSiblingIndex(node) {
	if (node.parentNode) {
		let i;
		for (i = 0; (node = node.previousSibling); i++);
		return i;
	}
	return -1;
}

function unique(arr) {
	return arr.filter(a => a || a === 0).filter((value, index, self) => self.indexOf(value) === index);
}

function contentToText(elm) {
	let isOnFreshLine = true;
	const parseChildNodesForValueAndLines = (childNodes) => {
		let str = '';
		for (let i = 0; i < childNodes.length; i++) {
			const childNode = childNodes[i];
			if (childNode.nodeName === 'BR') {
				str += '\n';
				isOnFreshLine = true;
				continue;
			}
			if (childNode.nodeName === 'DIV' && isOnFreshLine === false) {
				str += '\n';
			}
			isOnFreshLine = false;
			if (childNode.nodeType === 3) {
				str += childNode.textContent;
			}
			str += parseChildNodesForValueAndLines(childNode.childNodes);
		}
		return str;
	}
	return parseChildNodesForValueAndLines(elm.childNodes);
}

function firstToUpperCase(str) {
	return str.charAt(0).toUpperCase() + str.slice(1);
}

function removeTextNodes(elm) {
	const children = elm.childNodes;
	for (let i = 0; i < children.length; i++) {
		if (children[i].nodeType === 3) {
			children[i].parentNode.removeChild(children[i]);
		}
	}
}

async function wait(millis) {
	return new Promise((resolve) => {
		const timer = setTimeout(() => {
			clearTimeout(timer);
			resolve();
		}, millis);
	});
}

async function waitForSeconds(seconds) {
	return wait(seconds * 1000);
}

async function waitForFrame() {
	return new Promise((resolve) => requestAnimationFrame(resolve));
}

async function waitForFrames(i = 3) {
	return new Promise(async (resolve) => {
		while (i-- > 0) {
			await waitForFrame();
		}
		resolve();
	});
}

async function waitUntil(func) {
	while (!func()) {
		await waitForFrame();
	}
}

function imageLoaded(src) {
	const img = new Image();
	img.src = src;
	return img.complete;
}

async function loadImage(src) {
	return new Promise((resolve, reject) => {
		const img = new Image();
		img.onload = () => resolve(img);
		img.onerror = (e) => reject(e);
		img.src = src;
	});
}

async function transitionEnded(elm) {
	return new Promise((resolve) => {
		const completed = function () {
			elm.removeEventListener(window.transitionEnd, completed);
			resolve();
		};
		elm.addEventListener(window.transitionEnd, completed);
	});
}

function isDescendant(parent, node) {
	if (parent && node) {
		while (node = node.parentNode) {
			if (node === parent) {
				return true;
			}
		}
	}
	return false;
}
