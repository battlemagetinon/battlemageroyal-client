LOCK = {};

(() => {
	const locks = [];

	LOCK.Lock = async (key) => {
		if (key === undefined || key === null || key === true || key === false) {
			return;
		}
		const prevReleased = LOCK.WaitUntilReleased(key);
		const lock = { key: key };
		lock.released = new Promise(resolve => lock.unlock = resolve);
		locks.push(lock);
		return prevReleased;
	}

	LOCK.IsLocked = (key) => {
		for (let i = 0; i < locks.length; i++) {
			if (locks[i].key === key) {
				return true;
			}
		}
		return false;
	}

	LOCK.Count = (key) => {
		let count = 0;
		for (let i = 0; i < locks.length; i++) {
			if (locks[i].key === key) {
				count++;
			}
		}
		return count;
	}

	LOCK.Unlock = (key) => {
		if (key === undefined || key === null || key === true || key === false) {
			return;
		}
		for (let i = 0; i < locks.length; i++) {
			if (locks[i].key === key) {
				locks[i].unlock();
				locks.splice(i, 1);
				break;
			}
		}
	}

	LOCK.WaitUntilReleased = async (key) => {
		for (let i = locks.length - 1; i >= 0; i--) {
			if (locks[i].key === key) {
				return locks[i].released;
			}
		}
		return null;
	}

})();
