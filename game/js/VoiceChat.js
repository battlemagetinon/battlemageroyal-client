(() => {
	VOICE_CHAT = {
		DURATION_MILLIS: 2500,
		Commands: {
			Greet: 0,
			Thank: 1,
			Threaten: 2,
			Plead: 3
		}
	};

	let _instance;
	Object.defineProperty(VOICE_CHAT, "instance", {
		get: () => {
			if (!_instance) {
				_instance = new VOICE_CHAT.VoiceChat();
			}
			return _instance;
		}
	});
})();

VOICE_CHAT.VoiceChat = function () {
	const _voiceChat = document.getElementById("voice_chat");
	const _messages = [];

	let _message;
	let _scale;
	let _i;

	this.Display = async (message, position) => {
		if (!message) {
			return;
		}

		const li = document.createElement("li");
		const div = document.createElement("div");

		div.innerHTML = message;

		switch (position) {
			case GUI.Position.Right:
				li.className = "right";
				break;
			case GUI.Position.Left:
			default:
				li.className = "left";
				break;
		}

		li.classList.add("inactive");
		li.appendChild(div);
		_voiceChat.appendChild(li);

		await waitForFrames();
		li.classList.remove("inactive");
		_messages.push({ elm: li, text: div, progress: 0, timestamp: Date.now() });
	}

	this.Update = (now) => {
		for (_i = _messages.length - 1; _i >= 0; _i--) {
			_message = _messages[_i];

			if (_message.elm.parentNode) {
				_message.progress = Math.min(1, (now - _message.timestamp) / 300);

				_scale = _message.progress - 1;
				_scale = _scale * _scale * ((ACTION_HUB.TENSION + 1) * _scale + ACTION_HUB.TENSION) + 1;

				_message.text.style.fontSize = (16 / 9) * 9 * 2.5 * _scale * (container.offsetHeight / 900) + "px";

				if (now - _message.timestamp >= VOICE_CHAT.DURATION_MILLIS) {
					const message = _message;
					message.elm.classList.add("inactive");
					transitionEnded(message.elm).then(() => message.elm.parentNode && message.elm.parentNode.removeChild(message.elm));
				}
			}
			else {
				_messages.splice(_i, 1);
			}
		}
	}
}