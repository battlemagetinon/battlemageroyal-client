(() => {
    const LOCAL_STORAGE_KEY = 'SETTINGS.data';
    let _data;

    try {
        _data = JSON.parse(window.localStorage.getItem(LOCAL_STORAGE_KEY) || "{}");
    }
    catch (e) {
        _data = {};
    }

    SETTINGS = {};

    SETTINGS.Get = (key, defaultValue) => _data[key] !== undefined ? _data[key] : defaultValue;

    SETTINGS.Set = (key, value) => window.localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(Object.assign(_data, { [key]: value })));

    SETTINGS.SetMany = (data) => window.localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(Object.assign(_data, data)));

    SETTINGS.Remove = (key) => {
        if (key in _data) {
            delete _data[key];
            window.localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(_data));
        }
    };

    SETTINGS.Reset = () => {
        _data = {};
        window.localStorage.removeItem(LOCAL_STORAGE_KEY);
    };
})();
