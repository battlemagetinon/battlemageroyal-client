(() => {
	GUI = {
		BACKGROUND_TRANSITION_DURATION: 1000,
		ERROR_MESSAGE_DURATION_MILLIS: 5000,
		Position: {
			Left: 0,
			Right: 1
		},
		Animation: {
			Hit: "hit",
			Drop: "drop",
			Shrink: "shrink",
			HitFade: "hit_fade",
			HitShrink: "hit_shrink",
			HitDrop: "hit_drop",
			Transgender: "transgender",
		}
	};

	let _instance;
	Object.defineProperty(GUI, "instance", {
		get: () => {
			if (!_instance) {
				_instance = new GUI.Gui();
			}
			return _instance;
		}
	});

	GUI.Gui = function () {
		const _preloader = document.getElementById("preloader");
		const _frame = document.getElementById("frame");
		const _characters = document.getElementById("characters");
		const _dialog = document.getElementById("dialog");
		const _menuButton = document.getElementById("menu").getElementsByClassName("button")[0];
		const _myselfButton = document.getElementById("myself").getElementsByClassName("button")[0];
		const _options = document.getElementById("options");
		const _acquisition = document.getElementById("acquisition");
		const _overheadStatus = document.getElementById("overhead_status");
		const _location = document.getElementById("location");
		const _actions = document.getElementById("actions");
		const _errorMessages = document.getElementById("error_messages");
		const _tokens = document.getElementById("tokens");

		const _backgroundWrapper = document.getElementById("background_wrapper");

		const _characterHoverCanvas = document.getElementById("character_hover").getElementsByTagName("canvas");
		const _characterHoverImages = [];
		const _characterHoverImageData = [];
		const _preloaded = [];

		const _actionHubs = [];

		const _findThem = /\$them/g;
		const _findTheir = /\$their/g;

		let _background = _backgroundWrapper.getElementsByTagName("div")[0];

		let _playerStatus;
		let _opponentStatus;

		let _currentBackground = null;

		let _alert = null;
		let _alertResolve;

		let _backgroundImage = null;
		let _backgroundMouseEnabled = false;
		let _backgroundOffset = 0;
		let _backgroundMouseOffset = 0;

		const _animatorSize = { width: 512, height: 648 };
		const _animatorDuration = 2000;

		let _animator;
		let _animatorCanvas;
		let _animatorProgress = 0;

		let _isSayCommand = false;
		let _character = null
		let _position = -1;
		let _start = 0;

		let _deltaTime;
		let _now;
		let _prevTime = 0;
		let _i;

		const _displayedCharacters = [null, null];
		const _dialogList = _dialog.getElementsByTagName("li");

		let _rejectHideDialog;
		let _menuCancellable;

		let _parallaxEnabled = SETTINGS.Get("background_parallax", true);

		Object.defineProperties(this, {
			'parallaxEnabled': {
				get: () => _parallaxEnabled,
				set: (enabled) => {
					_parallaxEnabled = enabled;
					if (!enabled) {
						if (_backgroundImage) {
							_backgroundWrapper.style.transform = "";
						}
						_frame.style.transform = "";
					}
				}
			},
		});

		{
			removeTextNodes(_characters);
			const list = _characters.getElementsByTagName("li");
			for (let i = 0; i < list.length; i++) {
				list[i].onmousemove = e => GUI.instance.MouseOverCharacter(e) ? e.target.classList.add("hover") : e.target.classList.remove("hover");
				list[i].onmouseout = e => e.target.classList.remove("hover");
				list[i].onclick = e => {
					if (CURSOR.instance.locked) {
						return;
					}
					const index = getSiblingIndex(e.currentTarget);
					const dazed = GAME_MANAGER.instance.IsDazed();

					const x = e.pageX !== undefined ? e.pageX : e.targetTouches[0].pageX;
					const y = e.pageY !== undefined ? e.pageY : e.targetTouches[0].pageY;

					GUI.instance.CloseActionHubs();

					if (GUI.instance.MouseOverCharacter(e)) {
						if (!_actionHubs[index] || _actionHubs[index].IsClosed()) {
							if (index == 0) {
								const actions = [
									!LOCATION.instance.npc && (!dazed || _playerStatus && _playerStatus.mind > 0)
										? new ACTION.Action("Vocal", ACTION.Cost.None, [
											new ACTION.Action("Greet", ACTION.Cost.None, () => GAME_MANAGER.instance.Send({ action: "VoiceChat", command: VOICE_CHAT.Commands.Greet })),
											new ACTION.Action("Thank", ACTION.Cost.None, () => GAME_MANAGER.instance.Send({ action: "VoiceChat", command: VOICE_CHAT.Commands.Thank })),
											new ACTION.Action("Threaten", ACTION.Cost.None, () => GAME_MANAGER.instance.Send({ action: "VoiceChat", command: VOICE_CHAT.Commands.Threaten })),
											new ACTION.Action("Plead", ACTION.Cost.None, () => GAME_MANAGER.instance.Send({ action: "VoiceChat", command: VOICE_CHAT.Commands.Plead })),
										])
										: null,
									!dazed ? new ACTION.Action("Action", ACTION.Cost.Spell, () => MENU.Spells.Open({ position: GUI.Position.Right, target: true, isSelf: true, onlySpellActions: true })) : null,
									!dazed ? new ACTION.Action("Action", ACTION.Cost.Action, () => MENU.Spells.Open({ position: GUI.Position.Right, target: true, isSelf: true, onlySpellActions: false })) : null,
									!LOCATION.instance.opponent && !dazed && (LOCATION.instance.explorable || LOCATION.instance.searchable)
										? new ACTION.Action(LOCATION.instance.explorable ? "Explore" : "Search", ACTION.Cost.Action, (action) => GAME_MANAGER.instance.Send({ action: "Explore", search: action.label == "Search" }))
										: null,
									LOCATION.instance.opponent && !dazed || LOCATION.instance.npc || LOCATION.instance.faculty ? new ACTION.Action("Leave", GAME_MANAGER.instance.IsDazed("opponent") || LOCATION.instance.faculty ? ACTION.Cost.None : ACTION.Cost.Action, () => GAME_MANAGER.instance.Send({ action: "Leave" })) : null,
								];
								_actionHubs[index] = new ACTION_HUB.ActionHub(x, y, _displayedCharacters[index].nature, false, actions);
							}
							else if (LOCATION.instance.npc) {
								const options = LOCATION.instance.npc.options || [];
								const actions = [];
								const usedInteractions = GAME_MANAGER.instance.GetUsedInteractions(LOCATION.instance.npc.names);
								for (let i = 0; i < options.length; i++) {
									let option = options[i];
									actions.push(new ACTION.Action(option.label, option.cost, () => GAME_MANAGER.instance.Send({ action: "Location", interaction: option.event }), usedInteractions.indexOf(option.event) < 0));
								}
								_actionHubs[index] = new ACTION_HUB.ActionHub(x, y, LOCATION.instance.npc.nature, true, actions);
							}
							else {
								const ignored = GAME_MANAGER.instance.OpponentIgnored();
								const specialAction = LOCATION.instance.specialActions;
								const actions = [
									!dazed ? new ACTION.Action("Action", ACTION.Cost.Spell, () => MENU.Spells.Open({ position: GUI.Position.Left, target: true, isSelf: false, onlySpellActions: true })) : null,
									!dazed ? new ACTION.Action("Action", ACTION.Cost.Action, () => MENU.Spells.Open({ position: GUI.Position.Left, target: true, isSelf: false, onlySpellActions: false })) : null,
									!dazed && specialAction && specialAction.sex.length + specialAction.spells.length + specialAction.actions.length > 0
										? new ACTION.Action("Special", ACTION.Cost.SpecialAction, [
											specialAction.sex.length > 0
												? new ACTION.Action("Sex", ACTION.Cost.SpecialAction, async () => {
													const labels = [];
													const actionsByLabel = {}
													const displayedCharacter = _displayedCharacters[characterToPosition(LOCATION.instance.opponent)];
													for (let i = 0; i < specialAction.sex.length; i++) {
														let label = specialAction.sex[i].replace(_findTheir, displayedCharacter.eir).replace(_findThem, displayedCharacter.em);
														actionsByLabel[label] = specialAction.sex[i];
														labels.push(label);
													}
													labels.sort();
													MENU.CloseAll();
													let index = await GUI.instance.ShowMenu(labels, true);
													GAME_MANAGER.instance.SpecialAction("sex", index >= 0 ? actionsByLabel[labels[index]] : null);
												})
												: null,
											specialAction.spells.length > 0 ? new ACTION.Action("Transformation", ACTION.Cost.SpecialSpell, async () => {
												specialAction.spells.sort();
												MENU.CloseAll();
												const index = await GUI.instance.ShowMenu(specialAction.spells, true);
												GAME_MANAGER.instance.SpecialAction("spell", index >= 0 ? specialAction.spells[index] : null);
											}) : null,
											specialAction.actions.length > 0 ? new ACTION.Action("Transformation", ACTION.Cost.SpecialAction, () => { }) : null,
										])
										: null,
									new ACTION.Action("Profile", ACTION.Cost.None, () => LOCATION.instance.opponent && window.open(`/character/${LOCATION.instance.opponent.token}`)),
									new ACTION.Action(ignored ? "Unignore" : "Ignore", ACTION.Cost.None, () => GAME_MANAGER.instance.IgnoreOpponent(!ignored)),
								];
								_actionHubs[index] = new ACTION_HUB.ActionHub(x, y, _displayedCharacters[index].nature, true, actions);
							}

							e.target.classList.remove("hover");
						}
						else {
							e.target.classList.add("hover");
						}
					}
				};
			}

			const images = _preloader.getElementsByTagName("img");
			for (let i = 0; i < images.length; i++) {
				_preloaded.push(images[i].src);
			}

			const icons = document.getElementById("frame_top_right").getElementsByClassName("icons")[0];
			icons.getElementsByClassName("unread_messages")[0].onclick = () => MENU.Messages.Open();
			icons.getElementsByClassName("friend_requests")[0].onclick = () => MENU.Social.Open();
			icons.getElementsByClassName("online_friends")[0].onclick = () => MENU.Social.Open();
			icons.getElementsByClassName("roleplay_broadcasting")[0].onclick = () => MENU.Social.OpenScenarioCreation();
			icons.getElementsByClassName("crafting")[0].onclick = () => MENU.Crafting.Open({ item: GAME_MANAGER.instance.GetCraftingBench() });
		};

		_backgroundWrapper.addEventListener("click", async () => {
			const title = document.getElementById("location_title");
			if (title && title.getElementsByTagName("span").length >= 2) {
				if (await GUI.instance.Alert("Stop waiting for another player to join and continue to location immediately?", "Stop", "Wait")) {
					GAME_MANAGER.instance.Send({ action: "Location", nextLocation: true });
				}
			}
		});

		_myselfButton.onclick = () => MENU.Myself.Toggle(GUI.Position.Left);

		_menuButton.onclick = (e) => DROPDOWN.instance.Open(e,
			[
				{
					label: "Inventory",
					onclick: () => MENU.Inventory.Open()
				},
				{
					label: "Spells",
					onclick: () => MENU.Spells.Open({ position: GUI.Position.Right })
				},
				{
					label: "Skills",
					onclick: () => MENU.Skills.Open()
				},
				{
					label: "Social",
					onclick: () => MENU.Social.Open()
				},
				{
					label: "Messages",
					onclick: () => MENU.Messages.Open()
				},
				{
					label: "Profile",
					onclick: () => window.open(`/character/${LOCATION.instance.player.token}`)
				},
				{
					label: "Settings",
					onclick: () => MENU.Settings.Open()
				},
				{
					label: "Exit",
					onclick: () => this.ExitAlert()
				},
			],
			{
				left: "2%",
				top: "6%"
			}
		);

		container.addEventListener("mousemove", e => {
			if (_backgroundMouseEnabled && !_alert) {
				const rect = container.getBoundingClientRect();
				const x = e.pageX !== undefined ? e.pageX : e.targetTouches[0].pageX;
				const y = e.pageY !== undefined ? e.pageY : e.targetTouches[0].pageY;
				if (y < rect.height * 0.75) {
					_backgroundMouseOffset = ((x - rect.x) / rect.width) * 2 - 1;
				}
			}
		});

		this.CloseActionHubs = (e) => {
			for (let i = 0; i < _actionHubs.length; i++) {
				if (_actionHubs[i] && (e === undefined || !isDescendant(_actions, e.target))) {
					_actionHubs[i].Close();
				}
			}
			if (_menuCancellable) {
				this.HideMenu();
			}
		}

		container.addEventListener("click", this.CloseActionHubs);
		_location.onclick = MENU.Location.Toggle;

		this.MouseOverCharacter = (e) => {
			const index = getSiblingIndex(e.target.parentNode);

			if (
				!_characterHoverImageData[index] || GAME_MANAGER.instance.InScenario() ||
				_actionHubs[index] && !(_actionHubs[index].IsClosed() || _actionHubs[index].IsClosing())
			) {
				return false;
			}

			const imageData = _characterHoverImageData[index];
			const rect = e.target.getBoundingClientRect();
			let x = e.pageX !== undefined ? e.pageX : e.targetTouches[0].pageX;
			let y = e.pageY !== undefined ? e.pageY : e.targetTouches[0].pageY;
			x = Math.round((e.pageX - rect.x - imageData.width) * (index === 0 ? -1 : 1));
			y = Math.round(e.pageY - rect.y);

			return imageData.data[(y * imageData.width + x) * 4 + 3] > 255 / 2;
		}

		this.Alert = async (message, option1, option2 = "Cancel") => new Promise(resolve => {
			this.CloseAlert();

			document.activeElement.blur();

			_alert = document.createElement("div");
			_alert.className = "alert";
			_alertResolve = resolve;

			let wrapper = document.createElement("div");
			wrapper.className = "wrapper";

			let div = document.createElement("div");
			div.innerHTML = message;
			wrapper.appendChild(div);

			div = document.createElement("div");
			div.innerHTML = option1;
			div.onclick = () => {
				resolve(true);
				this.CloseAlert();
			};

			wrapper.appendChild(div);

			if (option2) {
				div = document.createElement("div");
				div.innerHTML = option2;
				div.onclick = () => this.CloseAlert();
				wrapper.appendChild(div);
			}

			_alert.appendChild(wrapper);
			container.appendChild(_alert);
		});

		this.ExitAlert = async () => {
			if (await this.Alert("Exit to the main page?", "Exit")) {
				window.location.href = '/';
			}
		}

		this.AlertIsOpen = () => _alert && _alert.parentNode;

		this.CloseAlert = () => {
			if (_alertResolve) {
				_alertResolve(false);
				_alertResolve = null;
			}
			if (_alert && _alert.parentNode) {
				_alert.parentNode.removeChild(_alert);
				_alert = null;
				return true;
			}
			_alert = null;
			return false;
		}

		this.Prompt = async (message, defaultValue, option1, option2 = "Cancel") => new Promise(resolve => {
			this.CloseAlert();

			document.activeElement.blur();

			_alert = document.createElement("div");
			_alert.className = "alert prompt";
			_alertResolve = resolve;

			const wrapper = document.createElement("div");
			wrapper.className = "wrapper";

			let div = document.createElement("div");
			div.innerHTML = message;
			wrapper.appendChild(div);

			let form = document.createElement("form");
			form.autocomplete = "off"
			form.method = "post";
			form.action = "#";
			form.onsubmit = (e) => {
				e.preventDefault();
				resolve(input.value);
				this.CloseAlert();
				false;
			};
			wrapper.appendChild(form);

			let input = document.createElement("input");
			input.type = "text";
			input.maxLength = 255;
			input.value = GAME_MANAGER.instance.Evaluate(defaultValue);
			form.appendChild(input);

			div = document.createElement("div");
			div.innerHTML = option1;
			div.onclick = () => {
				resolve(input.value);
				this.CloseAlert();
			};

			wrapper.appendChild(div);

			if (option2) {
				div = document.createElement("div");
				div.innerHTML = option2;
				div.onclick = () => this.CloseAlert();
				wrapper.appendChild(div);
			}

			_alert.appendChild(wrapper);
			container.appendChild(_alert);
			input.focus();
			if (input.value.length > 0) {
				input.setSelectionRange(0, input.value.length);
			}
		});

		this.ShowMenu = async (options, cancellable) => {
			const ul = document.createElement("ul");

			_menuCancellable = false;

			clearHTML(_options);

			if (options.length == 0) {
				return;
			}

			if (_displayedCharacters[GUI.Position.Left] && _displayedCharacters[GUI.Position.Left].nature) {
				ul.className = _displayedCharacters[GUI.Position.Left].nature;
			}

			for (let i = 0; i < options.length; i++) {
				const li = document.createElement("li");

				if (typeof options[i] === "string") {
					li.innerHTML = options[i];
				}
				else {
					li.innerHTML = options[i].label;

					if (options[i].isAction) {
						li.className = options[i].isSpell ? "spell" : "star";
						ul.classList.add("tokens");
					}

					if (options[i].isAction && GAME_MANAGER.instance.actions.actions == 0 || options[i].isSpell && GAME_MANAGER.instance.actions.spells == 0) {
						li.classList.add("disabled");
					}
				}

				ul.appendChild(li);
			}

			_options.appendChild(ul);
			const list = ul.getElementsByTagName("li");

			MENU.CloseAll();

			await waitForFrames();

			_options.style.top = `${50 - (ul.offsetHeight / container.offsetHeight) / 2 * 100}%`;

			container.classList.add("choice");
			_options.classList.remove("inactive");

			if (list.length > 0) {
				if (ul.offsetHeight > 0) {
					await transitionEnded(list[list.length - 1]);
				}

				_menuCancellable = cancellable;

				let index = -1;
				const onOptionClicked = async (elm) => index = getSiblingIndex(elm);

				for (let i = 0; i < list.length; i++) {
					(entry => {
						entry.onclick = () => onOptionClicked(entry);
					})(list[i]);
				}

				await waitUntil(() => index >= 0 || _options.classList.contains("inactive"));
				await this.HideMenu();

				return index;
			}
		};

		this.HideMenu = async () => {
			if (_options.classList.contains("inactive")) {
				return;
			}
			_options.classList.add("inactive");
			await Promise.race([
				waitUntil(() => window.getComputedStyle(_options).display == "none"),
				transitionEnded(_options),
			]);
			container.classList.remove("choice");
		};

		this.ShowCrafting = async (params, item) => {
			let obj = null;
			MENU.Crafting.Open(Object.assign(params, { item: item || GAME_MANAGER.instance.GetCraftingBench(), callback: response => obj = response, }));
			await waitUntil(() => obj != null);
			return obj;
		};

		this.PreloadImage = async (source) => {
			if (_preloaded.indexOf(source) >= 0) {
				return await loadImage(source);
			}
			_preloaded.push(source);
			const image = await loadImage(source);
			_preloader.appendChild(image);
			return image;
		};

		this.SetCharacter = (character, position, enterOrLeave, key) => {
			if (character) {
				return this.ShowCharacter(character, position, enterOrLeave, key);
			}
			return this.HideCharacter(position, enterOrLeave, false, key);
		};

		this.ShowCharacter = async (character, position, enter, key) => {
			await LOCK.Lock(key || this);
			try {
				const morph = character && typeof character === "object" && character.morph;

				if (position === undefined || position < 0) {
					position = character.player ? GUI.Position.Left : GUI.Position.Right;
				}

				const prevCharacter = _displayedCharacters[position];

				if (prevCharacter && prevCharacter.id === CHARACTER.GetId(character)) {
					if (prevCharacter.gender !== character.gender) {
						await this.UpdateCharacter(character, true);
					}
					if (prevCharacter.GetSprite() !== CHARACTER.GetSprite(character)) {
						await this.UpdateCharacterImmediately(character, true);
					}
					return false;
				}

				if (character.id === undefined) {
					character = new CHARACTER.Character(character);
				}

				const elm = _characters.getElementsByTagName("li")[position];

				if (prevCharacter) {
					await this.HideCharacter(position, false, false, true);
				}

				if (_displayedCharacters[(position + 1) % 2] && _displayedCharacters[(position + 1) % 2].id === character.id) {
					await this.HideCharacter((position + 1) % 2, false, false, true);
				}

				_displayedCharacters[position] = character;
				elm.className = `inactive ${character.gender} ${character.nature}`;

				const div = document.createElement("div");
				const sprite = character.GetSprite();

				if (character.HasSprite()) {
					div.style.backgroundImage = `url(${sprite})`;
				}
				else if (!character.player) {
					elm.classList.add("npc");
				}

				clearHTML(elm);
				elm.appendChild(div);

				if (enter) {
					elm.classList.add("enter");
				}

				await waitForFrames();

				try {
					const img = await this.PreloadImage(sprite);
					setCharacterHoverImage(img, position);
				}
				catch (e) {
					div.style.backgroundImage = "";
				}

				if (morph) {
					const rect = elm.getBoundingClientRect();
					const canvas = elm.getElementsByTagName("canvas")[0];
					if (!canvas) {
						canvas = document.createElement("canvas");
					}
					canvas.width = rect.width;
					canvas.height = rect.height;
					const avatarURL = window.getComputedStyle(div).backgroundImage;
					const beg = avatarURL.indexOf("http");
					const end = Math.min(avatarURL.lastIndexOf("\""), avatarURL.lastIndexOf(")"))
					const avatarImage = await loadImage(avatarURL.substring(beg, end > 0 ? end : avatarURL.length));
					const morphImage = await loadImage(IMAGE_PROCESSING.getMorphImage(morph.image, morph.color));
					await IMAGE_PROCESSING.ready;
					const points = [IMAGE_PROCESSING.findPoints(avatarImage), IMAGE_PROCESSING.findPoints(morphImage)];
					if (points[0] && points[1]) {
						const animator = new IMAGE_PROCESSING.Animator(points[0], Object.assign(points[1], Object.assign(morph.details, { flipped: position === 0 })), [-0.1, 1.1]);
						animator.applyMask = true;
						animator.antialiasing = true;
						elm.appendChild(canvas);
						animator.DrawFrame(1, canvas, Math.round(canvas.width), Math.round(canvas.height));
						div.style.display = "none";
					}
				}

				elm.classList.remove("inactive");
				await transitionEnded(elm);
				return true;
			}
			finally {
				LOCK.Unlock(key || this);
			}
		};

		this.HideCharacter = async (character, leave, closeChat, key) => {
			await LOCK.Lock(key || this);
			try {
				const position = typeof character === "number" ? character : characterToPosition(character);

				if (position < 0) {
					return;
				}

				const elm = _characters.getElementsByTagName("li")[position];
				_displayedCharacters[position] = null;
				setCharacterHoverImage(null, position);

				if (closeChat !== false) {
					LOCAL_CHAT.Close(position);
				}

				if (!elm.classList.contains("inactive")) {
					elm.classList.remove("enter");
					if (leave) {
						elm.classList.add("leave");
					}

					_actionHubs[position] && _actionHubs[position].Close();

					await waitForFrames();

					elm.classList.add("inactive");

					await transitionEnded(elm);

					elm.classList.remove("leave");
				}

				const div = elm.getElementsByTagName("div")[0];
				if (div) {
					div.style.display = null
				}

				const canvas = elm.getElementsByTagName("canvas")[0];
				if (canvas) {
					if (canvas === _animatorCanvas) {
						_animatorCanvas = null;
						_animator = null;
					}
					if (canvas.parentNode) {
						canvas.parentNode.removeChild(canvas);
					}
				}
			}
			finally {
				LOCK.Unlock(key || this);
			}
		};

		this.SetLocation = async (location, key) => {
			if (!location) {
				return;
			}

			await LOCK.Lock(key || this);
			try {
				const title = _location.getElementsByClassName("location")[0];
				title.textContent = location.title;

				let background = LOCATION.GetBackgroundURL(location, CALENDAR.instance.daytime);

				if (background === _currentBackground) {
					return;
				}

				LOCAL_CHAT.Clear();
				this.HideStatus();
				this.HideDialog();

				await this.HideCharacter(GUI.Position.Left, true, false, true);
				await this.HideCharacter(GUI.Position.Right, false, false, true);

				if (background != _currentBackground) {
					_backgroundMouseEnabled = false;

					if (_currentBackground) {
						let list = _backgroundWrapper.getElementsByTagName("div");
						for (let i = 0; i < list.length; i++) {
							list[i].classList.add("inactive");
						}
						await wait(GUI.BACKGROUND_TRANSITION_DURATION + 200);
					}

					_currentBackground = LOCATION.GetBackgroundURL(location, CALENDAR.instance.daytime);

					if (_currentBackground) {
						_background.style.backgroundImage = `url(${_currentBackground})`;
						_backgroundImage = await this.PreloadImage(_currentBackground);

						_background.classList.remove("inactive");
						await wait(GUI.BACKGROUND_TRANSITION_DURATION);

						const title = document.getElementById("location_title");
						title && title.parentNode.removeChild(title);
					}
					else {
						_background.style.backgroundImage = null;
						_backgroundImage = null;
					}
					_backgroundMouseEnabled = true;
				}
			}
			finally {
				LOCK.Unlock(key || this);
			}
		};

		this.UpdateLocationBackground = async (waitUntilReleased = true) => {
			if (waitUntilReleased) {
				await LOCK.WaitUntilReleased(this);
			}

			if (_background && !_background.classList.contains("inactive")) {
				const div = document.createElement("div");
				let updated = false;
				let background;

				while ((background = LOCATION.instance.GetBackgroundURL(CALENDAR.instance.daytime)) !== _currentBackground) {
					div.style.backgroundImage = `url(${background})`;
					_currentBackground = background;
					_backgroundImage = await GUI.instance.PreloadImage(background);
					updated = true;
				}

				if (updated && !LOCK.IsLocked(this)) {
					const prevBackground = _background;

					_background = div;
					_backgroundWrapper.insertBefore(_background, prevBackground);

					await wait(GUI.BACKGROUND_TRANSITION_DURATION);

					_backgroundWrapper.removeChild(prevBackground);
				}
			}
		};

		this.UpdateLocation = async (location) => {
			await LOCK.Lock(this);
			try {
				const title = _location.getElementsByClassName("location")[0];
				let background;

				while ((background = location.GetBackgroundURL(CALENDAR.instance.daytime)) !== _currentBackground || title.textContent !== location.title) {
					if (location.background.includes(_currentBackground)) {
						await this.UpdateLocationBackground(false);
					}
					else {
						await this.SetLocation(location, true);
					}
				}

				await this.SetCharacter(location.player, GUI.Position.Left, true, true);

				LOCAL_CHAT.Resume(GUI.Position.Left);

				if (await this.SetCharacter(location.opponent || location.npc, GUI.Position.Right, true, true)) {
					if (location.opponent) {
						this.ShowStatus();
						this.OpenLocalChat();
					}
					else {
						this.HideOpponentStatus();
						if (location.npc) {
							VOICE_CHAT.instance.Display(_displayedCharacters[1] && _displayedCharacters[1].GetGreeting(), GUI.Position.Right);
						}
					}
				}
			}
			finally {
				LOCK.Unlock(this);
			}
		};

		this.UpdateCharacter = async (character, key) => {
			await LOCK.Lock(key || this);
			try {
				position = characterToPosition(character);
				await this.HideCharacter(character, false, false, true);
				await this.ShowCharacter(character, position, false, true);
			}
			finally {
				LOCK.Unlock(key || this);
			}
		};

		this.UpdateCharacterImmediately = async (character, key) => {
			if (!character) {
				return;
			}
			await LOCK.Lock(key || this);
			try {
				const position = characterToPosition(character);
				if (position >= 0) {
					const elm = _characters.getElementsByTagName("li")[position];
					const div = document.createElement("div");
					div.style.backgroundImage = `url(${character.expression})`;
					const img = await this.PreloadImage(character.expression);
					clearHTML(elm);
					elm.appendChild(div);
					setCharacterHoverImage(img, position);
				}
			}
			finally {
				LOCK.Unlock(key || this);
			}
		};

		this.LocationReady = async () => {
			const title = document.getElementById("location_title");

			if (title && title.getElementsByTagName("span").length < 2) {
				const span = document.createElement("span");
				const labels = ["Waiting for encounter", "Click to stop"];
				span.innerHTML = labels[0];
				title.appendChild(span);

				let prevOpacity = 0;
				let opacity = 0;
				let direction = 0;
				let prevDirection = 0;
				let i = 0;

				do {
					opacity = getComputedStyle(span).opacity;
					direction = opacity - prevOpacity;
					if (direction > 0 && prevDirection < 0) {
						i = (i + 1) % labels.length;
						span.innerHTML = labels[i];
					}
					prevOpacity = opacity;
					prevDirection = direction;
					await waitForFrame();
				}
				while (span && span.parentNode);
			}
		};

		this.LeaveLocation = async (destination) => {
			await LOCK.Lock(this);
			try {
				LOCAL_CHAT.Clear();
				this.HideDialog();

				if (_displayedCharacters[0]) {
					await this.HideCharacter(GUI.Position.Left, true, false, true);
				}

				if (_displayedCharacters[1]) {
					await this.HideCharacter(GUI.Position.Right, false, false, true);
				}

				if (_currentBackground) {
					_backgroundMouseEnabled = false;

					const list = _backgroundWrapper.getElementsByTagName("div");
					for (let i = 0; i < list.length; i++) {
						list[i].classList.add("inactive");
					}
					await wait(GUI.BACKGROUND_TRANSITION_DURATION + 200);

					_background.style.backgroundImage = null;
					_backgroundImage = null;
					_currentBackground = null;
				}

				if (destination) {
					const title = document.getElementById("location_title") || document.createElement("div");
					title.id = "location_title";
					title.className = "inactive";
					clearHTML(title);

					const span = document.createElement("span");
					span.innerHTML = LOCATION.LocationToTitle(destination) || destination;
					title.appendChild(span);

					container.prepend(title);

					await waitForFrames();

					title.classList.remove("inactive");
				}
			}
			finally {
				LOCK.Unlock(this);
			}
		};

		this.AnimateCharacter = async (character, animation) => {
			await LOCK.Lock(this);
			try {
				const position = characterToPosition(character);

				if (position < 0) {
					return;
				}

				if (character.id === undefined) {
					character = new CHARACTER.Character(character);
				}

				const elm = _characters.getElementsByTagName("li")[position];

				elm.classList.remove("leave");
				elm.classList.remove("enter");

				switch (animation) {
					case GUI.Animation.HitFade:
					case GUI.Animation.HitDrop:
					case GUI.Animation.HitShrink:
					case GUI.Animation.Drop:
					case GUI.Animation.Shrink:
						_displayedCharacters[position] = null;
						setCharacterHoverImage(null, position);
						break;
				}

				switch (animation) {
					case GUI.Animation.Hit:
					case GUI.Animation.HitFade:
					case GUI.Animation.HitDrop:
					case GUI.Animation.HitShrink:
						elm.classList.add("flash");
						if (character.id === CHARACTER.GetId(LOCATION.instance.player)) {
							flashBackground();
						}
						await transitionEnded(elm);
						switch (animation) {
							case GUI.Animation.Hit:
								elm.classList.add("out");
								await transitionEnded(elm);
								elm.classList.remove("flash");
								elm.classList.remove("out");
								return;
							case GUI.Animation.HitFade:
								elm.classList.add("fade");
								break;
							case GUI.Animation.HitDrop:
								elm.classList.add("drop");
								break;
							case GUI.Animation.HitShrink:
								elm.classList.add("shrink");
								break;
						}
						await transitionEnded(elm);
						break;
					case GUI.Animation.Drop:
					case GUI.Animation.Shrink:
						elm.classList.add(animation);
						await transitionEnded(elm);
						break;
					case GUI.Animation.Transgender:
						await this.HideCharacter(position, false, false, true);
						character.gender = character.gender == "male" ? "female" : "male";
						await this.ShowCharacter(character, position, false, true);
					default:
						return;
				}

				elm.className = "inactive";
			}
			finally {
				LOCK.Unlock(this);
			}
		};

		this.MorphCharacter = async (character, imageURL, settings, color) => {
			await LOCK.Lock(this);
			try {
				const position = characterToPosition(character);

				if (position < 0) {
					return;
				}

				const elm = _characters.getElementsByTagName("li")[position];
				const div = elm.getElementsByTagName("div")[0];

				const avatarURL = window.getComputedStyle(div).backgroundImage;
				const beg = avatarURL.indexOf("http");
				const end = Math.min(avatarURL.lastIndexOf("\""), avatarURL.lastIndexOf(")"))

				const avatarImage = await loadImage(avatarURL.substring(beg, end > 0 ? end : avatarURL.length));
				const morphImage = await loadImage(IMAGE_PROCESSING.getMorphImage(imageURL, color));

				await IMAGE_PROCESSING.ready;

				const points = [IMAGE_PROCESSING.findPoints(avatarImage), IMAGE_PROCESSING.findPoints(morphImage)];

				if (!points[0] || !points[1]) {
					return;
				}

				const animator = new IMAGE_PROCESSING.Animator(points[0], Object.assign(points[1], Object.assign(settings || {}, { flipped: position === 0 })), [-0.1, 1.1]);
				animator.applyMask = true;
				animator.antialiasing = true;

				await waitForFrame();

				const canvas = elm.getElementsByTagName("canvas")[0];
				if (!canvas) {
					canvas = document.createElement("canvas");
				}

				const rect = elm.getBoundingClientRect();
				canvas.width = Math.round(rect.width);
				canvas.height = Math.round(rect.height);
				elm.appendChild(canvas);

				_animator = animator;
				_animatorProgress = 0;
				_animatorCanvas = canvas;
				_animatorSize.width = canvas.width;
				_animatorSize.height = canvas.height;

				await waitUntil(() => _animatorProgress > 0);
				div.style.display = "none";
			}
			finally {
				LOCK.Unlock(this);
			}
		};

		this.ResetMorph = async () => {
			await LOCK.Lock(this);
			try {
				_animatorProgress = 1;

				if (_animator) {
					_animator = null;
				}

				const li = _characters.getElementsByTagName("li")[0];
				const canvas = li.getElementsByTagName("canvas")[0];
				if (canvas) {
					await this.HideCharacter(GUI.Position.Left, false, false, true);
				}

				const list = _characters.getElementsByTagName("div");
				for (let i = 0; i < list.length; i++) {
					if (list[i].style.display == "none") {
						list[i].style.display = "";
					}
				}

				if (_animatorCanvas && _animatorCanvas.parentNode) {
					_animatorCanvas.parentNode.removeChild(_animatorCanvas);
				}
			}
			finally {
				LOCK.Unlock(this);
			}
		};

		this.SetTextParameters = (isSayCommand, character, position, start) => {
			_isSayCommand = isSayCommand;
			_character = character;
			_position = position;
			_start = start;
		};

		this.ShowText = async (text) => {
			if (_rejectHideDialog) {
				_rejectHideDialog();
				_rejectHideDialog = null;
			}

			if (_isSayCommand) {
				let position;

				if (_character && _character.id === undefined) {
					_character = new CHARACTER.Character(_character);
				}

				if (typeof _position === "number" && _position >= 0) {
					position = _position;
				}
				else if (typeof _character === "number") {
					position = _character;
				}
				else {
					position = characterToPosition(_character);
				}

				switch (position) {
					case GUI.Position.Left:
					case GUI.Position.Right:
						await this.HideDialog(-1);
						break;
					default:
						await Promise.all([
							this.HideDialog(GUI.Position.Left),
							this.HideDialog(GUI.Position.Right),
						]);
						break;
				}

				const start = _start;
				_start = 0;

				if (!GAME_MANAGER.instance.hasLoot) {
					return new Promise(resolve => new DIALOG.Dialog(text, _dialogList[position + 1], _isSayCommand, start, resolve));
				}
				new DIALOG.Dialog(text, _dialogList[position + 1], _isSayCommand, start);
			}
			else {
				await Promise.all([
					this.HideDialog(GUI.Position.Left),
					this.HideDialog(GUI.Position.Right),
				]);
				if (!GAME_MANAGER.instance.hasLoot) {
					return new Promise(resolve => new DIALOG.Dialog(text, _dialogList[0], false, 0, resolve));
				}
				new DIALOG.Dialog(text, _dialogList[0], false, 0);
			}
		};

		this.HideDialog = async (position) => {
			let elm;
			if (position !== undefined) {
				if (!_dialogList[position + 1].classList.contains("inactive")) {
					elm = _dialogList[position + 1];
				}
				_dialogList[position + 1].classList.add("inactive");
			}
			else {
				for (let i = 0; i < _dialogList.length; i++) {
					if (!_dialogList[i].classList.contains("inactive")) {
						elm = _dialogList[i];
					}
					_dialogList[i].classList.add("inactive");
				}
			}
			LOCAL_CHAT.SetTypingIndicator(false, position);
			if (elm) {
				return new Promise(async (resolve, reject) => {
					_rejectHideDialog = reject;
					await transitionEnded(elm);
					elm.classList.remove("chat");
					_rejectHideDialog = null;
					resolve();
				});
			}
		};

		this.OpenLocalChat = async () => Promise.all([
			LOCAL_CHAT.player.Show(),
			LOCAL_CHAT.opponent.Show(),
		]);

		this.HideStatus = () => {
			this.HidePlayerStatus();
			this.HideOpponentStatus();
		};

		this.HidePlayerStatus = () => {
			const menu = MENU.Inspect.menu;
			if (_playerStatus && (!menu || !menu.classList.contains("player"))) {
				_playerStatus.Hide();
			}
		};

		this.HideOpponentStatus = () => _opponentStatus && _opponentStatus.Hide();

		this.ShowStatus = () => {
			this.ShowPlayerStatus();
			this.ShowOpponentStatus();
		};

		this.ShowPlayerStatus = () => _playerStatus && _playerStatus.Show();

		this.ShowOpponentStatus = () => _opponentStatus && _opponentStatus.Show();

		this.SetPlayerStatus = (status) => {
			if (!_playerStatus) {
				_playerStatus = new STATUS.Status(document.getElementById("status").getElementsByTagName("li")[0], status, () => MENU.Myself.Toggle(GUI.Position.Left));
			}
			else {
				_playerStatus.SetStatus(status);
			}
		}

		this.SetOpponentStatus = (status) => {
			if (!_opponentStatus) {
				_opponentStatus = new STATUS.Status(document.getElementById("status").getElementsByTagName("li")[1], status, () => MENU.Inspect.Toggle(GUI.Position.Right, LOCATION.instance.opponent ? LOCATION.instance.opponent.token : null));
			}
			else {
				_opponentStatus.SetStatus(status);
			}
		}

		this.Acquired = async (label, category) => {
			const transitionTime = 600;
			const displayTime = 1200;

			const div = document.createElement("div");
			const subdiv = document.createElement("div");

			div.appendChild(subdiv);

			clearHTML(_acquisition);
			_acquisition.appendChild(div);

			div.className = "inactive" + (category ? ` ${category}` : "");
			subdiv.className = "inactive";
			subdiv.innerHTML = "-";

			await waitForFrames();

			div.classList.remove("inactive");

			await new Promise(async (resolve) => {
				div.onclick = () => resolve();
				await wait(transitionTime);

				if (category) {
					clearHTML(subdiv);

					let span;
					switch (category) {
						case "spell":
							span = document.createElement("span");
							span.className = "token";
							subdiv.appendChild(span);
							span = document.createElement("span");
							span.innerHTML = "New Spell";
							subdiv.appendChild(span);
							break;
						case "card":
							span = document.createElement("span");
							span.className = "token";
							subdiv.appendChild(span);
							span = document.createElement("span");
							span.innerHTML = "New Fortune Card";
							subdiv.appendChild(span);
							break;
						case "action":
							span = document.createElement("span");
							span.className = "token";
							subdiv.appendChild(span);
							span = document.createElement("span");
							span.innerHTML = "Acquired Action";
							subdiv.appendChild(span);
							break;
						case "spell":
							span = document.createElement("span");
							span.className = "token";
							subdiv.appendChild(span);
							span = document.createElement("span");
							span.innerHTML = "New Spell";
							subdiv.appendChild(span);
							break;
						case "skill":
							span = document.createElement("span");
							span.innerHTML = "New Skill";
							subdiv.appendChild(span);
							break;
						case "crafting":
							span = document.createElement("span");
							span.innerHTML = "New Crafting Recipe";
							subdiv.appendChild(span);
							break;
					}

					subdiv.classList.remove("inactive");
					await transitionEnded(subdiv);
					await wait(displayTime);
					subdiv.classList.add("inactive");
					await transitionEnded(subdiv);
				}

				clearHTML(subdiv);
				subdiv.innerHTML = label;
				subdiv.classList.remove("inactive");
				await transitionEnded(subdiv);
				await wait(displayTime);
				div.classList.add("close");
				await transitionEnded(div);
				resolve();
			});

			if (div.parentNode) {
				div.parentNode.removeChild(div);
			}
		};

		this.OverheadStatus = async (label, className, position) => {
			const div = document.createElement("div");

			_overheadStatus.appendChild(div);

			if (typeof label === "string" && label.length > 0) {
				const span = document.createElement("span");
				span.innerHTML = label;
				div.appendChild(span);
			}

			div.className = "inactive" + (className ? ` ${className}` : "");

			await waitForFrames();

			div.classList.remove("inactive");

			await wait(2000);

			div.classList.add("close");

			await transitionEnded(div);

			if (div.parentNode) {
				div.parentNode.removeChild(div);
			}
		};

		this.UpdateToken = async (counter, tokens, max, difference) => {
			const elm = _tokens.getElementsByClassName(counter)[0];
			const list = elm.getElementsByTagName("span");
			switch (counter) {
				case "spells":
				case "actions":
					list[0].style.display = tokens >= 10 ? "" : "none";
					list[0].innerHTML = tokens >= 10 ? Math.floor(tokens / 10) : "";
					list[1].innerHTML = tokens % 10;
					list[3].style.display = max >= 10 ? "" : "none";
					list[3].innerHTML = max >= 10 ? Math.floor(max / 10) : "";
					list[4].innerHTML = max % 10;
					break;
				case "cards":
					list[0].innerHTML = tokens;
					break;
			}
			if (difference > 0) {
				const div = document.createElement("div");
				div.innerHTML = `+${difference}`;
				div.className = "bonus inactive";

				const prevBonus = elm.getElementsByClassName("bonus")[0];
				if (prevBonus) {
					prevBonus.parentNode.removeChild(prevBonus);
				}

				elm.prepend(div);

				await waitForFrames();

				div.classList.remove("inactive");

				await wait(2000);

				div.classList.add("close");

				await transitionEnded(div);

				if (div.parentNode) {
					div.parentNode.removeChild(div);
				}
			}
		};

		this.DisplayMessage = async function (message) {
			message = message.trim();

			while (message.endsWith(".")) {
				message = message.substr(0, message.length - 1);
			}

			const div = document.createElement("div");
			const subdiv = document.createElement("div");

			subdiv.innerHTML = message;

			div.classList.add("inactive");
			div.appendChild(subdiv);
			_errorMessages.appendChild(div);

			await waitForFrames();
			div.classList.remove("inactive");

			await wait(GUI.ERROR_MESSAGE_DURATION_MILLIS);

			div.classList.add("inactive");
			await transitionEnded(div);

			div.parentNode && div.parentNode.removeChild(div);
		}

		const flashBackground = async () => {
			_background.classList.add("flash");
			await transitionEnded(_background);

			_background.classList.add("out");
			await transitionEnded(_background);

			_background.classList.remove("flash");
			_background.classList.remove("out");
		}

		const characterToPosition = (character) => {
			if (character) {
				const id = CHARACTER.GetId(character);
				for (let i = 0; i < _displayedCharacters.length; i++) {
					if (_displayedCharacters[i] && _displayedCharacters[i].id === id) {
						return i;
					}
				}
			}
			return -1;
		};

		const setCharacterHoverImage = (img, position) => {
			_characterHoverImages[position] = img;
			this.Resize();
		}

		this.Resize = () => {
			for (let i = 0; i < _characterHoverImages.length; i++) {

				if (_characterHoverImages[i]) {
					let context = _characterHoverCanvas[i].getContext("2d");
					let width = _background.offsetWidth * 0.4;
					let height = _background.offsetHeight * 0.9;
					let hoverImage = _characterHoverImages[i];

					_characterHoverCanvas[i].width = width;
					_characterHoverCanvas[i].height = height;

					let scaledWidth = height / hoverImage.height * hoverImage.width;
					context.drawImage(hoverImage, (width - scaledWidth) / 2, 0, scaledWidth, height);

					_characterHoverImageData[i] = context.getImageData(0, 0, width, height);
				}
				else {
					_characterHoverImageData[i] = null;
				}
			}
			for (let i = 0; i < COUNTDOWN.list.length; i++) {
				COUNTDOWN.list[i].Resize();
			}
		};

		this.SetUnreadMessages = (i) => {
			const elm = document.getElementById("frame_top_right").getElementsByClassName("icons")[0].getElementsByClassName("unread_messages")[0];
			elm.getElementsByTagName("span")[0].innerHTML = i;
			i == 0 ? elm.classList.add("inactive") : elm.classList.remove("inactive");
		};

		this.SetFriendRequests = (i) => {
			const elm = document.getElementById("frame_top_right").getElementsByClassName("icons")[0].getElementsByClassName("friend_requests")[0];
			elm.getElementsByTagName("span")[0].innerHTML = i;
			i == 0 ? elm.classList.add("inactive") : elm.classList.remove("inactive");
		};

		this.SetOnlineFriends = (i) => {
			const elm = document.getElementById("frame_top_right").getElementsByClassName("icons")[0].getElementsByClassName("online_friends")[0];
			elm.getElementsByTagName("span")[0].innerHTML = i;
			i == 0 ? elm.classList.add("inactive") : elm.classList.remove("inactive");
		};

		this.SetCraftingBench = (b) => {
			const elm = document.getElementById("frame_top_right").getElementsByClassName("icons")[0].getElementsByClassName("crafting")[0];
			b ? elm.classList.remove("inactive") : elm.classList.add("inactive");
		};

		this.SetRoleplayBroadcasting = (b) => {
			const elm = document.getElementById("frame_top_right").getElementsByClassName("icons")[0].getElementsByClassName("roleplay_broadcasting")[0];
			b ? elm.classList.remove("inactive") : elm.classList.add("inactive");
		};

		this.SetDaytime = (i) => CALENDAR.instance.SetDaytime(i);

		this.Update = (time) => {
			_deltaTime = time - _prevTime;
			_now = Date.now();

			if (_deltaTime > 0) {
				_backgroundOffset += (_backgroundMouseOffset - _backgroundOffset) / _deltaTime;
			}

			if (_parallaxEnabled) {
				if (_backgroundImage) {
					_backgroundWrapper.style.transform = `translateX(${_background.offsetWidth * - 0.02 * _backgroundOffset}px)`;
				}
				_frame.style.transform = `translateX(${_background.offsetWidth * - 0.01 * _backgroundOffset}px)`;
			}

			if (_options.firstChild) {
				_options.style.top = `${50 - (_options.firstChild.offsetHeight / container.offsetHeight) / 2 * 100}%`;
			}

			_actionHubs[0] && _actionHubs[0].Update(_now);
			_actionHubs[1] && _actionHubs[1].Update(_now);

			if (_animator && _animatorProgress < 1) {
				_animatorProgress = clamp01(_animatorProgress + _deltaTime / _animatorDuration);
				_animator.DrawFrame(easeInOutBack(_animatorProgress), _animatorCanvas, _animatorSize.width, _animatorSize.height);
			}

			VOICE_CHAT.instance.Update(_now);
			CALENDAR.instance.Update();
			MENU.Messages.Update();
			MENU.Spells.Update();
			LOCAL_CHAT.Update(time);

			for (_i = 0; _i < COUNTDOWN.list.length; _i++) {
				COUNTDOWN.list[_i].Update(_now);
			}

			_prevTime = time;
		};
	};

	function easeInOutElastic(x) {
		const c5 = (2 * Math.PI) / 4.5;
		return x === 0
			? 0
			: x === 1
				? 1
				: x < 0.5
					? -(Math.pow(2, 20 * x - 10) * Math.sin((20 * x - 11.125) * c5)) / 2
					: (Math.pow(2, -20 * x + 10) * Math.sin((20 * x - 11.125) * c5)) / 2 + 1;
	}

	function easeInOutExpo(x) {
		return x === 0
			? 0
			: x === 1
				? 1
				: x < 0.5 ? Math.pow(2, 20 * x - 10) / 2
					: (2 - Math.pow(2, -20 * x + 10)) / 2;
	}

	function easeInOutBack(x) {
		const c1 = 1.70158;
		const c2 = c1 * 1.525;
		return x < 0.5
			? (Math.pow(2 * x, 2) * ((c2 + 1) * 2 * x - c2)) / 2
			: (Math.pow(2 * x - 2, 2) * ((c2 + 1) * (x * 2 - 2) + c2) + 2) / 2;
	}

	function easeOutBounce(x) {
		const n1 = 7.5625;
		const d1 = 2.75;
		if (x < 1 / d1) {
			return n1 * x * x;
		}
		if (x < 2 / d1) {
			return n1 * (x -= 1.5 / d1) * x + 0.75;
		}
		if (x < 2.5 / d1) {
			return n1 * (x -= 2.25 / d1) * x + 0.9375;
		}
		return n1 * (x -= 2.625 / d1) * x + 0.984375;
	}
})();
