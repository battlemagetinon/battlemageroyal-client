FILTER = {};

FILTER.Filter = function (_elm, _id, _placeholder, _hideUnselected = false, _childClass = null) {
	const _input = document.createElement("input");

	let _list;

	let _search = "";
	let _prevSearch = "";

	_input.type = "text";
	_input.id = _id;
	_input.name = _id;

	if (_placeholder) {
		_input.placeholder = _placeholder;
	}

	_input.onchange = () => {
		_search = _input.value.toLowerCase().trim();
		if (_prevSearch !== _search) {
			_prevSearch = _search;
			filterList();
		}
	};

	Object.defineProperties(this, {
		'input': {
			get: () => _input
		},
	});

	_input.onpaste = () => _input.onchange();
	_input.oninput = () => _input.onchange();
	_input.onkeydown = () => _input.onchange();

	const filterList = function () {
		let next;

		const hideUnselected = _hideUnselected && (_hideUnselected === true || typeof _hideUnselected === "function" && _hideUnselected());
		const list = [[], []];

		for (let i = 0; i < _list.length; i++) {
			const match = isMatch(_list[i]);

			if (_childClass && !_list[i].classList.contains(_childClass)) {
				const sublist = [[_list[i]], []];
				const prev = i;
				let matchAny = match;

				while ((next = i + 1) < _list.length && _list[next].classList.contains(_childClass)) {
					let b = match || isMatch(_list[next]);
					matchAny = matchAny || b;
					display(_list[next], b, hideUnselected);
					sublist[b ? 0 : 1].push(_list[next]);
					i++;
				}

				list[matchAny ? 0 : 1].push(prev != i ? sublist : _list[i]);
				display(_list[prev], matchAny, hideUnselected);
			}
			else {
				list[match ? 0 : 1].push(_list[i]);
				display(_list[i], match, hideUnselected);
			}
		};

		appendList(list);
	}

	const appendList = (list) => {
		for (let i = 0; i < list.length; i++) {
			for (let j = 0; j < list[i].length; j++) {
				if (Array.isArray(list[i][j])) {
					appendList(list[i][j]);
				}
				else if (list[i][j].parentNode) {
					list[i][j].parentNode.appendChild(list[i][j]);
				}
			}
		}
	};

	const display = (elm, match, hideUnselected) => {
		if (match) {
			elm.style.opacity = _search.length === 0 ? "" : "1";
			elm.style.display = "";
			elm.onmouseout = null;
		}
		else if (!hideUnselected) {
			elm.style.opacity = "0.25";
		}
		else {
			elm.style.display = "none";
		}
	};

	const isMatch = (elm) => {
		if (!elm.classList.contains("inactive")) {
			const nodes = elm.childNodes;
			for (let i = 0; i < nodes.length; i++) {
				if (nodes[i].textContent.toLowerCase().indexOf(_search) >= 0) {
					return true;
				}
			}
		}
		return false;
	};

	this.Update = () => {
		_list = toArray(_elm.getElementsByTagName(_elm.tagName.toLowerCase() == "table" ? "tr" : "li"));
		filterList();
	}

	this.Clear = () => {
		_input.value = "";
		_search = "";
		_prevSearch = "";
		filterList();
	}

	this.Update();
}
