(() => {
	DROPDOWN = {
	};

	let _instance;
	Object.defineProperty(DROPDOWN, "instance", {
		get: () => {
			if (!_instance) {
				_instance = new DROPDOWN.Dropdown();
			}
			return _instance;
		}
	});
})();

DROPDOWN.Dropdown = function () {
	const _dropdown = document.getElementById("dropdown");
	let _trigger;

	this.Open = async function (e, list, position) {
		this.Close();

		clearHTML(_dropdown);

		for (let i = 0; i < list.length; i++) {
			if (!list[i]) {
				continue;
			}
			const div = document.createElement("div");
			const span = document.createElement("span");
			span.innerHTML = list[i].label;

			div.appendChild(span);
			div.className = nameToRef(list[i].label);
			div.onclick = list[i].onclick;

			_dropdown.appendChild(div);
		}

		_trigger = e.currentTarget || e.target;
		_trigger.style.pointerEvents = "none";

		await waitForFrame();

		_dropdown.classList.remove("inactive");

		if (position) {
			_dropdown.style.left = position.left;
			_dropdown.style.top = position.top;
		}
		else {
			const rect = container.getBoundingClientRect();
			const x = e.pageX !== undefined ? e.pageX : e.targetTouches[0].pageX;
			const y = e.pageY !== undefined ? e.pageY : e.targetTouches[0].pageY;

			if (x - rect.x + _dropdown.offsetWidth > rect.width) {
				_dropdown.style.left = "";
				_dropdown.style.right = 0;
			}
			else {
				_dropdown.style.left = `${(x - rect.x) / rect.width * 100}%`;
				_dropdown.style.right = "";
			}

			if (y - rect.y + _dropdown.offsetHeight > rect.height) {
				_dropdown.style.top = "";
				_dropdown.style.bottom = 0;
			}
			else {
				_dropdown.style.top = `${(y - rect.y) / rect.height * 100}%`;
				_dropdown.style.bottom = "";
			}
		}
	}

	this.Close = function () {
		_dropdown.classList.add("inactive");
		_dropdown.style.right = "";
		if (_trigger) {
			_trigger.style.pointerEvents = "";
		}
	};

	container.addEventListener("click", () => !_dropdown.classList.contains("inactive") ? DROPDOWN.instance.Close() : null);
}