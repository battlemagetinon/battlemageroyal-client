(() => {
	CALENDAR = {
	};

	let _instance;

	Object.defineProperty(CALENDAR, "instance", {
		get: () => {
			if (!_instance) {
				_instance = new CALENDAR.Calendar();
			}
			return _instance;
		}
	});

	CALENDAR.Calendar = function () {
		const _calendar = document.getElementById("calendar");
		const _daytime = document.getElementById("daytime");
		const _location = document.getElementById("location");
		const _turnTimer = _location.getElementsByClassName("counter")[0];
		const _list = _turnTimer.getElementsByTagName("span");

		let _turnEnd = 0;

		let _currentDaytime = 0;
		let _prevMonth;
		let _prevDate;
		let _prevWeekday;
		let _prevDaytime;

		let _timeRemaining;
		let _m;
		let _s;
		let _i;

		removeTextNodes(_calendar);

		Object.defineProperties(this, {
			'daytime': {
				get: () => _currentDaytime
			},
			'morning': {
				get: () => _currentDaytime == 0
			},
			'afternoon': {
				get: () => _currentDaytime == 1
			},
			'evening': {
				get: () => _currentDaytime == 2
			},
			'night': {
				get: () => _currentDaytime == 3
			},
		});

		this.SetTime = async (month, date, weekday, daytime, remainingTime, holiday) => {
			_turnEnd = Date.now() + remainingTime * 1000;
			_currentDaytime = daytime;
			daytime = this.ToDaytime(daytime).toLowerCase();

			_location.classList.add("visible");

			if (daytime != _prevDaytime) {
				GUI.instance.UpdateLocationBackground();

				_prevDaytime = daytime;

				if (!_daytime.classList.contains("inactive")) {
					_daytime.classList.add("inactive");
					transitionEnded(_daytime).then(() => _daytime.className = _prevDaytime);
				}
				else {
					_daytime.className = _prevDaytime;
				}
			}

			if (_prevMonth == month && _prevDate == date && _prevWeekday == weekday) {
				return;
			}

			const div = document.createElement("div");
			const monthElm = document.createElement("div");
			const dateElm = document.createElement("div");
			const weekdayElm = document.createElement("div");
			const span = document.createElement("span");
			const firstChild = _calendar.firstChild;

			monthElm.className = "month";
			dateElm.className = "date";
			weekdayElm.className = "weekday";

			monthElm.innerHTML = this.ToMonth(month);
			dateElm.innerHTML = date;
			span.innerHTML = this.ToWeekday(weekday);

			weekdayElm.appendChild(span);

			div.appendChild(monthElm);
			div.appendChild(dateElm);
			div.appendChild(weekdayElm);

			if (holiday) {
				weekdayElm.classList.add("holiday");
			}

			if (firstChild) {
				firstChild.classList.add("inactive");
				_calendar.insertBefore(div, firstChild);
				await transitionEnded(firstChild);
				_calendar.removeChild(firstChild);
			}
			else {
				_calendar.appendChild(div);
			}

			_prevMonth = month;
			_prevDate = date;
			_prevWeekday = weekday;
		};

		this.SetDaytime = async (daytime) => {
			if (_turnEnd > 0) {
				return;
			}

			_currentDaytime = daytime;

			GUI.instance.UpdateLocationBackground();

			daytime = this.ToDaytime(daytime);

			if (daytime) {
				daytime = daytime.toLowerCase();

				if (!_daytime.classList.contains("inactive")) {
					_daytime.classList.add("inactive");
					await transitionEnded(_daytime);
					_daytime.className = daytime;
					await transitionEnded(_daytime);
				}
			}
		};

		this.ToMonth = (i) => {
			switch (i) {
				case 0:
					return "January";
				case 1:
					return "February";
				case 2:
					return "March";
				case 3:
					return "April";
				case 4:
					return "May";
				case 5:
					return "June";
				case 6:
					return "July";
				case 7:
					return "August";
				case 8:
					return "September";
				case 9:
					return "October";
				case 10:
					return "November";
				case 11:
					return "December";
				default:
					return null;
			}
		};

		this.ToWeekday = (i) => {
			switch (i) {
				case 0:
					return "Sunday";
				case 1:
					return "Monday";
				case 2:
					return "Tuesday";
				case 3:
					return "Wednesday";
				case 4:
					return "Thursday";
				case 5:
					return "Friday";
				case 6:
					return "Saturday";
				default:
					return null;
			}
		};

		this.ToDaytime = (i) => {
			switch (i) {
				case 0:
					return "Morning";
				case 1:
					return "Afternoon";
				case 2:
					return "Evening";
				case 3:
					return "Night";
				default:
					return null;
			}
		};

		this.Update = () => {
			if (_turnEnd == 0) {
				return;
			}

			_timeRemaining = _turnEnd - Date.now();

			if (_timeRemaining > 0) {
				_m = Math.floor(_timeRemaining / 60000);
				_s = Math.floor(_timeRemaining / 1000) % 60;
				_i = Math.floor(_s / 10);
				_s = _s % 10;
			}
			else {
				_m = 0;
				_i = 0;
				_s = 0;
			}

			if (_list[0].innerHTML != _m) {
				_list[0].innerHTML = _m;
			}
			if (_list[2].innerHTML != _i) {
				_list[2].innerHTML = _i;
			}
			if (_list[3].innerHTML != _s) {
				_list[3].innerHTML = _s;
			}

			TITLE.time = `${_m}:${_i}${_s}`;
		}
	};
})();
