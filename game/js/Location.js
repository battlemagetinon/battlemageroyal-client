(() => {
	LOCATION = {
		current: null,
		GetLocationsByType: (type) => {
			let locations = [];
			const discoveredLocations = GAME_MANAGER.instance.discoveredLocations;
			for (let name in discoveredLocations) {
				if (discoveredLocations[name].type == type) {
					locations.push(name);
				}
			}
			return locations;
		},
		GetBackgroundURL: (location, daytime, small) => {
			const backgroundURL = location.background[Math.min(daytime, location.background.length - 1)];
			return backgroundURL && small ? backgroundURL.replace("assets", "assets/small") : backgroundURL;
		},
		LocationToTitle: (name) => {
			const location = GAME_MANAGER.instance.discoveredLocations[name];
			return location && location.title;
		},
	};

	let _instance;

	Object.defineProperties(LOCATION, {
		'instance': {
			get: () => {
				if (!_instance) {
					_instance = new LOCATION.Location();
				}
				return _instance;
			}
		},
	});
})();

LOCATION.Location = function () {
	const _elm = document.getElementById("location");
	const _obj = {};

	let _pendingLeave;

	Object.defineProperties(this, {
		'id': {
			get: () => _obj.id
		},
		'name': {
			get: () => _obj || _obj.name
		},
		'title': {
			get: () => _obj.title || _obj.name
		},
		'location': {
			get: () => _elm
		},
		'background': {
			get: () => _obj.background || []
		},
		'type': {
			get: () => _obj.type
		},
		'player': {
			get: () => _obj.character,
		},
		'opponent': {
			get: () => !GAME_MANAGER.instance.InScenario() ? _obj.opponent : null
		},
		'npc': {
			get: () => !GAME_MANAGER.instance.InScenario() ? _obj.npc : null
		},
		'faculty': {
			get: () => _obj.faculty === true
		},
		'resting': {
			get: () => _obj.resting === true
		},
		'explorable': {
			get: () => _obj.explorable === true
		},
		'searchable': {
			get: () => _obj.searchable === true
		},
		'specialActions': {
			get: () => _obj.opponent ? _obj.specialActions : null
		},
		'publicActions': {
			get: () => _obj.actions
		},
	});

	this.GetBackgroundURL = (daytime, small) => LOCATION.GetBackgroundURL(this, daytime, small);

	this.Sync = (location) => {
		if (location.id && location.id !== _obj.id) {
			if (location.opponent === undefined) {
				location.opponent = null;
			}
			if (location.npc === undefined) {
				location.npc = null;
			}
			if (location.specialActions === undefined) {
				location.specialActions = null;
			}
			if (location.actions === undefined) {
				location.actions = null;
			}
			_pendingLeave = false;
		}

		if (location.opponent) {
			if (location.opponent.character !== undefined) {
				const character = location.opponent.character;
				delete location.opponent.character;
				location.opponent = Object.assign({}, character, location.opponent);
			}
			if (location.opponent.countdowns !== undefined) {
				delete location.opponent.countdowns;
			}
			if (location.opponent.status !== undefined) {
				const status = location.opponent.status;
				delete location.opponent.status;
				if (_obj.specialActions && status.mind > 0 && status.body > 0) {
					location.specialActions = null;
				}
				GUI.instance.SetOpponentStatus(status);
			}
		}

		const prev = Object.assign({}, _obj);
		const changed = {};

		Object.assign(_obj, location);

		for (let prop in location) {
			if (location[prop] !== prev[prop]) {
				changed[prop] = true;
			}
		}

		if (location.nextLocation !== undefined) {
			if (location.nextLocation === true) {
				GUI.instance.LocationReady();
			}
			else {
				if (LOCATION.instance.npc) {
					VOICE_CHAT.instance.Display(CHARACTER.GetFarewell(LOCATION.instance.npc), GUI.Position.Right);
				}
				GUI.instance.LeaveLocation(location.nextLocation);
				_pendingLeave = true;
			}
		}

		if (changed.character) {
			MENU.Myself.Refresh();
		}

		if (changed.npc && prev.npc && _obj.npc) {
			_obj.npc = Object.assign(prev.npc, _obj.npc);
		}

		if (changed.opponent) {
			if ((prev.opponent != null) != (_obj.opponent != null)) {
				if (_obj.opponent) {
					NOTIFICATION.instance.Encounter(_obj.opponent.names, _obj.opponent.username);
				}
				else {
					NOTIFICATION.instance.EncounterEnded(prev.opponent.names, prev.opponent.username);
					GUI.instance.HideOpponentStatus();
					MENU.Spells.Close();
				}
			}

			if (prev.opponent && _obj.opponent) {
				_obj.opponent = Object.assign(prev.opponent, _obj.opponent);
				if (prev.opponent.names != _obj.opponent.names) {
					MENU.Spells.Refresh();
				}
			}

			if (!_obj.opponent) {
				COUNTDOWN.ClearByTarget("opponent");
			}
		}

		if (!_pendingLeave && !GAME_MANAGER.instance.InScenario() && Object.keys(changed).length > 0) {
			GUI.instance.UpdateLocation(this);
		}
	};
};
