<?php

require '../../../../api/config.php';

$url = $_GET['url'];
$index = strpos($url, '/');

if ($index !== false) {
    $color_ref = substr($url, 0, $index);
    $item_path = substr($url, $index + 1);
    $source_path = realpath("${_SERVER['DOCUMENT_ROOT']}/game/assets/items/$item_path");

    if (file_exists($source_path)) {
        $type = mime_content_type($source_path);

        $path = "${_SERVER['DOCUMENT_ROOT']}/temp/game/assets/items/dyed/$color_ref/$item_path";
        $index = strrpos($path, '/', -1);
        $dir = substr($path, 0, $index);

        if (file_exists($path) && filemtime($path) > filemtime($source_path)) {
            header("Content-Type: $type");
            readfile($path);
            die();
        }

        if ($type == 'image/png' && strpos($source_path, '/var/www/battlemageroyal.com/test/game/assets/items/dyed') !== 0) {
            $asset_path = "items/$item_path";

            foreach (Item::get_item_details() as $entry) {
                if ($entry['image_url'] === $asset_path || isset($entry['worn_image_url']) && $entry['worn_image_url'] === $asset_path) {
                    $details = $entry;
                }
            }

            if ($details && Item::base_item_has_color($details, $color_ref)) {
                list($width, $height) = getimagesize($source_path);
                $source = imagecreatefrompng($source_path);

                $image = imagecreatetruecolor($width, $height);
                imagesavealpha($image, true);
                imagealphablending($image, false);

                switch ($details['image_clustering'] ? strtolower($details['image_clustering']) : null) {
                    case 'kmeans':
                        $k = $details['kmeans_clusters'] ? $details['kmeans_clusters'] : 2;
                        break;
                    default:
                        $k = 0;
                        break;
                }

                switch ($details['image_coloring'] ? strtolower($details['image_coloring']) : null) {
                    case 'all':
                    default:
                        $coloring = ImageProcessing::All;
                        break;
                    case 'primary':
                        $coloring = ImageProcessing::Primary;
                        break;
                    case 'base':
                        $coloring = ImageProcessing::Base;
                        break;
                    case 'white':
                        $coloring = ImageProcessing::White;
                        break;
                    case 'dark':
                    case 'black':
                        $brightness_mod = $details['image_brightness_modifier'] ?: 1;
                        $coloring = ImageProcessing::Black;
                        break;
                }

                $postprocess = $details['image_postprocessing'] ? strtolower($details['image_postprocessing']) : null;
                $contrast_clamp = $details['image_contrast_clamp'] ?: 0;
                $threshold = $details['image_threshold'] ?: 0;
                $hue_rotate = isset($details['image_hue_rotate']) ? $details['image_hue_rotate'] : false;
                $scale_brightness = isset($details['image_scale_brightness']) ? $details['image_scale_brightness'] : true;
                $brightness_scale_threshold = $details['image_brightness_threshold'] ?: 1;
                $base_color = $details['image_base_color'] ? sscanf($details['image_base_color'], "#%02x%02x%02x") : null;
                $format = isset($details['image_clustering_format']) && strtolower($details['image_clustering_format']) === 'hsv' ? ImageProcessing::HSV : ImageProcessing::RGB;

                if (ImageProcessing::process($source, $image, $width, $height, $color_ref, $k, $base_color, $format, $coloring, $threshold, $scale_brightness !== false, $brightness_scale_threshold, $postprocess, $contrast_clamp, $brightness_mod, $hue_rotate)) {
                    if (!is_dir($dir)) {
                        mkdir($dir, 0700, true);
                    }
                    header("Content-Type: $type");
                    imagepng($image, $path);
                    imagepng($image);
                    die();
                }
            }
        }
    }
}

header("HTTP/1.1 500 Internal Server Error");
