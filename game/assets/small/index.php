<?php

define("MAX_SIZE", 256);

$url = $_GET['url'];
$source_path = realpath("${_SERVER['DOCUMENT_ROOT']}/game/assets/$url");

if (file_exists($source_path)) {
    $type = mime_content_type($source_path);

    $index = strrpos($url, '/', -1);
    $filename = substr($url, $index + 1);
    $dir = "${_SERVER['DOCUMENT_ROOT']}/temp/game/assets/small/" . substr($url, 0, $index);
    $path = "$dir/$filename";

    if (file_exists($path) && filemtime($path) > filemtime($source_path)) {
        header("Content-Type: $type");
        readfile($path);
        die();
    }

    if (strpos($source_path, '/var/www/battlemageroyal.com/test/game/assets/small') !== 0 &&
        strpos($source_path, '/var/www/battlemageroyal.com/test/game/assets/items/dyed') !== 0
    ) {
        switch ($type) {
            case 'image/jpeg':
            case 'image/png':
                list($width, $height) = getimagesize($source_path);
                $scale = MAX_SIZE / ($width > $height ? $width : $height);

                if ($scale >= 1) {
                    header("Content-Type: $type");
                    readfile($source_path);
                    die();
                }

                $new_width = $width * $scale;
                $new_height = $height * $scale;

                $image = imagecreatetruecolor($new_width, $new_height);
                $source = $type === 'image/png' ? imagecreatefrompng($source_path) : imagecreatefromjpeg($source_path);

                if (imagecopyresampled($image, $source, 0, 0, 0, 0, $new_width, $new_height, $width, $height)) {
                    if (!is_dir($dir)) {
                        mkdir($dir, 0700, true);
                    }

                    header("Content-Type: $type");
                    if ($type === 'image/png') {
                        imagepng($image, $path);
                        imagepng($image);
                    } else {
                        imagejpeg($image, $path);
                        imagejpeg($image);
                    }
                    die();
                }
                break;
        }
    }
}

header("HTTP/1.1 500 Internal Server Error");
