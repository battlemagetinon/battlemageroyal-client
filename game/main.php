<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>Battle Mage Royal</title>
	<meta name="description" content="The game world of Battle Mage Royal">
	<meta name="author" content="">
	<meta name="keywords" content="">
	<meta name="viewport" content="width=device-width">
	<meta name="robots" content="noindex, nofollow">

	<?php require '../internals/favicon.php';?>

	<link href="https://fonts.googleapis.com/css?family=Alex+Brush|Merienda|Ubuntu+Condensed" rel="stylesheet">

	<link rel="stylesheet" href="/assets/css/fontawesome.css">
	<link rel="stylesheet" href="/assets/css/ionicons.css">
	<link rel="stylesheet" href="/assets/css/normalize.css">
	<link rel="stylesheet" href="/assets/css/norskode.css">
	<link rel="stylesheet" href="/assets/css/impact.css">

	<?php
function format_dir($dir)
{
    return preg_replace('{\\.([^./]+)$}', "." . filemtime($dir) . ".\$1", $dir);
}

foreach (glob("css/*.css") as $css) {
    echo '<link rel="stylesheet" href="' . format_dir($css) . "\">\n\t";
}

foreach (glob("js/*.js") as $js) {
    echo '<script defer src="' . format_dir($js) . "\"></script>\n\t";
}

foreach (glob("js/Menu/*.js") as $js) {
    echo '<script defer src="' . format_dir($js) . "\"></script>\n\t";
}
?>

	<script type="text/javascript">
		var container;
		var startupError = false;
		var openCV

		(function() {
			try {
				eval('let a = async () => {}');
			}
			catch (e) {
				if (e instanceof SyntaxError) {
					startupError = true;
				}
			}
		})();

		window.addEventListener("beforeunload", function(e) {
			window.name = null;
		});

		window.name = "BattleMageRoyal:GameClient";

		document.addEventListener("DOMContentLoaded", function(e) {
			container = document.getElementById("container");

			if (startupError) {
				var alert = document.createElement("div");
				alert.className = "alert";

				var wrapper = document.createElement("div");
				wrapper.className = "wrapper";

				var div = document.createElement("div");
				div.innerHTML = "I'm sorry, but the game is unable to run in your browser!<br/>Please upgrade your browser or use another device to play the game";
				wrapper.appendChild(div);

				alert.appendChild(wrapper);
				container.appendChild(alert);
				return;
			}

			var idToken = <?php echo isset($_GET['id_token']) ? json_encode($_GET['id_token']) : 'null' ?>;
			GAME_MANAGER.instance.Connect(idToken);

			var tokens = document.getElementById("tokens").getElementsByTagName("div");
			tokens[0].onmouseenter = function(e){ TOOLTIP.instance.Show(e, TOOLTIP.GetContent(TOOLTIP.Token.Spell)) };
			tokens[1].onmouseenter = function(e){ TOOLTIP.instance.Show(e, TOOLTIP.GetContent(TOOLTIP.Token.Action)) };
			tokens[2].onmouseenter = function(e){ TOOLTIP.instance.Show(e, TOOLTIP.GetContent(TOOLTIP.Token.Card)) };
			tokens[0].ontouchstart = tokens[0].onmouseenter;
			tokens[1].ontouchstart = tokens[1].onmouseenter;
			tokens[2].ontouchstart = tokens[2].onmouseenter;

			document.body.onresize = resize;

			resize();
			update(0);

			var textSize = SETTINGS.Get("text_size_chat");
			if (textSize) {
				document.getElementById("dialog").classList.add(textSize);
			}

			var chatTimestamp = SETTINGS.Get("chat_timestamp");
			if (chatTimestamp) {
				document.getElementById("dialog").classList.add("timestamped");
			}

			setInterval(TITLE.Update, 1000);

			switch (window.location.hash.substr(1)) {
				case "yourself":
					MENU.Inspect.Open(GUI.Position.Left, true);
					break;
				case "inventory":
					MENU.Inventory.Open();
					break;
				case "spells":
					MENU.Spells.Open(GUI.Position.Right);
					break;
				case "skills":
					MENU.Skills.Open();
					break;
				case "social":
					MENU.Social.Open();
					break;
				case "messages":
					MENU.Messages.Open();
					break;
				case "settings":
					MENU.Settings.Open();
					break;
			}

			IMAGE_PROCESSING.loadOpenCV();
		});

		function resize() {
			if (SETTINGS.Get("fullscreen", false) || window.innerHeight >= screen.height -1) {
				container.classList.add("fullscreen");
			}
			else {
				container.classList.remove("fullscreen");
			}
			LOCAL_CHAT.Resize();
			GUI.instance.Resize();
		}

		function update(time) {
			window.requestAnimationFrame(update);
			GUI.instance.Update(time);
			container.scrollTop = 0;
			container.scrollLeft = 0;
		}

	</script>
</head>
<body>
	<div id="preloader"></div>
	<div id="character_hover">
		<canvas></canvas>
		<canvas></canvas>
	</div>
	<div id="container">
		<div id="background_wrapper" class="wrapper">
			<div class="inactive"></div>
		</div>
		<div id="frame" class="wrapper">
			<div></div>
			<div></div>
		</div>
		<div id="frame_top_right">
			<ul id="tokens">
				<li class="spells counter"><span></span><span>-</span><span>/</span><span style="display: none"></span><span>-</span><div></div></li>
				<li class="actions counter"><span></span><span>-</span><span>/</span><span style="display: none"></span><span>-</span><div></div></li>
				<li class="cards counter"><span>-</span><div></div></li>
			</ul>
			<div class="icons">
				<div class="unread_messages inactive"> <span>0</span></div>
				<div class="friend_requests inactive"> <span>0</span></div>
				<div class="online_friends inactive"> <span>0</span></div>
				<div class="crafting inactive"></div>
				<div class="roleplay_broadcasting inactive"></div>
			</div>
		</div>
		<div id="menus"></div>
		<div id="frame_top_left">
			<div id="location">
				<div>
					<span class="location"></span>
					<div class="counter">
						<span>0</span><span>:</span><span>0</span><span>0</span>
					</div>
				</div>
			</div>
		</div>
		<div id="myself">
			<div>
				<div class="button">
					Myself
				</div>
			</div>
		</div>
		<div id="menu">
			<div>
				<div class="button">
					Menu
				</div>
			</div>
		</div>
		<div id="calendar">
			<div></div>
		</div>
		<div id="daytime">
			<div>
				<div></div>
			</div>
		</div>
		<ul id="characters">
			<li class="inactive"></li>
			<li class="inactive"></li>
		</ul>
		<ul id="voice_chat"></ul>
		<ul id="status">
			<li class="inactive">
				<div>
					<div class="body">
						<div class="body_bar"></div>
					</div>
					<div class="mind">
						<div class="mind_bar"></div>
					</div>
				</div>
			</li>
			<li class="inactive">
				<div>
					<div class="body">
						<div class="body_bar"></div>
					</div>
					<div class="mind">
						<div class="mind_bar"></div>
					</div>
				</div>
			</li>
		</ul>
		<div id="countdowns"></div>
		<div id="overhead_status"></div>
		<ul id="dialog">
			<li class="inactive dialog_box">
				<div>
					<div class="text_field"></div>
				</div>
			</li>
			<li class="inactive dialog_box">
				<div>
					<div class="text_field"></div>
				</div>
				<div id="local_chat">
					<div>
						<select id="local_chat_channel" name="local_chat_channel">
							<option value="say">Say</option>
							<option value="ooc">Ooc</option>
							<option value="emote">Emote</option>
						</select>
						<span><div id="local_chat_input" class="editable" contenteditable="true"></div></span>
					</div>
				</div>
			</li>
			<li class="inactive dialog_box">
				<div>
					<div class="text_field"></div>
				</div>
			</li>
		</ul>
		<div id="notifications"></div>
		<div id="options" class="inactive"></div>
		<div id="actions"></div>
		<div id="acquisition"></div>
		<div id="dropdown" class="inactive"></div>
		<div id="tooltip" class="inactive"></div>
		<div id="unstack" class="inactive">
			<div>
				<span>0</span>
				<input type="range" value="0" min="0" max="1" step="1" list="stack_sizes">
				<span>1</span>
			</div>
			<div class="button">
				<span>Take</span>
			</div>
			<datalist id="stack_sizes"></datalist>
		</div>
		<div id="error_messages"></div>
		<div id="load_symbol">
			<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
		</div>
	</div>
</body>
</html>